// import React from "react";
// import PlacesAutocomplete, {
//   geocodeByAddress,
//   getLatLng,
// } from "react-places-autocomplete";
// import endpoints from "../endpoints";
// import GeneralSuccess from "./Alerts-2/GeneralSuccess";

// async function GrabLoc(address, latLng, x, y, successCallback) {
//   var storedToken = localStorage.getItem("stored_token");
//   var json_token = JSON.parse(storedToken);
//   console.log("json_token", json_token.token);
//   console.log("location object", address);
//   console.log("location latLng", latLng);
//   console.log("ID", x);
//   const locationObj = {
//     latitude: latLng.lat,
//     longitude: latLng.lng,
//     address: address,
//   };

//   console.log("location to be updated", locationObj);

//   try {
//     const response = await fetch(
//       y === "yes"
//         ? `${endpoints.addPartnerLocation}/${x}`
//         : `${endpoints.addCustomerLocation}/${x}`,
//       {
//         method: "PUT",
//         headers: {
//           Authorization: `Bearer ${json_token.token}`,
//           "Content-Type": "application/json",
//         },
//         body: JSON.stringify(locationObj),
//       }
//     );

//     const json = await response.json();
//     console.log("Response:", json);

//     if (json.status === "1" && successCallback) {
//       successCallback();
//     }
//   } catch (error) {
//     console.error("There was a problem with your fetch operation:", error);
//   }
// }

// class LocationSearchInput extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { address: "" };
//   }

//   handleChange = (address) => {
//     this.setState({ address });
//   };

//   handleSelect = (address) => {
//     geocodeByAddress(address)
//       .then((results) => getLatLng(results[0]))
//       .then((latLng) =>
//         GrabLoc(
//           address,
//           latLng,
//           this.props.partnerID,
//           this.props.isPartner,
//           () => this.setState({ success: true }) // Callback to set success state
//         )
//       )
//       .catch((error) => console.error("Error", error));
//   };

//   render() {
//     return (
//       <>
//         <GeneralSuccess
//           open={this.state.success}
//           autoHideDuration={6000}
//           msg={"Successfully updated Location"}
//           onClose={() => this.setState({ success: false })}
//         />

//         <PlacesAutocomplete
//           value={this.state.address}
//           onChange={this.handleChange}
//           onSelect={this.handleSelect}
//         >
//           {({
//             getInputProps,
//             suggestions,
//             getSuggestionItemProps,
//             loading,
//           }) => (
//             <div>
//               <input
//                 {...getInputProps({
//                   placeholder: "Search Places ...",
//                   className: "location-search-input",
//                 })}
//               />
//               <div className="autocomplete-dropdown-container">
//                 {loading && <div>Loading...</div>}
//                 {suggestions.map((suggestion) => {
//                   const className = suggestion.active
//                     ? "suggestion-item--active"
//                     : "suggestion-item";
//                   const style = suggestion.active
//                     ? { backgroundColor: "#fff", cursor: "pointer" }
//                     : { backgroundColor: "#fff", cursor: "pointer" };
//                   return (
//                     <div
//                       {...getSuggestionItemProps(suggestion, {
//                         className,
//                         style,
//                       })}
//                     >
//                       <span>{suggestion.description}</span>
//                     </div>
//                   );
//                 })}
//               </div>
//             </div>
//           )}
//         </PlacesAutocomplete>
//       </>
//     );
//   }
// }

// //export default LocationSearchInput;

// export default LocationSearchInput;

import React from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import endpoints from "../endpoints";
import GeneralSuccess from "./Alerts-2/GeneralSuccess";

function reloadPage() {
  window.location.reload();
}

async function GrabLoc(address, latLng, x, y, successCallback) {
  const storedToken = localStorage.getItem("stored_token");
  const json_token = JSON.parse(storedToken);
 // console.log("json_token", json_token.token);
  console.log("location object", address);
  console.log("location latLng", latLng);
  console.log("ID", x);
  const locationObj = {
    latitude: latLng.lat,
    longitude: latLng.lng,
    address: address,
  };

  console.log("location to be updated", locationObj);

  try {
    const response = await fetch(
      y === "yes"
        ? `${endpoints.addPartnerLocation}/${x}`
        : `${endpoints.addCustomerLocation}/${x}`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(locationObj),
      }
    );

    const json = await response.json();
    console.log("Response:", json);

    if (json.status === "1" || (json.status === "Success" && successCallback)) {
      successCallback();

      setTimeout(() => {
        // setOpenAddCompany(false);
        reloadPage();
      }, 2000);
    }
  } catch (error) {
    console.error("There was a problem with your fetch operation:", error);
  }
}

class LocationSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: "", success: false };
  }

  handleChange = (address) => {
    this.setState({ address });
  };

  handleSelect = (address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) =>
        GrabLoc(
          address,
          latLng,
          this.props.partnerID,
          this.props.isPartner,
          () => this.setState({ success: true }) // Callback to set success state
        )
      )
      .catch((error) => console.error("Error", error));
  };

  render() {
    const inputStyles = {
      backgroundColor: "#f0f0f0", // Change this to your desired background color
      color: "#000", // Change this to your desired text color
      padding: "10px",
      borderRadius: "4px",
      border: "1px solid #ddd",
      width: "100%",
    };

    return (
      <>
        <GeneralSuccess
          open={this.state.success}
          autoHideDuration={6000}
          msg={"Successfully updated Location"}
          onClose={() => this.setState({ success: false })}
        />

        <PlacesAutocomplete
          value={this.state.address}
          onChange={this.handleChange}
          onSelect={this.handleSelect}
        >
          {({
            getInputProps,
            suggestions,
            getSuggestionItemProps,
            loading,
          }) => (
            <div>
              <input
                {...getInputProps({
                  placeholder: "Search Places ...",
                  className: "location-search-input",
                  style: inputStyles, // Apply the custom styles here
                })}
              />
              <div className="autocomplete-dropdown-container">
                {loading && <div>Loading...</div>}
                {suggestions.map((suggestion) => {
                  const className = suggestion.active
                    ? "suggestion-item--active"
                    : "suggestion-item";
                  const style = suggestion.active
                    ? { backgroundColor: "#fafafa", cursor: "pointer" }
                    : { backgroundColor: "#ffffff", cursor: "pointer" };
                  return (
                    <div
                      {...getSuggestionItemProps(suggestion, {
                        className,
                        style,
                      })}
                    >
                      <span>{suggestion.description}</span>
                    </div>
                  );
                })}
              </div>
            </div>
          )}
        </PlacesAutocomplete>
      </>
    );
  }
}

export default LocationSearchInput;
