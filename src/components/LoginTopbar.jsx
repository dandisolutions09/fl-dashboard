import React from "react";
import { AiOutlineMenu } from "react-icons/ai";
import { FaHome, FaUser } from "react-icons/fa";
import { IoGameControllerOutline } from "react-icons/io5";
import { RiAccountCircleLine } from "react-icons/ri";
import { TbLogout2 } from "react-icons/tb";
import { BiSolidShare } from "react-icons/bi";
import { HiServer } from "react-icons/hi";
import { useNavigate } from "react-router-dom";

export default function LoginTopbar() {
  const handleNav = () => {
    setNav(!nav);
    console.log("nav changed");
    handleBar(); // Call handleBar after setting nav
  };
  const navigate = useNavigate();
  const handleNavigate = () =>{navigate("/dash");} 

  return (
    <>
      <div className="relative w-full sm:block z-50 bg-[#f5f5f5] shadow-md shadow-gray-400 py-3 select-none">
        <div className="flex flex-row justify-end m-2 sm:space-x-6 space-x-2 sm:mr-6 mr-3">
          <div className="flex flex-row mt-2 gap-1 cursor-pointer hover:underline decoration-solid underline-offset-8 decoration-black">
            <BiSolidShare
              color="#000"
              size={18}
              onClick={handleNav}
              className=" z-[50] md:hiddden cursor-pointer"
            />
            <h2 className="text-black sm:text-sm text-xs">Main website</h2>
          </div>
          <div className="flex flex-row mt-2 gap-1 cursor-pointer hover:underline decoration-solid underline-offset-8 decoration-black">
            <FaUser
              color="#000"
              size={18}
              onClick={handleNav}
              className=" z-[50] md:hiddden cursor-pointer"
            />
            <h2 className="text-black sm:text-sm text-xs">User Login</h2>
          </div>
          <div className="flex flex-row mt-2 gap-1 cursor-pointer hover:underline decoration-solid underline-offset-8 decoration-black">
            <HiServer
              color="#000"
              size={18}
              onClick={handleNav}
              className=" z-[50] md:hiddden cursor-pointer "
            />
            <h2 className="text-black sm:text-sm text-xs">Provider Login</h2>
          </div>
          <div
            className="flex flex-row mt-2 gap-1 cursor-pointer hover:underline decoration-solid underline-offset-8 decoration-black"
            // onClick={handleNavigate}
          >
            <FaHome
              color="#000"
              size={18}
              className=" z-[50] md:hiddden cursor-pointer"
            />
            <h2 className="text-black sm:text-sm text-xs">Company Login</h2>
          </div>
        </div>
      </div>
    </>
  );
}
