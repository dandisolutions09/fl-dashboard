import React from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

const AdminAddfailed = ({ open, onClose, autoHideDuration, name, mssg }) => {
  return (
    <Snackbar
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert onClose={onClose} severity="error" sx={{ width: ["60%", "100%"] }}>
        {mssg} {name}
      </Alert>
    </Snackbar>
  );
};

export default AdminAddfailed;
