import React, { useState } from 'react'
import endpoints from '../endpoints';
import axios from "axios";


export default function UserNameTag() {
      const [firstName, setFirstName] = useState();
      const [lastName, setLastName] = useState();
        async function getAdminDetails() {
          var storedToken = localStorage.getItem("stored_token");
          var stored_json_data = JSON.parse(storedToken);
          console.log("json_token", stored_json_data);
          console.log(
            "full url",
            `${endpoints.getAdminById}/${stored_json_data.admin_id}`
          );

          try {
            // Replace 'YOUR_ENDPOINT' with the actual endpoint URL
            const response = await axios.get(
              `${endpoints.getAdminById}/${stored_json_data.admin_id}`,
              {
                headers: {
                  Authorization: `Bearer ${stored_json_data.token}`,
                },
              }
            );
            //setAdmin(response.data);
            console.log("response", response.data.first_name);
            setFirstName(response.data.first_name);
            setLastName(response.data.last_name);
          } catch (error) {
            //setError(error.message);
            console.log("error", error);
          }
        }
          React.useEffect(() => {
            //handleGetAllRecords();
            getAdminDetails();
          }, []);
  return (
    <div>
      <h1 className="text-black text-sm">
        {firstName} {lastName}
      </h1>
    </div>
  );
}
