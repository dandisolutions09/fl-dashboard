import * as React from "react";
import { BarChart } from "@mui/x-charts/BarChart";

export default function BasicBarChart() {
    const data2= [4, 3, 5, 6, 7, 3, 8] 
  return (
    <BarChart
      xAxis={[
        {
          scaleType: "band",
          data: ["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"],
        },
      ]}
      series={[{ data: data2 }]}
      width={500}
      height={300}
    />
  );
}
