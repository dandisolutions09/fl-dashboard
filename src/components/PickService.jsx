import React, { useState } from "react";
import Button from "@mui/material/Button";
import { Badge, IconButton, Typography } from "@mui/material";
import endpoints from "../endpoints";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import GeneralSuccess from "./Alerts-2/GeneralSuccess";
import GeneralError from "./Alerts-2/GeneralError";

function YourComponent({
  services,
  selectedCustomerProp,
  itemIdProp,
  cartNumber,
  setCartNumber,

  cartItems,
  setCartItems,
}) {
  console.log("pick service:->>>>>", services);

  const initialButtonStates = services.map(() => ({
    color: "#000",
    text: "Pick",
  }));

  console.log("intial button states--->", initialButtonStates);
  const [buttonStates, setButtonStates] = useState(initialButtonStates);
  const [serviceId, setServiceId] = useState(initialButtonStates);

  const [generalError, setGeneralError] = useState();
  const [generalSuccess, setGeneralSuccess] = useState();

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  //const [cartNumber, setCartNumber] = useState();

  //      setServiceId(responseData.service_id);

  //   const handleConfirm_pick_service = (serviceIndex) => {
  //     setButtonStates(prevState => {
  //       const newButtonStates = [...prevState];
  //       newButtonStates[serviceIndex] = {
  //         color: "#ff0000", // Red color
  //         text: "Remove"
  //       };
  //       return newButtonStates;
  //     });
  //     // Do whatever other actions you need here
  //   };

  const handleConfirm_pick_service = (serviceIndex, service) => {
    setButtonStates((prevState) => {
      const newButtonStates = [...prevState];
      const currentState = newButtonStates[serviceIndex].text;
      if (currentState === "Pick") {
        newButtonStates[serviceIndex] = {
          color: "#000", // Original color
          text: "Pick",
        };
        // Call function to pick service
        updateCustomerCart_ServiceDetails(service);

        setSuccessMsg("Added Successfully to Cart!");
        setGeneralSuccess(true);
      } else {
        newButtonStates[serviceIndex] = {
          color: "#000", // Original color
          text: "Pick",
        };
        // Call function to remove service
        // remove_ServiceDetailsfromCart();
      }
      return newButtonStates;
    });
  };

  const updateCustomerCart_ServiceDetails = async (service) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);

    console.log("SERVICE ID", service.service_id);
    // console.log("Updating id", selectedRowData._id);

    // console.log("updated data", data, json_token);

    const ServiceDetails = {
      service_id: service.service_id,
      type: service.type,
      status: service.status,
      description: service.description,
      subcategory: service.subcategory,
      created_at: service.created_at,
      price: service.price,
      service_duration: service.service_duration,
      ServiceDuration: service.duration,

      // },
    };

    console.log("item id prop", itemIdProp);
    console.log("selectedCustomerProp id prop:->", selectedCustomerProp);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      //{{LOCAL_BASE_URL}}/add-services-to-cart?id=66225643fae1a42124a52d24&cart_item_id=f4d2feaa-7714-415e-b5c4-0d84c915a513

      const response = await fetch(
        //  `${endpoints.updateCustomerCart}/${selectedCustomer._id}`,
        // `${endpoints.addServiceToCart}?id=${selectedCustomerProp._id}&cart_item_id=${itemIdProp}`,
        `${endpoints.addServiceToBooking}/${selectedCustomerProp._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(ServiceDetails),
        }
      );
      console.log("response after edit:->>", response);
      const responseData = await response.json(); // Extract JSON data from the response body
      console.log(responseData);
      setServiceId(responseData.service_id);
      console.log("cart length", responseData.pending_booking.cart.length);
      setCartNumber(responseData.pending_booking.cart.length);
      // setCartItems(responseData.pending_booking.cart);

      console.log("cart Items", responseData.pending_booking.cart);

      const cartData = {
        cart_number: responseData.pending_booking.cart.length,
        // admin_id: incoming_admin_id,
      };
      //setUserData(userData);

      const userDataJSON = JSON.stringify(cartData);

      localStorage.setItem("stored_cart_number", userDataJSON);

      //setItemId(responseData.item_id);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  };

  const remove_ServiceDetailsfromCart = async (service) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("Updating id", selectedRowData._id);

    //console.log("cardObject customer details", ServiceDetails);
    console.log("item id prop", itemIdProp);
    console.log("selectedCustomerProp id prop:->", selectedCustomerProp);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      //{{LOCAL_BASE_URL}}/add-services-to-cart?id=66225643fae1a42124a52d24&cart_item_id=f4d2feaa-7714-415e-b5c4-0d84c915a513
      //{{LOCAL_BASE_URL}}/remove-services-from-cart?id=66225643fae1a42124a52d24&cart_id=b8a320c7-7ec9-461f-b858-e97525cdbf63&service_id=3412fb55-092b-4044-a168-3d1bd5c618e7

      const response = await fetch(
        //  `${endpoints.updateCustomerCart}/${selectedCustomer._id}`,
        `${endpoints.removeServiceFromCart}?id=${selectedCustomerProp._id}&cart_id=${itemIdProp}&service_id=${serviceId}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          //body: JSON.stringify(ServiceDetails),
        }
      );
      console.log("response after edit:->>", response);
      const responseData = await response.json(); // Extract JSON data from the response body
      console.log(responseData);
      //setItemId(responseData.item_id);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  };

  return (
    <div>
      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />

      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />

      {services.map((service, serviceIndex) => (
        <div
          key={serviceIndex}
          className="flex flex-row gap-2 justify-between items-center border-b py-2 border-gray-400"
        >
          <Typography sx={{ fontSize: [13] }}>-{service.type}</Typography>

          <Typography sx={{ fontSize: [13] }}>KES: {service.price}</Typography>

          <Typography sx={{ fontSize: [13] }}>
            {service.localization}
          </Typography>
          <Typography sx={{ fontSize: [13] }}>{service.description}</Typography>

          <Button
            variant="contained"
            size="small"
            sx={{ bgcolor: buttonStates[serviceIndex].color }}
            onClick={() => handleConfirm_pick_service(serviceIndex, service)}
          >
            {buttonStates[serviceIndex].text}
          </Button>
        </div>
      ))}
    </div>
  );
}

export default YourComponent;
