import React from "react";
import { AiOutlineMenu } from "react-icons/ai";
import { IoGameControllerOutline } from "react-icons/io5";
import { RiAccountCircleLine } from "react-icons/ri";
import { TbLogout2 } from "react-icons/tb";
import { useNavigate } from "react-router-dom";
import { IoIosNotificationsOutline, IoMdNotifications } from "react-icons/io";
import {
  Badge,
  Box,
  IconButton,
  LinearProgress,
  Typography,
} from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MoreIcon from "@mui/icons-material/MoreVert";
import MailIcon from "@mui/icons-material/Mail";
import AccountCircle from "@mui/icons-material/AccountCircle";
import endpoints from "../endpoints";
import { useState } from "react";
import axios from "axios";

export default function TopNav() {
  const [loading, setLoading] = React.useState(false);
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const handleNav = () => {
    setNav(!nav);
    console.log("nav changed");
    handleBar(); // Call handleBar after setting nav
  };
  const navigate = useNavigate();
  const handle_logout = () => {
    setLoading(true);
    setTimeout(() => {
      navigate("/");
    }, 1000);
  };

  async function getAdminDetails() {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
   // console.log("json_token", stored_json_data);
    // console.log(
    //   "full url",
    //   `${endpoints.getAdminById}/${stored_json_data.admin_id}`
    // );

    try {
      // Replace 'YOUR_ENDPOINT' with the actual endpoint URL
      const response = await axios.get(
        `${endpoints.getAdminById}/${stored_json_data.admin_id}`,
        {
          headers: {
            Authorization: `Bearer ${stored_json_data.token}`,
          },
        }
      );
      //setAdmin(response.data);
      console.log("response", response.data.first_name);
      setFirstName(response.data.first_name);
      setLastName(response.data.last_name);
    } catch (error) {
      //setError(error.message);
      console.log("error", error);
    }
  }

  React.useEffect(() => {
    //handleGetAllRecords();
    getAdminDetails();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      {loading && <LinearProgress />}
      <div className="relative w-full sm:block hidden  z-50 bg-[#f5f5f5] shadow-md shadow-gray-300 select-none">
        <div className="flex flex-row justify-between">
          <div className="flex">
            <AiOutlineMenu
              color="#000"
              size={23}
              onClick={handleNav}
              className="m-4 z-[50] md:hiddden cursor-pointer"
            />
            <h2 className="text-black mt-4">
              {firstName} {lastName}
            </h2>
          </div>
          <Box sx={{ display: { xs: "none", md: "flex" }, marginX: 3 }}>
            <IconButton
              size="large"
              aria-label="show 17 new notifications"
              color="inherit"
            >
              <Badge badgeContent={17} color="error">
                <IoMdNotifications color="#000" size={24} />
              </Badge>
            </IconButton>
            <IconButton
              size="large"
              aria-label="show 4 new mails"
              color="inherit"
              onClick={handle_logout}
            >
              <TbLogout2 color="#000" size={24} />
              <Typography sx={{ color: "#000", marginTop: 1 }}>
                Logout
              </Typography>
            </IconButton>
            {/* <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              // aria-controls={menuId}
              aria-haspopup="true"
              // onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton> */}
          </Box>
        </div>
      </div>
    </>
  );
}
