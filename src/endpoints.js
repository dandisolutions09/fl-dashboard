//const BASE_URL = "https://finelooks-backend-service.onrender.com";
//const BASE_URL = "http://localhost:8080";
const BASE_URL = "https://api.finelooks.co.uk";
//sss

//const LOCAL_BASE_URL = "http://localhost:8080"; // Your base URL
const GOOGLE_API =
  "https://maps.googleapis.com/maps/api/place/autocomplete/json";

const GOOGLE =
  "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDolZ9dXT3cqWqqHt6XTyojZlzt1EmtZoo&input=Ngumba&types=geocode&components=country:KE";

//const BASE_URL = "https://8301-41-80-117-153.ngrok-free.app";
//const BASE_URL = "http://localhost:8080";

//https://8301-41-80-117-153.ngrok-free.app

const endpoints = {
  login: `${BASE_URL}/admin-login`,
  getAdmins: `${BASE_URL}/get-admins`,
  addAdmin: `${BASE_URL}/add-admin`,
  getAdminById: `${BASE_URL}/get-admin`,
  resetAdminPassword: `${BASE_URL}/admin-reset-password`,
  editAdminInfo: `${BASE_URL}/update-admin`,

  //partner endpoints
  //getPartnerById: `${LOCAL_BASE_URL}/get-partner-by-id`,
  registerPartner: `${BASE_URL}/add-partner`,
  editProviderRegisrationInfo: `${BASE_URL}/update-provider-info`,
  addProviderAvailability: `${BASE_URL}/add-availability-dash`,
  addProviderBookingPreference: `${BASE_URL}/add-booking-preference`,
  deleteProvider: `${BASE_URL}/delete-partner`,

  uploadProfilePic: `${BASE_URL}/upload-profile`,
  uploadServiceCategoryPic: `${BASE_URL}/upload-category-pic`,
  updateLogoPicURL: `${BASE_URL}/add-businessLogo`,
  updateProfilePicURL: `${BASE_URL}/update-profile-url`,

  uploadLogoPic: `${BASE_URL}/upload-logo`,
  updateLogoPicURL: `${BASE_URL}/add-businessLogo`,
  // uploadLogoPic: `${BASE_URL}/upload-logo`,
  //updateLogoPicURL: `${BASE_URL}/add-businessLogo`,

  //ADD PARTNER SERVICE
  addPartnerService: `${BASE_URL}/add-partner-services`,
  getSortedServices: `${BASE_URL}/get-grouped-services`,
  getSortedPartnerServices: `${BASE_URL}/get-sorted-partner-services-dash`,
  getPartners: `${BASE_URL}/get-partners`,
  getActivePartners: `${BASE_URL}/get-active-partners`,

  filterPartners: `${BASE_URL}/filter-partners-by-status`,
  filterCustomers: `${BASE_URL}/filter-customers-by-status`,
  // Add more endpoints as needed

  //SERVICES
  getServices: `${BASE_URL}/get-services`,
  //getServices: `${LOCAL_BASE_URL}/get-services`,
  postServices: `${BASE_URL}/add-service`,

  //CUSTOMERS
  getCustomers: `${BASE_URL}/get-customers`,
  getActiveCustomers: `${BASE_URL}/get-active-customers`,
  registerCustomer: `${BASE_URL}/add-customer`,
  uploadCustomerProfilePic: `${BASE_URL}/upload-customer-profile`,
  updateCustomerProfilePicURL: `${BASE_URL}/update-customer-profile-url`,
  editCustomerRegistrationInfo: `${BASE_URL}/update-customer-info`,
  updateCustomerCart_CustomerDetails: `${BASE_URL}/update-cart-customerdetails`,
  updateCustomerCart: `${BASE_URL}/update-customer-cart`,
  addPendingBooking: `${BASE_URL}/add-pending-booking`,

  addServiceToCart: `${BASE_URL}/add-services-to-cart`,
  addServiceToBooking: `${BASE_URL}/add-service-to-booking`,

  getCustomerCart: `${BASE_URL}/get-customer-cart`,

  //add-service-to-booking/
  removeItemFromCart: `${BASE_URL}/remove-from-cart`,

  //get timelsots
  getTimeSlots: `${BASE_URL}/get-timeslots`,

  //add time and date to booking
  addBookingTime: `${BASE_URL}/add-booking-time`,

  //Get Cart Items
  getCartItems: `${BASE_URL}/get-customer-cart`,

  //data

  //LOGS
  getLogs: `${BASE_URL}/get-logs`,

  //get partner by id
  getPartnerByID: `${BASE_URL}/get-partner-by-id`,

  //finalize booking
  finalizeBooking: `${BASE_URL}/finalize-booking`,

  ///GetBookings
  getAllBookings: `${BASE_URL}/get-bookings`,

  //Google Maps
  getPlaces: `${GOOGLE}`,

  //ADD LOCATION
  addPartnerLocation: `${BASE_URL}/add-location`,

  //ADD CUSTOMER-LOCATION
  addCustomerLocation: `${BASE_URL}/add-customer-location`,

  //CUSTOMER RESET PASSWORD
  resetCustomerPassword: `${BASE_URL}/customer-reset-password`,

  //DEACTIVATE CUSTOMER
  deactivateCustomer: `${BASE_URL}/deactivate-customer`,

  //ACTIVATE CUSTOMER
  activateCustomer: `${BASE_URL}/activate-customer`,

  ///ADD SERVICE CATEGORY
  postServicesCategory: `${BASE_URL}/add-service-category`,

  //GET SERVICE CATEGORY
  getServiceCategories: `${BASE_URL}/get-service-categories`,

  //LOGOUT
  logout: `${BASE_URL}/logout`,

  //GET STATS
  getStats: `${BASE_URL}/get-stats`,


  //filterbooking status
  filterBookingStatus : `${BASE_URL}/filter-bookings`,
};

export default endpoints;
