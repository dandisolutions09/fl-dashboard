import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import endpoints from "../endpoints";
import { CiSettings } from "react-icons/ci";
import CircularProgress from "@mui/joy/CircularProgress";
import WorkspacesIcon from "@mui/icons-material/Workspaces";
import dayjs from "dayjs";
import location from "../assets/location.png";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { MobileDatePicker } from "@mui/x-date-pickers/MobileDatePicker";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { StaticDatePicker } from "@mui/x-date-pickers/StaticDatePicker";
import BookOnlineIcon from "@mui/icons-material/BookOnline";
import BookIcon from "@mui/icons-material/Book";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import AccessTimeOutlinedIcon from "@mui/icons-material/AccessTimeOutlined";
import PhoneAndroidOutlinedIcon from "@mui/icons-material/PhoneAndroidOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import Loader2 from "../components/loader2/Loader2.jsx";
import { FaTrashAlt } from "react-icons/fa";
//import { IoMdCloseCircle } from 'react-icons/io';
import {
  Badge,
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  IconButton,
  InputAdornment,
  InputLabel,
  LinearProgress,
  Menu,
  MenuItem,
  Modal,
  Radio,
  RadioGroup,
  Select,
  Step,
  StepButton,
  StepLabel,
  Stepper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import { TextareaAutosize as BaseTextareaAutosize } from "@mui/base/TextareaAutosize";
import TablePagination from "@mui/material/TablePagination";
import { styled, alpha } from "@mui/material/styles";
import { BsGearFill } from "react-icons/bs";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import {
  MdAirplanemodeActive,
  MdAirplanemodeInactive,
  MdEdit,
  MdModeEdit,
  MdOutlineAirplanemodeInactive,
} from "react-icons/md";
import { RiAccountPinCircleFill, RiDeleteBinFill } from "react-icons/ri";
import {
  IoIosCloseCircle,
  IoIosCloseCircleOutline,
  IoMdCloseCircle,
  IoMdNotifications,
} from "react-icons/io";
import { useFormik } from "formik";
import { useState } from "react";
import hsp from "../assets/avatar5.jpg";
import hsp4 from "../assets/admin-logo.png";
import hsp1 from "../assets/Untitled.png";
import axios from "axios";
import { FaCamera, FaUser, FaUsers } from "react-icons/fa";
import utils from "../utils";
import { Textarea } from "../styles";
import AdminAddedSuccess from "../components/Alerts/AdminAddedSuccess";
import UserNameTag from "../components/UserNameTag";
import AdminAddfailed from "../components/Alerts/AdminAddfailed";

import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import LightTooltip from "../components/LightTooltip";
import { HiServer } from "react-icons/hi";
import YourComponent from "../components/PickService";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import { Close } from "@mui/icons-material";
import { dayCalendarClasses } from "@mui/x-date-pickers";
// import imagemin from "imagemin";
// import imageminJpegtran from "imagemin-jpegtran";
// import imageminPngquant from "imagemin-pngquant";

import GeneralError from "../components/Alerts-2/GeneralError";
import GeneralSuccess from "../components/Alerts-2/GeneralSuccess";

const today = dayjs();

const isInCurrentMonth = (date) => date.get("month") === dayjs().get("month");

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",

    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function ManualBookingPage() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [loader, setLoader] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openAction, setOpenAction] = React.useState(false);
  const [isOPen, setIsOpen] = React.useState(false);
  const [openAddCompany, setOpenAddCompany] = React.useState(false);
  const [openfile, setOpenfile] = useState(false);
  const [openBgfile, setOpenBgfile] = useState(false);
  const [searchValue, setSearchValue] = React.useState();
  const [searchTerm, setSearchTerm] = React.useState();
  const [searchTerm_providers, setSearchTerm_providers] = React.useState();

  const [searchTerm_customers, setSearchTerm_customers] = useState();
  const [openCartModal, setOpenCartModal] = React.useState(false);

  const [openProviderUpdateSuccess, setOpenProviderUpdateSuccess] =
    useState(false);

  const [openmap, setOpenmap] = React.useState(false);
  const { time_array } = utils;
  const [file, setFile] = useState();
  const [bgfile, setBgFile] = useState();
  const [activeStep, setActiveStep] = React.useState(0);
  const [openEdit, setOpenEdit] = React.useState(false);
  const [skipped, setSkipped] = React.useState(new Set());
  const [openConfirmPick, setOpenConfirmPick] = React.useState(false);
  const [openConfirmPick_service, setOpenConfirmPick_service] =
    React.useState(false);
  const [openConfirmPick_provider, setOpenConfirmPick_provider] =
    React.useState(false);

  const [completed, setCompleted] = React.useState({});
  const profilePicRef = React.useRef(null);
  const [pp, setPP] = useState("");
  const [openAddAdminSuccess, setOpenAddAdminSuccess] = React.useState(false);
  const [openImageSuccess, setOpenImageSuccess] = React.useState(false);
  const [openAddAdminFailed, setOpenAddAdminFailed] = React.useState(false);
  const [openProviderUpdateFailed, setOpenProviderUpdateFailed] =
    React.useState(false);

  const [subcategory, setSubcategory] = React.useState("mens_hair");
  const [expanded, setExpanded] = React.useState(false);

  const [activeCustomers, setActiveCustomer] = useState();
  const [activeProviders, setActiveProvider] = useState();

  const [cartNumber, setCartNumber] = useState();
  const [cartItems, setCartItems] = useState();
  const [timeslots, setTimeSlots] = useState();
  const [selectedTimeSlot, setSelectedTimeSlot] = useState();
  const [selectedBookingDate, setSelectedBookingDate] = useState();
  const [timeslotsDay, setTimeslotsDay] = useState();

  //setCustomer

  const [BookingPreferencevalue, setBookingPreferencevalueValue] =
    React.useState();
  function reloadPage() {
    window.location.reload();
  }
  // const [paymentMethod, setPaymentMethod] = React.useState("Mobile Service");
  const [selectedRowData, setSelectedRowData] = useState();

  const [serviceType, setServiceType] = useState();
  const [paymentmethod, setPaymentMethod] = useState();

  const [bp, setBP] = useState();

  const [stateToken, setToken] = useState();

  const [sortedPartnerServices, setSortedServices] = useState();

  const [selectedCustomer, setSelectedCustomer] = useState();

  const [selectedProvider, setSelectedProvider] = useState();

  const [itemID, setItemId] = useState();

  const [service, setService] = useState();

  const [buttonColor, setButtonColor] = useState("#000"); // Initial color is black
  const [buttonText, setButtonText] = useState("Pick"); // Initial text is "Pick"
  //const [buttonStates, setButtonStates] = useState(Array(services.length).fill({ color: "#000", text: "Pick" }));
  // const initialButtonStates = services.map(() => ({ color: "#000", text: "Pick" }));
  // const [buttonStates, setButtonStates] = useState(initialButtonStates);
  const [totalServiceDuration, setTotalServicesDuration] = useState();
  const [totalServicePrice, setTotalServicePrice] = useState();

  const [partner_serviceType, setPartnerServiceType] = useState();
  const [partner_Location, setProviderLocation] = useState();

  const [generalError, setGeneralError] = useState();
  const [generalSuccess, setGeneralSuccess] = useState();

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  const [bookingStatus, setBookingStatus] = React.useState("ALL");

  const [activateCustomer, setOpenActivateCustomerRecordModal] =
    useState(false);

  const menuItems = [
    { value: "ALL", label: "ALL" },
    { value: "ONGOING", label: "ONGOING" },
    { value: "UPCOMING", label: "UPCOMING" },
    { value: "COMPLETED", label: "COMPLETED" },
    { value: "CANCELLED", label: "CANCELLED" },
  ];

  //setProviderLocation

  const handleOPen_cartmodal = () => {
    setOpenCartModal(true);
    console.log("selected provider", selectedProvider);
    handleGetCustomerCart();
  };
  const handleClose_cartmodal = () => {
    setOpenCartModal(false);
  };

  const handleChangeServiceLocation = (event) => {
    setServiceType(event.target.value);
    console.log("Service Location:->", event.target.value);
  };

  const handleChangeRadio_paymentMethod = (event) => {
    setPaymentMethod(event.target.value);
    console.log("payment method:->", event.target.value);
  };

  const handleExpansion = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : null);
  };

  function handleGetSortedPartnerServices(searchVal) {
    setLoading(true);
    console.log("handle get all records called");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    // console.log("selected", selectedRowData._id);

    if (subcategory !== null) {
      const filtered_type = subcategory;
      console.log("subcategory", filtered_type);

      axios
        .get(
          `${endpoints.getSortedPartnerServices}?partner_id=${selectedProvider._id}`,
          {
            headers: {
              Authorization: `Bearer ${json_token.token}`,
              "Content-Type": "application/json",
            },
          }
        )
        .then((response) => {
          //const json = response.data;
          // json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response sorted data:- ", response.data);
          setSortedServices(response.data.partner_services);

          // if (searchVal === undefined || searchVal.trim().length === 0) {
          //   console.log("my json from edit", rows);
          //   setRows(json);
          // } else {
          //   // console.log("else json", json);
          //   const results = json.filter((user) => {
          //     const Type = user.partner_services
          //       ? user.partner_services.toLowerCase()
          //       : "";
          //     const searchTerm = searchVal.toLowerCase();
          //     return Type.includes(searchTerm);
          //   });
          //   console.log("my results", results);
          //   setRows(results);
          // }
        })
        .catch((error) => {
          console.error("There was a problem with your Axios request:", error);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      console.log("Subcategiry empty", subcategory);
    }
  }

  function handleGetCustomer(searchVal) {
    console.error("Handle Get Customer");
    setLoading(true);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    setToken(json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);

    fetch(endpoints.getActiveCustomers, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        // console.log("JSON:->", json);
        // console.log("customer results:--->", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

        // setCustomer(json);

        localStorage.setItem("activeUserData", JSON.stringify(json));
        console.log("customer data set globally...");
        setActiveCustomer(json);

        // Your further processing logic
        // if (searchVal === undefined || searchVal.trim().length === 0) {
        //   //setRows(json);
        //   setCustomer(json);
        // } else {
        //   const results = json.filter((user) => {
        //     const firstName = user.first_name
        //       ? user.first_name.toLowerCase()
        //       : "";
        //     const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        //     const BusinessName = user.main_businessName
        //       ? user.main_businessName.toLowerCase()
        //       : "";
        //     const email = user.email ? user.email.toLowerCase() : "";

        //     const searchTerm = searchVal.toLowerCase();

        //     return (
        //       firstName.includes(searchTerm) ||
        //       lastName.includes(searchTerm) ||
        //       BusinessName.includes(searchTerm) ||
        //       email.includes(searchTerm)
        //     );
        //   });
        //   //setRows(results);
        //   console.log("results:->", results);
        //   setCustomer(results);
        // }
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  // function handleGetProviders(searchVal) {
  //   console.error("Handle Get Customer");
  //   setLoading(true);
  //   var storedToken = localStorage.getItem("stored_token");
  //   var json_token = JSON.parse(storedToken);
  //   console.log("json_token", json_token.token);

  //   setToken(json_token.token);

  //   //console.error("retrieved username  and ID:-->", storedData);
  //   setLoading(true);

  //   fetch(endpoints.getActivePartners, {
  //     method: "GET",
  //     headers: {
  //       Authorization: `Bearer ${json_token.token}`,
  //       "Content-Type": "application/json", // optional, depending on your API requirements
  //     },
  //   })
  //     .then((response) => response.json())
  //     .then((json) => {
  //       // console.log("JSON:->", json);
  //       console.log("customer results:--->", json);
  //       json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

  //       localStorage.setItem("activeProviderData", JSON.stringify(json));
  //       console.log("provider data set globally...");
  //       setActiveProvider(json);

  //       // Your further processing logic
  //       // if (searchVal === undefined || searchVal.trim().length === 0) {
  //       //   //setRows(json);
  //       //   setProvider(json);
  //       // } else {
  //       //   const results = json.filter((user) => {
  //       //     const firstName = user.first_name
  //       //       ? user.first_name.toLowerCase()
  //       //       : "";
  //       //     const lastName = user.last_name ? user.last_name.toLowerCase() : "";
  //       //     const BusinessName = user.main_businessName
  //       //       ? user.main_businessName.toLowerCase()
  //       //       : "";
  //       //     const email = user.email ? user.email.toLowerCase() : "";

  //       //     const searchTerm = searchVal.toLowerCase();

  //       //     return (
  //       //       firstName.includes(searchTerm) ||
  //       //       lastName.includes(searchTerm) ||
  //       //       BusinessName.includes(searchTerm) ||
  //       //       email.includes(searchTerm)
  //       //     );
  //       //   });
  //       //   //setRows(results);
  //       //   console.log("results:->", results);
  //       //   setProvider(results);
  //       // }
  //     })
  //     .catch((error) => {
  //       console.error("There was a problem with your fetch operation:", error);
  //     })
  //     .finally(() => {
  //       setLoading(false);
  //     });
  // }

  function handleGetProviders(searchVal) {
    console.error("Handle Get Providers");
    setLoading(true);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("json_token", json_token.token);

    setToken(json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);

    fetch(endpoints.getActivePartners, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        // console.log("JSON:->", json);
        console.log("customer results:--->", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        setActiveProvider(json);

        localStorage.setItem("activeProviderData", JSON.stringify(json));
        console.log("provider data set globally...");
        setActiveProvider(json);

        // // Your further processing logic
        // if (searchVal === undefined || searchVal.trim().length === 0) {
        //   //setRows(json);
        //   setProvider(json);
        // } else {
        //   const results = json.filter((user) => {
        //     const firstName = user.first_name
        //       ? user.first_name.toLowerCase()
        //       : "";
        //     const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        //     const BusinessName = user.main_businessName
        //       ? user.main_businessName.toLowerCase()
        //       : "";
        //     const email = user.email ? user.email.toLowerCase() : "";

        //     const searchTerm = searchVal.toLowerCase();

        //     return (
        //       firstName.includes(searchTerm) ||
        //       lastName.includes(searchTerm) ||
        //       BusinessName.includes(searchTerm) ||
        //       email.includes(searchTerm)
        //     );
        //   });
        //   //setRows(results);
        //   console.log("results:->", results);
        //   setActiveProvider(results);
        // }
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const updatePartnerBookingPreference = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    const bookingpreference = {
      payment_option: paymentmethod,
      service_type: serviceType,
    };

    console.log("main object", bookingpreference);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.addProviderBookingPreference}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(bookingpreference),
        }
      );
      console.log("response after edit:->>", response);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  };

  // const calculateTotalDuration = (x) => {
  //   let totalDuration = 0;

  //   x.forEach((service) => {
  //     totalDuration += parseInt(service.service_duration);
  //   });

  //   return totalDuration/60;
  // };

  const calculateTotalDuration = (x) => {
    let totalDuration = 0;

    x.forEach((service) => {
      totalDuration += parseInt(service.service_duration);
    });

    if (totalDuration < 60) {
      return `${totalDuration} minute(s)`;
    } else {
      const hours = Math.floor(totalDuration / 60);
      const minutes = totalDuration % 60;
      if (minutes === 0) {
        return `${hours} hour(s)`;
      } else {
        return `${hours} hour(s) and ${minutes} minute(s)`;
      }
    }
  };

  const calculateTotalServicePrice = (x) => {
    let totalPrice = 0;

    x.forEach((service) => {
      totalPrice += parseInt(service.price);
    });

    return totalPrice;
  };

  const remove_ServiceDetailsfromCart = async (service) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("Updating id", selectedRowData._id);

    //console.log("cardObject customer details", ServiceDetails);
    //  console.log("item id prop", itemIdProp);
    // console.log("selectedCustomerProp id prop:->", selectedCustomerProp);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      //{{LOCAL_BASE_URL}}/add-services-to-cart?id=66225643fae1a42124a52d24&cart_item_id=f4d2feaa-7714-415e-b5c4-0d84c915a513
      //{{LOCAL_BASE_URL}}/remove-services-from-cart?id=66225643fae1a42124a52d24&cart_id=b8a320c7-7ec9-461f-b858-e97525cdbf63&service_id=3412fb55-092b-4044-a168-3d1bd5c618e7
      console.log("Service id", service.service_id);
      //console.log("customer id",)
      const response = await fetch(
        //  `${endpoints.updateCustomerCart}/${selectedCustomer._id}`,
        `${endpoints.removeItemFromCart}?customer_id=${selectedCustomer._id}&service_id=${service.service_id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          //body: JSON.stringify(ServiceDetails),
        }
      );
      console.log("response after removal:->>", response);
      const responseData = await response.json(); // Extract JSON data from the response body
      console.log(responseData.cart);
      setCartItems(responseData.cart);

      setTotalServicesDuration(calculateTotalDuration(responseData.cart));
      setTotalServicePrice(calculateTotalServicePrice(responseData.cart));
      console.log(
        "total service duration",
        calculateTotalDuration(responseData.cart)
      );

      console.log(
        "total service Price",
        calculateTotalServicePrice(responseData.cart)
      );

      setCartNumber(responseData.cart.length);
      console.log("updated cart", responseData.cart);
      console.log("length of the cart", responseData.cart.length);
      //setItemId(responseData.item_id);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  };

  function handleRemove(x) {
    console.log("service picked", x);
  }

  const submitBookingPreference = async () => {
    updatePartnerBookingPreference();
  };

  ///GEt customer cart items
  function handleGetCustomerCart(searchVal) {
    console.error("Handle Get Customer");
    setLoading(true);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("json_token", json_token.token);

    setToken(json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);
    //get(`${endpoints.getSortedServices}?subcategory=all`

    fetch(`${endpoints.getCustomerCart}?id=${selectedCustomer._id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        // console.log("JSON:->", json);
        console.log("cart results:--->", json);
        setCartItems(json.cart);
        setTotalServicesDuration(calculateTotalDuration(json.cart));
        setTotalServicePrice(calculateTotalServicePrice(json.cart));
        //setPartnerServiceType();

        //json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  ///Get partner by ID
  ///GEt customer cart items
  function handleGetPartnerByID(x) {
    console.error("Handle Get Customer");
    setLoading(true);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    setToken(json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);
    //get(`${endpoints.getSortedServices}?subcategory=all`

    fetch(`${endpoints.getPartnerByID}/${selectedProvider._id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        // console.log("JSON:->", json);
        console.log("Partner By ID--->", json);
        setPartnerServiceType(json.service_type);
        setProviderLocation(json.location.address);

        //setCartItems(json.cart);
        // setTotalServicesDuration(calculateTotalDuration(json.cart));
        //setTotalServicePrice(calculateTotalServicePrice(json.cart));

        //json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  ///get time slots

  function handleGetTimeSlots(incoming_date) {
    console.error("Handle Get Customer");

    if (incoming_date == undefined) {
      incoming_date = new Date();
    }

    const dateString = incoming_date; // Example date string in "YYYY-MM-DD" format

    console.log("incomind date", dateString);

    // Parse the date string into a Date object
    const selectedBookingDate = new Date(dateString);

    console.log("parsed date", selectedBookingDate);

    const daysOfWeek = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    const dayIndex = selectedBookingDate.getDay();
    // Get the full name of the day using the day index
    const fullDayName = daysOfWeek[dayIndex];

    console.log("FULLDAY", fullDayName);

    setTimeslotsDay(fullDayName);

    setLoading(true);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("json_token", json_token.token);

    setToken(json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);
    //get(`${endpoints.getSortedServices}?subcategory=all`

    fetch(
      `${endpoints.getTimeSlots}?id=${selectedProvider._id}&day=${fullDayName}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json", // optional, depending on your API requirements
        },
      }
    )
      .then((response) => response.json())
      .then((json) => {
        // console.log("JSON:->", json);
        //console.log("cart results:--->", json.timeslots);
        setTimeSlots(json.timeslots);
        //setCartItems(json.cart);
        //json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  //

  const blue = {
    100: "#DAECFF",
    200: "#b6daff",
    400: "#3399FF",
    500: "#007FFF",
    600: "#0072E5",
    900: "#003A75",
  };

  const grey = {
    50: "#F3F6F9",
    100: "#E5EAF2",
    200: "#DAE2ED",
    300: "#C7D0DD",
    400: "#B0B8C4",
    500: "#9DA8B7",
    600: "#6B7A90",
    700: "#434D5B",
    800: "#303740",
    900: "#1C2025",
  };

  const steps = [
    "Choose Customer",
    "Choose Provider",
    "Service Selection",
    "Date and Time",
    "Check Out",
  ];

  const actionId = "primary-action-menu";

  const handleOpen_file = () => setOpenfile(true);
  const handleClose_file = () => setOpenfile(false);
  const handleClose_Bgfile = () => setOpenBgfile(false);

  const handleOpen_AddCompany = () => {
    setOpenAddCompany(true);
  };
  const handleOpen_EditProvider = () => {
    // formikEditPartnerRegistration.setValues({
    //   email: selectedRowData.email,
    //   first_name: selectedRowData.first_name,
    //   last_name: selectedRowData.last_name,
    //   email: selectedRowData.email,
    //   phone_number: selectedRowData.phone_number,
    //   main_businessName: selectedRowData.main_businessName,
    //   bio: selectedRowData.bio,
    // });

    // if (
    //   selectedRowData.partner_availability.monday.opening_time == "" ||
    //   selectedRowData.partner_availability.monday.closing_time == "" ||
    //   selectedRowData.partner_availability.tuesday.opening_time == "" ||
    //   selectedRowData.partner_availability.tuesday.closing_time == "" ||
    //   selectedRowData.partner_availability.wednesday.opening_time == "" ||
    //   selectedRowData.partner_availability.wednesday.closing_time == "" ||
    //   selectedRowData.partner_availability.thursday.opening_time == "" ||
    //   selectedRowData.partner_availability.thursday.closing_time == "" ||
    //   selectedRowData.partner_availability.friday.opening_time == "" ||
    //   selectedRowData.partner_availability.friday.closing_time == "" ||
    //   selectedRowData.partner_availability.saturday.opening_time == "" ||
    //   selectedRowData.partner_availability.saturday.closing_time == "" ||
    //   selectedRowData.partner_availability.sunday.opening_time == "" ||
    //   selectedRowData.partner_availability.sunday.closing_time == ""
    // ) {
    //   console.log("opening time is empty");
    // }

    // formikPartnerAvailability.setValues({
    //   monday_opening_time:
    //     selectedRowData.partner_availability.monday.opening_time,
    //   monday_closing_time:
    //     selectedRowData.partner_availability.monday.closing_time,

    //   tuesday_opening_time:
    //     selectedRowData.partner_availability.tuesday.opening_time,
    //   tuesday_closing_time:
    //     selectedRowData.partner_availability.tuesday.closing_time,

    //   wednesday_opening_time:
    //     selectedRowData.partner_availability.wednesday.opening_time,
    //   wednesday_closing_time:
    //     selectedRowData.partner_availability.wednesday.closing_time,

    //   thursday_opening_time:
    //     selectedRowData.partner_availability.thursday.opening_time,
    //   thursday_closing_time:
    //     selectedRowData.partner_availability.thursday.closing_time,

    //   friday_opening_time:
    //     selectedRowData.partner_availability.friday.opening_time,
    //   friday_closing_time:
    //     selectedRowData.partner_availability.friday.closing_time,

    //   saturday_opening_time:
    //     selectedRowData.partner_availability.saturday.opening_time,
    //   saturday_closing_time:
    //     selectedRowData.partner_availability.saturday.closing_time,

    //   sunday_opening_time:
    //     selectedRowData.partner_availability.sunday.opening_time,
    //   sunday_closing_time:
    //     selectedRowData.partner_availability.sunday.closing_time,
    // });

    //setPP(selectedRowData.profile_pic);
    // setBP(selectedRowData.logo);
    //setServiceType(selectedRowData.service_type);
    // setPaymentMethod(selectedRowData.payment_option);
    handleGetCustomer();
    handleGetProviders();
    console.warn("get all records called");

    setOpenEdit(true);
  };

  ///handleOpen_DeleteProvider

  // async const handleOpen_DeleteProvider = () => {

  async function handleOpen_DeleteProvider() {
    //setOpenEdit(false);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    console.log(selectedRowData);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.deleteProvider}/${selectedRowData._id}`,

        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          //body: JSON.stringify(partnerRegistrationObject),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        setOpenAddAdminSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  }

  const handleConfirm_pick = (x) => {
    console.log("selected customer", x);
    setSelectedCustomer(x);
    setOpenConfirmPick(true);
  };

  const handleConfirm_pick_provider = (x) => {
    console.log("selected provider", x);
    setSelectedProvider(x);
    setOpenConfirmPick_provider(true);
  };

  const handleConfirm_pick_service = (idx, x) => {
    setButtonStates((prevState) => {
      const newButtonStates = [...prevState];
      newButtonStates[idx] = {
        color: "#ff0000", // Red color
        text: "Remove",
      };
      return newButtonStates;
    });

    console.log("Service", x);
    console.log("Cart ID", itemID);
    setService(x);
    setOpenConfirmPick_service(true);
  };

  const handleClose_EditProvider = () => {
    setOpenEdit(false);
  };
  const handleClose_AddCompany = () => {
    setOpenAddCompany(false);
  };

  function handleChange(e) {
    // Access the file object
    const file = e.target.files[0];
    console.log("upload profile picture");

    handleUploadProfile(file);

    // Create a new FileReader
    const reader = new FileReader();

    // Set up the onload event to handle the file read operation
    reader.onload = () => {
      // Get the Base64 encoded data
      const base64Data = reader.result;

      // Assign the Base64 data to the src attribute of the image element
      if (profilePicRef.current) {
        profilePicRef.current.src = base64Data;
      }

      // setPP(base64Data);

      // Optionally, you can handle the Base64 data (e.g., store it in state)
      // console.log("Base64 encoded image:", base64Data);
    };

    // Read the contents of the file as a Data URL
    reader.readAsDataURL(file);
    setFile(URL.createObjectURL(e.target.files[0]));
  }

  const updateCustomerCart_CustomerDetails = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("Updating id", selectedRowData._id);

    console.log("updated data", data, json_token);

    const cartObject_Details = {
      provider_name:
        selectedProvider.first_name + " " + selectedProvider.last_name,
      provider_id: selectedProvider._id,
      provider_phone_number: selectedProvider.phone_number,
      provider_email: selectedProvider.email,
      provider_location: {
        latitude: selectedProvider.location.latitude,
        longitude: selectedProvider.location.longitude,
        address: selectedProvider.location.address,
      },
      provider_payment_option: selectedProvider.payment_option,
      provider_service_type: selectedProvider.service_type,
      provider_businessname: selectedProvider.main_businessName,

      //customer stuff
      customer_id: selectedCustomer._id,
      customer_name:
        selectedCustomer.first_name + " " + selectedCustomer.last_name,
      customer_phone_number: selectedCustomer.phone_number,
      customer_email: selectedCustomer.email,
      customer_location: {
        latitude: selectedCustomer.location.latitude,
        longitude: selectedCustomer.location.longitude,
        address: selectedCustomer.location.address,
      },
    };

    console.log("cardObject customer details", cartObject_Details);

    handleGetSortedPartnerServices();

    try {
      setLoading(true); // Set loading to true when making the request

      // const bookingTime = {
      //   booking_time: selectedTimeSlot,
      //   booking_date: humanReadableDate,
      // };
      // console.log("bookint time object", bookingTime);

      const response = await fetch(
        `${endpoints.addPendingBooking}/${selectedCustomer._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(cartObject_Details),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        //console.log("json", response.json());

        console.log("Records updated successfully");
        response.json().then((data) => {
          console.log("json", data);

          if (data.status == "0") {
            console.error("missing fields");
            return;
          }
        });

        //   setOpenImageSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }

    // try {
    //   //setLoading(true); // Set loading to true when making the request
    //   //setOpenAddAdminSuccess(true);

    //   const response = await fetch(
    //     `${endpoints.addPendingBooking}/${selectedCustomer._id}`,

    //     {
    //       method: "PUT",
    //       headers: {
    //         "Content-Type": "application/json",
    //         Authorization: `Bearer ${json_token.token}`,
    //       },
    //       body: JSON.stringify(cartObject_Details),
    //     }
    //   );
    //   console.log("response after edit:->>", response.json());

    //   // setCartNumber(responseData.pending_booking.cart.length);
    //   //console.error("itemID:-",responseData.item_id);
    //   //setItemId(responseData.item_id);
    // } catch (error) {
    //   console.error("Error sending data to the backend:", error);
    // } finally {
    //   //setLoading(false); // Set loading to false after the request is complete (success or error)
    //   //handleClose_AddCompany();
    // }
  };

  const updateCustomerCart_ServiceDetails = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    console.log("updated data", data, json_token);

    const ServiceDetails = {
      // provider_name: selectedProvider.first_name,
      // provider_id: selectedProvider._id,
      // provider_phone_number: selectedProvider.phone_number,
      // provider_email: selectedProvider.email,
      // provider_location: {
      //   latitude: selectedProvider.location.latitude,
      //   longitude: selectedProvider.location.longitude,
      // },
      // provider_payment_option: selectedProvider.payment_option,

      // //customer stuff
      // customer_id: selectedCustomer._id,
      // customer_name: selectedCustomer.first_name,
      // customer_phone_number: selectedCustomer.phone_number,
      // customer_email: selectedCustomer.email,
      // customer_location: {
      //   latitude: selectedCustomer.location.latitude,
      //   longitude: selectedCustomer.location.longitude,

      type: service.type,
      status: service.status,
      description: service.description,
      subcategory: service.subcategory,
      created_at: service.created_at,
      price: service.price,
      ServiceDuration: service.duration,

      // },
    };

    console.log("cardObject customer details", ServiceDetails);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      //{{LOCAL_BASE_URL}}/add-services-to-cart?id=66225643fae1a42124a52d24&cart_item_id=f4d2feaa-7714-415e-b5c4-0d84c915a513

      const response = await fetch(
        //  `${endpoints.updateCustomerCart}/${selectedCustomer._id}`,
        //`${endpoints.addServiceToBooking}?id=${selectedCustomer._id}&cart_item_id=${itemID}`,
        `${endpoints.addServiceToBooking}/${selectedCustomer._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(ServiceDetails),
        }
      );
      console.log("response after edit:->>", response);
      const responseData = await response.json(); // Extract JSON data from the response body
      console.log(responseData);
      //setItemId(responseData.item_id);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  };

  function handleAddCustomer(inc_customer) {
    console.log("incoming customer:->", inc_customer);
    console.log("Current partner", selectedRowData);

    const cartObject = {
      provider_name: selectedRowData.first_name,
      provider_id: selectedRowData._id,
      provider_phone_number: selectedRowData.phone_number,
      provider_email: selectedRowData.email,
      provider_location: {
        latitude: selectedRowData.location.latitude,
        longitude: selectedRowData.location.longitude,
      },
      provider_payment_option: selectedRowData.payment_option,

      //customer stuff
      customer_id: inc_customer._id,
      customer_name: inc_customer.first_name,
      customer_phone_number: inc_customer.phone_number,
      customer_email: inc_customer.email,
      customer_location: {
        latitude: "inc_customer.location.latitude",
        longitude: " inc_customer.location.longitude",
      },
    };

    updateCustomerCart(cartObject);
  }

  const handleUploadProfile = async (file) => {
    try {
      setLoading(true);
      const formData = new FormData();
      formData.append("profile", file);

      // Replace 'your-upload-endpoint' with your actual upload endpoint
      const response = await axios.post(endpoints.uploadProfilePic, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${stateToken}`,
        },
      });

      console.log("Upload successful:", response.data);
      setPP(response.data.image_url);
      console.log("ID", selectedRowData._id);
      handleUpdateProfilePicURL(response.data.image_url);
    } catch (error) {
      console.error("Error uploading file:", error);
    } finally {
      console.log("Request completed."); // This will be executed regardless of success or failure
      setLoading(false);
    }
  };

  //handleUpdateLogoPic

  const handleUploadLogo = async (file) => {
    try {
      const formData = new FormData();
      formData.append("logo", file);

      // Replace 'your-upload-endpoint' with your actual upload endpoint
      const response = await axios.post(endpoints.uploadLogoPic, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${stateToken}`,
        },
      });

      console.log("Upload successful:", response.data);
      setBP(response.data.image_url);
      console.log("ID", selectedRowData._id);
      handleUpdateLogoPicURL(response.data);
    } catch (error) {
      console.error("Error uploading file:", error);
    }
  };

  const handleUpdateProfilePicURL = async (generatedURL) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("generatedURL", generatedURL);

    const profileURL = {
      profile_pic: generatedURL,
    };

    console.log("profileURLOBject", profileURL);

    try {
      setLoading(true); // Set loading to true when making the request

      const response = await fetch(
        `${endpoints.updateProfilePicURL}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(profileURL),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        setOpenImageSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  const updateBookingTime = async (generatedURL) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("generatedURL", generatedURL);

    const bookingTime = {
      booking_time: selectedTimeSlot,
      booking_date: selectedBookingDate,
    };

    console.log("profileURLOBject", logoURL);

    try {
      setLoading(true); // Set loading to true when making the request

      const response = await fetch(
        `${endpoints.updateLogoPicURL}/${selectedCustomer._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(bookingTime),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        setOpenImageSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  async function activateCustomerFunction() {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
    console.log("json_token", stored_json_data);

    const ongoing_json = {
      // first_name: selectedRowData.first_name,

      password: "newPassword",
    };

    try {
      // setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${stored_json_data.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };

      const response = await fetch(
        `${endpoints.activateCustomer}?id=${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${stored_json_data.token}`,
          },
          body: JSON.stringify(ongoing_json),
        }
      );
      console.log("response after edit:->>", response);

      response.json().then((data) => {
        console.log("json", data);

        if (data.status == "0") {
          console.log("Error detected");
          setErrorMsg("Something went wrong");
          setGeneralError(true);
          return;
        } else if (data.status == "1") {
          setOpenSuccess(true);
          setSuccessMsg("Activation Success!");
          setGeneralSuccess(true);
          setTimeout(() => {
            setOpenAddCompany(false);
            reloadPage();
          }, 2000);
        }
      });
    } catch (error) {
      console.error("Error sending data to the backend:", error);

      if (error.message == "Network Error") {
        console.log("Network Error");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      } else {
        console.log("Error sending data to the backend:");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      }
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  }

  function handleOpenActivateModal() {
    //setOpenDeleteRecordModal(true);
    console.log("activate customer");
    setOpenActivateCustomerRecordModal(true);
  }

  function handleClose_popActivateModal() {
    //setOpenDeleteRecordModal(true);
    setOpenActivateCustomerRecordModal(false);
  }

  function handleClickTimeslot(x) {
    setSelectedTimeSlot(x);
    setSuccessMsg("Success");
    setGeneralSuccess(true);
  }

  const handleUpdateLogoPicURL = async (generatedURL) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("generatedURL", generatedURL);

    const logoURL = {
      logo: generatedURL,
    };

    console.log("profileURLOBject", logoURL);

    try {
      setLoading(true); // Set loading to true when making the request

      const response = await fetch(
        `${endpoints.updateLogoPicURL}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(logoURL),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        setOpenImageSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  async function submitBookingDateTime() {
    console.error("FInish has been called!");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);

    let humanReadableDate = "";

    console.log("selectedBookingDate", selectedBookingDate);
    console.log("selectedTimeSlots", selectedTimeSlot);
    if (selectedBookingDate == undefined || humanReadableDate == "") {
      console.log("booking date is undefined", selectedBookingDate);
      const date = new Date();

      const options = {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        // hour: "numeric",
        // minute: "numeric",
        // second: "numeric",
        // timeZoneName: "short",
      };

      humanReadableDate = date.toLocaleString("en-US", options);
      console.error("human Readable Date:->", humanReadableDate);
      setSelectedBookingDate(humanReadableDate);
    }

    if (selectedTimeSlot == undefined) {
      console.error("Cannot proceed without Time or Date ");
      return;
    }
    try {
      setLoading(true); // Set loading to true when making the request

      const bookingTime = {
        booking_time: selectedTimeSlot,
        booking_date: humanReadableDate,
      };
      console.log("bookint time object", bookingTime);

      const response = await fetch(
        `${endpoints.addBookingTime}?id=${selectedCustomer._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(bookingTime),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        //console.log("json", response.json());

        console.log("Records updated successfully");
        response.json().then((data) => {
          console.log("json", data);

          if (data.status == "0") {
            console.error("missing fields");
            return;
          }

          const newActiveStep =
            isLastStep() && !allStepsCompleted()
              ? // It's the last step, but not all steps have been completed,
                // find the first step that has been completed
                steps.findIndex((step, i) => !(i in completed))
              : activeStep + 1;

          // updateCustomerCart_CustomerDetails();
          handleGetTimeSlots();
          setActiveStep(newActiveStep);

          // setOpenConfirmPick_provider(false);
          //setOpenCartModal(false);

          handleComplete();

          handleGetCustomerCart();
          handleGetPartnerByID();
        });

        //   setOpenImageSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  }

  //handleUploadLogo

  function handleChange2(e) {
    // Access the file object
    const bgfile = e.target.files[0];

    handleUploadLogo(bgfile);

    // Create a new FileReader
    const reader = new FileReader();

    // Set up the onload event to handle the file read operation
    reader.onload = () => {
      // Get the Base64 encoded data
      const base64Data = reader.result;

      // Assign the Base64 data to the src attribute of the image element
      if (profilePicRef.current) {
        profilePicRef.current.src = base64Data;
      }

      // setPP(base64Data);
      // setBP(base64Data);

      // Optionally, you can handle the Base64 data (e.g., store it in state)
      // console.log("Base64 encoded image:", base64Data);
    };

    // Read the contents of the file as a Data URL
    reader.readAsDataURL(bgfile);
    //setBgFile(URL.createObjectURL(e.target.files[0]));
  }

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;
    setActiveStep(newActiveStep);
  };

  const handleNextFromCustomer = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;
    setActiveStep(newActiveStep);

    setOpenConfirmPick(false);
    handleComplete(); //handle complete step
  };

  const handleNextFromProvider = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;

    updateCustomerCart_CustomerDetails();

    setActiveStep(newActiveStep);

    setOpenConfirmPick_provider(false);

    handleComplete(); //handle complete step
  };

  const handleNextFromServiceSelection = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;

    // updateCustomerCart_CustomerDetails();
    handleGetTimeSlots();
    setActiveStep(newActiveStep);

    // setOpenConfirmPick_provider(false);
    setOpenCartModal(false);

    handleComplete(); //handle complete step
  };

  const handleNextFromService = () => {
    // const newActiveStep =
    //   isLastStep() && !allStepsCompleted()
    //     ? // It's the last step, but not all steps have been completed,
    //       // find the first step that has been completed
    //       steps.findIndex((step, i) => !(i in completed))
    //     : activeStep + 1;
    // setActiveStep(newActiveStep);
    // updateCustomerCart_ServiceDetails();

    updateCustomerCart_ServiceDetails();

    setOpenConfirmPick_service(false);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    setCompleted(newCompleted);
    handleNext();
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted({});
  };

  const items = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={actionId}
      keepMounted
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      open={isOPen}
      onClose={handleActionMenuClose}
    >
      {/* <MenuItem className="flex gap-2" onClick={handleOpen_EditProvider}>
        <BookIcon fontSize="13px" /> Book
      </MenuItem> */}
      <MenuItem className="flex gap-2">
        <MdAirplanemodeActive /> Mark Ongoing
      </MenuItem>
      <MenuItem className="flex gap-2">
        <MdOutlineAirplanemodeInactive /> Cancel Booking
      </MenuItem>

      <MenuItem className="flex gap-2">
        <MdOutlineAirplanemodeInactive /> Mark Completed
      </MenuItem>
      <MenuItem className="flex gap-2" onClick={handleOpen_DeleteProvider}>
        <RiDeleteBinFill /> Delete
      </MenuItem>

      {selectedRowData && selectedRowData.status != "UPCOMING" && (
        <MenuItem className="flex gap-2" onClick={handleOpenActivateModal}>
          <MdAirplanemodeActive /> Activate
        </MenuItem>
      )}
    </Menu>
  );
  function handleActionMenuClose() {
    setIsOpen(false);
  }
  function handleActionMenuOpen() {
    setIsOpen(true);
  }

  function filterBookings(searchVal) {
    const data = JSON.parse(localStorage.getItem("bookingData")) || [];

    console.log("To be filtered users:->", data);

    if (searchVal === undefined || searchVal.trim().length === 0) {
      setRows(data);
    } else {
      const searchTerm = searchVal.toLowerCase().split(" ");

      const results = data.filter((user) => {
        const booking_id = user.booking_id ? user.booking_id.toLowerCase() : "";
        const customer_name = user.customer_name
          ? user.customer_name.toLowerCase()
          : "";
        const provider_name = user.provider_name
          ? user.provider_name.toLowerCase()
          : "";
        // const provider_businessname = user.provider_businessname
        //   ? user.provider_businessname
        //   : "";
        const companyName = user.provider_businessname
          ? user.provider_businessname.toLowerCase()
          : "";

        return searchTerm.every(
          (term) =>
            booking_id.includes(term) ||
            customer_name.includes(term) ||
            provider_name.includes(term) ||
            companyName.includes(term)
          //   companyName.includes(term)
        );
      });

      setRows(results);
    }
  }

  function filterUsers(searchVal) {
    const data = JSON.parse(localStorage.getItem("activeUserData")) || [];

    console.log("To be filtered users:->", data);

    if (searchVal === undefined || searchVal.trim().length === 0) {
      setActiveCustomer(data);
    } else {
      const searchTerm = searchVal.toLowerCase().split(" ");

      const results = data.filter((user) => {
        const firstName = user.first_name ? user.first_name.toLowerCase() : "";
        const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        const email = user.email ? user.email.toLowerCase() : "";
        const phoneNumber = user.phone_number ? user.phone_number : "";
        const companyName = user.main_businessName
          ? user.main_businessName.toLowerCase()
          : "";

        return searchTerm.every(
          (term) =>
            firstName.includes(term) ||
            lastName.includes(term) ||
            email.includes(term) ||
            phoneNumber.includes(term) ||
            companyName.includes(term)
        );
      });

      setActiveCustomer(results);
    }
  }

  ///
  function filterProviders(searchVal) {
    const data = JSON.parse(localStorage.getItem("activeProviderData")) || [];

    console.log("To be filtered users:->", data);

    if (searchVal === undefined || searchVal.trim().length === 0) {
      setActiveProvider(data);
    } else {
      const searchTerm = searchVal.toLowerCase().split(" ");

      const results = data.filter((user) => {
        const firstName = user.first_name ? user.first_name.toLowerCase() : "";
        const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        const email = user.email ? user.email.toLowerCase() : "";
        const phoneNumber = user.phone_number ? user.phone_number : "";
        const companyName = user.main_businessName
          ? user.main_businessName.toLowerCase()
          : "";

        return searchTerm.every(
          (term) =>
            firstName.includes(term) ||
            lastName.includes(term) ||
            email.includes(term) ||
            phoneNumber.includes(term) ||
            companyName.includes(term)
        );
      });

      setActiveProvider(results);
    }
  }

  function handleGetAllRecords(searchVal) {
    setLoading(true);
    console.warn("ALL Customers");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);
    setToken(json_token.token); // Assuming setToken is a function to set token somewhere in your code

    axios
      .get(endpoints.getAllBookings, {
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        // console.log("Response data: ", response.data);
        const json = response.data;
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

        localStorage.setItem("bookingData", JSON.stringify(json));
        console.log("booking data set globally...");
        console.log("json", json);
        setRows(json);

        const calculateCumulativeTotal = (cart) => {
          return cart.reduce((accumulator, service) => {
            return (
              accumulator + parseInt(service.price) * parseInt(service.number)
            );
          }, 0);
        };

        // Calculate cumulative total for all bookings initially
        const initialTotal = json.reduce((accumulator, booking) => {
          return accumulator + calculateCumulativeTotal(booking.cart);
        }, 0);

        console.log("inital total", initialTotal);

        // const calculateCumulativeTotal = (cart) => {
        //   return cart.reduce((accumulator, service) => {
        //     return (
        //       accumulator + parseInt(service.price) * parseInt(service.number)
        //     );
        //   }, 0);
        // };

        //console.log("total Services", calculateCumulativeTotal(json.cart))

        // if (searchVal === undefined || searchVal.trim().length === 0) {
        //   console.log("json", json);
        //   setRows(json);
        // } else {
        //   const results = json.filter((user) => {
        //     const firstName = user.first_name
        //       ? user.first_name.toLowerCase()
        //       : "";
        //     const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        //     const email = user.email ? user.email.toLowerCase() : "";
        //     const phoneNumber = user.phone_number ? user.phone_number : "";
        //     const CompanyName = user.main_businessName
        //       ? user.main_businessName.toLowerCase()
        //       : "";

        //     const searchTerm = searchVal.toLowerCase();

        //     return (
        //       firstName.includes(searchTerm) ||
        //       lastName.includes(searchTerm) ||
        //       email.includes(searchTerm) ||
        //       phoneNumber.includes(searchTerm) ||
        //       CompanyName.includes(searchTerm)
        //     );
        //   });

        //   setRows(results);
        // }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleDateChange = (newDate) => {
    // You can perform any additional logic here with the selected date

    const date = new Date(newDate);

    const options = {
      year: "numeric",
      month: "numeric",
      day: "numeric",
      // hour: "numeric",
      // minute: "numeric",
      // second: "numeric",
      // timeZoneName: "short",
    };

    const humanReadableDate = date.toLocaleString("en-GB", options);
    // console.log();
    setSelectedBookingDate(humanReadableDate);

    handleGetTimeSlots(date);
  };

  function handleOpenActivateModal() {
    //setOpenDeleteRecordModal(true);
    console.log("activate customer");
    setOpenActivateCustomerRecordModal(true);
  }
  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);

    filterBookings(event.target.value);
    //handleGetAllRecords(event.target.value);
    // handleGetCustomer(event.target.value);
  };

  const handleSearchChange_Providers = (event) => {
    setSearchTerm_providers(event.target.value);

    filterProviders(event.target.value);
    //handleGetAllRecords(event.target.value);
    //handleGetProviders(event.target.value);
  };

  ///handleSearchChange_Customer

  const handleSearchChange_Customer = (event) => {
    //setSearchTerm_providers(event.target.value);

    setSearchTerm_customers(event.target.value);
    filterUsers(event.target.value);
    //handleGetAllRecords(event.target.value);
    // handleGetProviders(event.target.value);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const submitFinalBooking = async (data) => {
    // console.log("main object", partnerRegistrationObject);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("json_token", json_token.token);

    try {
      setLoading(true);
      const response = await axios.post(
        //endpoints.finalizeBooking,
        `${endpoints.finalizeBooking}?id=${selectedCustomer._id}`,
        {
          headers: {
            Authorization: `Bearer ${json_token.token}`,
            "Content-Type": "application/json",
          },
        }
        //partnerRegistrationObject
      );

      console.log("Response from the backend:", response.data);

      if (response.data.status == "Error") {
        console.log("Error detected");
        setErrorMsg("Something went wrong");
        setGeneralError(true);
        return;
      } else if (response.data.status == "Success") {
        //setOpenSuccess(true);
        setSuccessMsg("Booked Successfully!");
        setGeneralSuccess(true);
        // setTimeout(() => {
        //   setOpenAddCompany(false);
        //   reloadPage();
        // }, 2000);
      }
      //setOpenAddAdminSuccess(true);

      // Reload the page only if there's no error
      // setTimeout(() => {
      //   reloadPage();
      // }, 2000);

      // response.json().then((data) => {
      //   console.log("json", data);

      //   if (data.status == "Error") {
      //     console.log("Error detected");
      //     setErrorMsg("Something went wrong");
      //     setGeneralError(true);
      //     return;
      //   } else if (data.status == "Success") {
      //     //setOpenSuccess(true);
      //     setSuccessMsg("Booked Successfully!");
      //     setGeneralSuccess(true);
      //     setTimeout(() => {
      //       setOpenAddCompany(false);
      //       reloadPage();
      //     }, 2000);
      //   }
      // });
    } catch (error) {
      console.error("Error sending data to the backend:", error);
      console.log("Error detected");
      setErrorMsg("Something went wrong");
      setGeneralError(true);
      //setOpenAddAdminFailed(true);
      // setOpenAddCompany(true); // Open the pop-up modal on error
    } finally {
      setLoading(false);
    }
  };

  function filterPartners(filter) {
    setLoading(true);

    console.log("sort services functions called", filter);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("json_token", json_token.token);

    axios
      //.get(endpoints.getBeautyServices, {
      .get(`${endpoints.filterBookingStatus}?status=${filter}`, {
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        if (response.data) {
          const json = response.data;
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log(
            "Response filtered data: after sorting:--> ",
            response.data
          );
          setRows(json);
        } else {
          setRows([]);
        }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoading(false);
      });
    /// } else {
    //console.log("Subcategiry empty", subcategory);
    // }
  }

  const updatePartnerRegistrationData = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    const partnerRegistrationObject = {
      email: data.email.trim(),
      //password: data.password.trim(),
      first_name: data.first_name,
      last_name: data.last_name,
      phone_number: data.phone_number,
      main_businessName: data.main_businessName,
      bio: data.bio,
      profile_pic: pp.image_url,
      logo: bp,
    };

    console.log("main object", partnerRegistrationObject);

    try {
      setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.editProviderRegisrationInfo}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(partnerRegistrationObject),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        setOpenProviderUpdateSuccess(true);
      } else {
        console.error("Failed to update record information");
        setOpenProviderUpdateFailed(true);
      }
    } catch (error) {
      setOpenProviderUpdateFailed(true);

      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false);

      // if (response.status === 200) {
      //   console.log("Records updated successfully");

      // } else {
      //   setOpenAddAdminFailed(true);
      //   setOpenAddCompany(true);
      //   console.error("Failed to update record information");
      // }

      // Set loading to false after the request is complete (success or error)
    }
    setTimeout(() => {
      setOpenAddCompany(false);
      reloadPage();
    }, 2000);
  };

  const handleChangeType = (event) => {
    console.log("event selected:->", event.target.value);
    setBookingStatus(event.target.value);

    if (event.target.value) {
      filterPartners(event.target.value);
    }
  };

  //updatePartnerAvailabilityData
  const updatePartnerAvailabilityData = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    const availability = {
      monday: {
        opening_time: data.monday_opening_time,
        closing_time: data.monday_closing_time,
      },
      tuesday: {
        opening_time: data.tuesday_opening_time,
        closing_time: data.tuesday_closing_time,
      },
      wednesday: {
        opening_time: data.wednesday_opening_time,
        closing_time: data.wednesday_closing_time,
      },
      thursday: {
        opening_time: data.thursday_opening_time,
        closing_time: data.thursday_closing_time,
      },
      friday: {
        opening_time: data.friday_opening_time,
        closing_time: data.friday_closing_time,
      },
      saturday: {
        opening_time: data.saturday_opening_time,
        closing_time: data.saturday_closing_time,
      },
      sunday: {
        opening_time: data.sunday_opening_time,
        closing_time: data.sunday_closing_time,
      },
    };

    console.log("main object", availability);

    try {
      setLoading(true); // Set loading to true when making the request
      setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.addProviderAvailability}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(availability),
        }
      );
      console.log("response after edit:->>", response);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  React.useEffect(() => {
    handleGetAllRecords();
    handleGetCustomer();

    //handleGetPartner("660e5e18bc8e1f4458c3b9ac");
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  const formikNewAdmin = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      email: "",
      admin_type: "",
      country: "",
      timezone: "",
      password: "",
      main_businessName: "",
      bio: "",
      logo: "",
      service_type: "",
      payment_option: "",
      social_media: "",
      location: {
        latitude: "",
        longitude: "",
      },

      partner_availability: {
        monday: {
          opening_time: "",
          closing_time: "",
        },
        tuesday: {
          opening_time: "",
          closing_time: "",
        },
        wednesday: {
          opening_time: "",
          closing_time: "",
        },
        thursday: {
          opening_time: "",
          closing_time: "",
        },
        friday: {
          opening_time: "",
          closing_time: "",
        },
        saturday: {
          opening_time: "",
          closing_time: "",
        },
        sunday: {
          opening_time: "",
          closing_time: "",
        },
      },
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      // postAdminRegistrationData(
      //   values.first_name.trim(),
      //   values.last_name.trim(),
      //   values.email.trim(),
      //   values.admin_type.trim(),
      //   values.password.trim()
      // );
    },
  });

  const formikPartnerRegistration = useFormik({
    initialValues: {
      email: "",
      password: "",
      first_name: "",
      last_name: "",
      phone_number: "",
      main_businessName: "",
      bio: "",
      profilePicture: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);
      console.log("profile image", profilePicRef.current);

      postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  //formikEditPartnerRegistration

  const formikEditPartnerRegistration = useFormik({
    initialValues: {
      email: "",
      password: "",
      first_name: "",
      last_name: "",
      phone_number: "",
      main_businessName: "",
      bio: "",
      profilePicture: "",
      logo: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);
      console.log("profile image", profilePicRef.current);

      //postPartnerRegistrationData(values);
      updatePartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  const formikPartnerAvailability = useFormik({
    initialValues: {
      monday_opening_time: "",
      monday_closing_time: "",

      tuesday_opening_time: "",
      tuesday_closing_time: "",

      wednesday_opening_time: "",
      wednesday_closing_time: "",

      thursday_opening_time: "",
      thursday_closing_time: "",

      friday_opening_time: "",
      friday_closing_time: "",

      saturday_opening_time: "",
      saturday_closing_time: "",

      sunday_opening_time: "",
      sunday_closing_time: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data partner availability:-", values);

      updatePartnerAvailabilityData(values);

      //postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  //formikPartnerBookingPreference

  const formikPartnerBookingPreference = useFormik({
    initialValues: {},
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data partner availability:-", values);

      updatePartnerAvailabilityData(values);

      //postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  return (
    <>
      {loader && <Loader2 />}

      <Modal
        open={activateCustomer}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className=" m-4">
              <span className="font-bold m-2">Activation Confirmation</span>
              <br></br>{" "}
              <span>Are you sure you want to Activate this Customer?</span>
            </h2>

            {/* <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            /> */}
          </div>

          <form>
            <div className="flex flex-col gap-3 ">
              <div className="flex flex-row gap-3 mt-3"></div>
              <div className="flex flex-row gap-4">
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={activateCustomerFunction}
                >
                  yes
                </Button>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={handleClose_popActivateModal}
                >
                  No
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />

      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />
      {/* start of Add Modal */}
      <Modal open={openConfirmPick} onClose={() => setOpenConfirmPick(false)}>
        <Box sx={style_box3}>
          <div className="flex flex-row justify-end m-2 py-2">
            <IoMdCloseCircle
              size={18}
              onClick={() => setOpenConfirmPick(false)}
              cursor={"pointer"}
              color="#000"
            />
          </div>

          <div className="flex flex-col gap-4 items-center justify-center m-2 py-2">
            <div className="flex flex-row gap-2">
              <Typography sx={{ color: "#000", fontSize: [15] }}>
                Proceed with customer:
              </Typography>
              <Typography
                sx={{ color: "#000", fontSize: [15], fontWeight: "bold" }}
              >
                {selectedCustomer && selectedCustomer.first_name}
              </Typography>
              <Typography
                sx={{ color: "#000", fontSize: [15], fontWeight: "bold" }}
              >
                {selectedCustomer && selectedCustomer.last_name}
              </Typography>
              <Typography sx={{ color: "#000", fontSize: [15] }}>?</Typography>
            </div>
            <div className="flex flex-row gap-4">
              <Button
                variant="outlined"
                size="small"
                onClick={() => setOpenConfirmPick(false)}
                color="error"
              >
                cancel
              </Button>
              <Button
                variant="contained"
                size="small"
                onClick={handleNextFromCustomer}
              >
                Proceed
              </Button>
            </div>
          </div>
        </Box>
      </Modal>
      <Modal
        open={openConfirmPick_service}
        onClose={() => setOpenConfirmPick_service(false)}
      >
        <Box sx={style_box3}>
          <div className="flex flex-row justify-end m-2 py-2">
            <IoMdCloseCircle
              size={18}
              onClick={() => setOpenConfirmPick_service(false)}
              cursor={"pointer"}
              color="#000"
            />
          </div>

          <div className="flex flex-col gap-4 items-center justify-center m-2 py-2">
            <Typography sx={{ color: "#000", fontSize: [15] }}>
              Proceed with the selected Service ?
            </Typography>
            <div className="flex flex-row gap-4">
              <Button
                variant="outlined"
                size="small"
                onClick={() => setOpenConfirmPick_service(false)}
                color="error"
              >
                cancel
              </Button>
              <Button
                variant="contained"
                size="small"
                onClick={handleNextFromService}
              >
                Proceed
              </Button>
            </div>
          </div>
        </Box>
      </Modal>
      <Modal
        open={openConfirmPick_provider}
        onClose={() => setOpenConfirmPick_provider(false)}
      >
        <Box sx={style_box3}>
          <div className="flex flex-row justify-end m-2 py-2">
            <IoMdCloseCircle
              size={18}
              onClick={() => setOpenConfirmPick_provider(false)}
              cursor={"pointer"}
              color="#000"
            />
          </div>

          <div className="flex flex-col gap-4 items-center justify-center m-2 py-2">
            <div className="flex flex-row gap-2">
              <Typography sx={{ color: "#000", fontSize: [15] }}>
                Proceed with Provider :
              </Typography>
              <Typography
                sx={{ color: "#000", fontSize: [15], fontWeight: "bold" }}
              >
                {selectedProvider && selectedProvider.first_name}
              </Typography>
              <Typography
                sx={{ color: "#000", fontSize: [15], fontWeight: "bold" }}
              >
                {selectedProvider && selectedProvider.last_name}
              </Typography>
              <Typography sx={{ color: "#000", fontSize: [15] }}>?</Typography>
            </div>
            <div className="flex flex-row gap-4">
              <Button
                variant="outlined"
                size="small"
                onClick={() => setOpenConfirmPick_provider(false)}
                color="error"
              >
                cancel
              </Button>
              <Button
                variant="contained"
                size="small"
                onClick={handleNextFromProvider}
              >
                Proceed
              </Button>
            </div>
          </div>
        </Box>
      </Modal>

      <Modal
        open={openAddCompany}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <>
            <div className="flex flex-row justify-between m-2 py-2">
              <Typography sx={{ color: "#000", fontSize: [18] }}>
                Add Provider
              </Typography>
              <IoMdCloseCircle
                size={18}
                onClick={handleClose_AddCompany}
                cursor={"pointer"}
                color="#000"
              />
            </div>
            <form onSubmit={formikPartnerRegistration.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    variant="outlined"
                    type="text"
                    id="first_name"
                    name="first_name"
                    label="Fist Name"
                    placeholder="First Name"
                    value={formikPartnerRegistration.values.first_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />

                  <TextField
                    type="text"
                    id="last_name"
                    name="last_name"
                    label="Last Name"
                    placeholder="Last Name"
                    value={formikPartnerRegistration.values.last_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    type="text"
                    id="email"
                    name="email"
                    label="Email"
                    placeholder=" Email"
                    value={formikPartnerRegistration.values.email}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                  <TextField
                    id="phone_number"
                    name="phone_number"
                    label="Phone Number"
                    placeholder="Phone Number"
                    value={formikPartnerRegistration.values.phone_number}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>

              <div className="flex flex-row gap-3 ">
                <TextField
                  type="text"
                  id="main_businessName"
                  name="main_businessName"
                  label="Business Name"
                  placeholder="Business Name"
                  value={formikPartnerRegistration.values.main_businessName}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
                <TextField
                  id="password"
                  name="password"
                  label="Password"
                  placeholder="Password"
                  value={formikPartnerRegistration.values.password}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Bio description </FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="bio"
                  id="bio"
                  name="bio"
                  label="Bio"
                  placeholder="Bio"
                  value={formikPartnerRegistration.values.bio}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-3 mt-3">
                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>
          </>
        </Box>
      </Modal>

      <Modal open={openCartModal}>
        <Box sx={style_box3}>
          <div className="my-3 flex flex-row justify-between">
            <p className="text-black font-bold">Cart ITems</p>
            <IoMdCloseCircle
              size={18}
              onClick={() => setOpenCartModal(false)}
              cursor={"pointer"}
              color="#000"
            />
          </div>

          <div>
            {cartItems &&
              cartItems.map((object, index) => (
                <div key={index} className="flex flex-row gap-8">
                  <p className="text-black text-xs">
                    {index + 1}: {object.type}
                  </p>
                  <p className="text-black text-xs">
                    {" "}
                    {object.service_duration}hrs
                  </p>

                  <p className="text-black text-xs"> KES: {object.price}</p>

                  <Button
                    variant="outlined"
                    color="error"
                    onClick={() => remove_ServiceDetailsfromCart(object)}
                  >
                    remove
                  </Button>
                </div>
              ))}
          </div>
          <Button onClick={handleNextFromServiceSelection}>Finish</Button>
        </Box>
      </Modal>

      {/* End fo Add Modal */}
      <Modal
        open={openEdit}
        onClose={handleClose_EditProvider}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box} className="select-none">
          <div className="flex flex-row justify-between m-2 py-2">
            <Typography
              sx={{ color: "#000", fontSize: [18, 24], fontWeight: "bold" }}
            >
              Book Services
            </Typography>
            <IoMdCloseCircle
              size={18}
              onClick={handleClose_EditProvider}
              cursor={"pointer"}
              color="#000"
            />
          </div>
          <Stepper nonLinear activeStep={activeStep}>
            {steps.map((label, index) => (
              <Step key={label} completed={completed[index]}>
                <StepButton color="inherit" onClick={handleStep(index)}>
                  {label}
                </StepButton>
              </Step>
            ))}
          </Stepper>
          <div>
            {allStepsCompleted() ? (
              <React.Fragment>
                <Typography sx={{ mt: 2, mb: 1 }}>
                  All steps completed - you&apos;re finished
                </Typography>
                <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                  <Box sx={{ flex: "1 1 auto" }} />
                  <Button onClick={handleReset}>Reset</Button>
                </Box>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Typography
                  sx={{
                    mt: 2,
                    mb: 1,
                    py: 1,
                    color: "#000",
                  }}
                >
                  {/* Step {activeStep + 1} */}
                  {activeStep === 0 ? (
                    // <p>First form</p>
                    <>
                      <div className=" sm:mt-8 mt-1 ">
                        <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                          <SearchIconWrapper>
                            <SearchIcon sx={{ color: "#616569" }} />
                          </SearchIconWrapper>
                          <StyledInputBase
                            value={searchTerm_customers}
                            onChange={handleSearchChange_Customer}
                            placeholder="Search Customer…"
                            inputProps={{ "aria-label": "search" }}
                          />
                        </Search>
                      </div>

                      <div>
                        <div className="flex flex-row items-center">
                          <Typography
                            sx={{
                              fontWeight: "bold",
                              fontSize: [14, 18],
                              margin: 1,
                            }}
                          >
                            Customers
                          </Typography>
                          <RiAccountPinCircleFill
                            size={24}
                            className="text-gray-400"
                          />
                        </div>
                        <ul
                          className="m-2 rounded shadow-lg py-2"
                          style={{
                            overflowY: "scroll",
                            maxHeight: "40vh",
                          }}
                        >
                          {activeCustomers &&
                            activeCustomers.length > 0 &&
                            activeCustomers.map((item, index) => (
                              <LightTooltip title="Select Customer">
                                <li
                                  key={index}
                                  // onClick={() => handleAddCustomer(item)}
                                  onClick={() => handleConfirm_pick(item)}
                                  className="py-3 border-b cursor-pointer border-gray-300 hover:bg-slate-300 duration-200 ease-linear"
                                >
                                  <div className="flex flex-row justify-between mx-2">
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.first_name}
                                    </Typography>
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.last_name}
                                    </Typography>
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.phone_number}
                                    </Typography>
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.email}
                                    </Typography>
                                  </div>
                                </li>
                              </LightTooltip>
                            ))}
                        </ul>
                      </div>

                      {/* <form
                        onSubmit={formikPartnerAvailability.handleSubmit}
                        className="w-full flex flex-col"
                      >
                    

                        <div className="flex flex-col gap-3 mt-3">
                          <div>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              type="submit"
                            >
                              Save
                            </Button>
                          </div>
                        </div>
                      </form> */}
                    </>
                  ) : activeStep === 1 ? (
                    <>
                      <div className=" sm:mt-8 mt-1 ">
                        <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                          <SearchIconWrapper>
                            <SearchIcon sx={{ color: "#616569" }} />
                          </SearchIconWrapper>
                          <StyledInputBase
                            value={searchTerm_providers}
                            onChange={handleSearchChange_Providers}
                            placeholder="Search Provider…"
                            inputProps={{ "aria-label": "search" }}
                          />
                        </Search>
                      </div>

                      <div>
                        <div className="flex flex-row items-center">
                          <Typography
                            sx={{
                              fontWeight: "bold",
                              fontSize: [14, 18],
                              margin: 1,
                            }}
                          >
                            Providers
                          </Typography>
                          <HiServer size={24} className="text-gray-400" />
                        </div>
                        <ul
                          className="m-2 rounded shadow-lg py-2"
                          style={{
                            overflowY: "scroll",
                            maxHeight: "40vh",
                          }}
                        >
                          {activeProviders &&
                            activeProviders.length > 0 &&
                            activeProviders.map((item, index) => (
                              <LightTooltip title="Select service provider">
                                <li
                                  key={index}
                                  // onClick={() => handleAddCustomer(item)}
                                  onClick={() =>
                                    handleConfirm_pick_provider(item)
                                  }
                                  className="py-3 border-b cursor-pointer border-gray-300 hover:bg-slate-300 duration-200 ease-linear"
                                >
                                  <div className="flex flex-row justify-between mx-2">
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.first_name}
                                    </Typography>
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.last_name}
                                    </Typography>
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.phone_number}
                                    </Typography>
                                    <Typography
                                      sx={{
                                        fontSize: [12, 13],
                                      }}
                                    >
                                      {item.email}
                                    </Typography>
                                  </div>
                                </li>
                              </LightTooltip>
                            ))}
                        </ul>
                      </div>

                      {/* <form
                        onSubmit={formikPartnerAvailability.handleSubmit}
                        className="w-full flex flex-col"
                      >
                    

                        <div className="flex flex-col gap-3 mt-3">
                          <div>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              type="submit"
                            >
                              Save
                            </Button>
                          </div>
                        </div>
                      </form> */}
                    </>
                  ) : activeStep === 2 ? (
                    <>
                      <div className="flex flex-row gap-[556px]">
                        <Typography
                          sx={{
                            fontWeight: "bold",
                            fontSize: [14, 18],
                          }}
                        >
                          Select Services
                        </Typography>

                        {/* <Typography
                          sx={{
                            fontWeight: "bold",
                            fontSize: [14, 18],
                          }}
                        >
                          Cart
                        </Typography> */}
                        <IconButton
                          size="large"
                          aria-label="show 17 new notifications"
                          color="inherit"
                          onClick={handleOPen_cartmodal}
                        >
                          <Badge badgeContent={cartNumber} color="error">
                            <ShoppingCartIcon color="#000" size={24} />
                          </Badge>
                        </IconButton>
                      </div>

                      <div className="mt-10 flex flex-col gap-4 px-2 justify-center  mb-4 w-full">
                        <div className="flex flex-row justify-start gap-4 items-center">
                          {/* <div className="  ">
                            <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                              <SearchIconWrapper>
                                <SearchIcon sx={{ color: "#616569" }} />
                              </SearchIconWrapper>
                              <StyledInputBase
                                value={searchTerm}
                                onChange={handleSearchChange}
                                placeholder="Search…"
                                inputProps={{ "aria-label": "search" }}
                              />
                            </Search>
                          </div> */}
                        </div>
                        {sortedPartnerServices &&
                          Object.entries(sortedPartnerServices).map(
                            ([category, services], index) => (
                              <Accordion
                                key={index}
                                expanded={expanded === `panel${index}`}
                                onChange={handleExpansion(`panel${index}`)}
                                sx={{
                                  "& .MuiAccordion-region": {
                                    height:
                                      expanded === `panel${index}` ? "auto" : 0,
                                  },
                                  "& .MuiAccordionDetails-root": {
                                    display:
                                      expanded === `panel${index}`
                                        ? "block"
                                        : "none",
                                  },
                                }}
                                className="w-[300px] sm:w-[300px] md:w-[500px] lg:w-[800px]"
                              >
                                <AccordionSummary
                                  expandIcon={<ExpandMoreIcon />}
                                  aria-controls={`panel${index}-content`}
                                  id={`panel${index}-header`}
                                >
                                  <div className="flex flex-row justify-center items-center">
                                    <img src={hsp} alt="" className="w-12" />
                                    <Typography
                                      sx={{
                                        fontWeight: "bold",
                                        fontSize: [14],
                                      }}
                                    >
                                      {category}
                                    </Typography>
                                  </div>
                                </AccordionSummary>
                                <AccordionDetails>
                                  <div className="flex flex-col gap-2">
                                    <YourComponent
                                      services={services}
                                      selectedCustomerProp={selectedCustomer}
                                      itemIdProp={itemID}
                                      cartNumber={cartNumber}
                                      setCartNumber={setCartNumber}
                                      //cartItems={cartItems}
                                      /// setCartItems={setCartItems}
                                    />
                                  </div>
                                </AccordionDetails>
                              </Accordion>
                            )
                          )}
                      </div>
                    </>
                  ) : activeStep === 3 ? (
                    <>
                      <form
                      // onSubmit={formikPartnerBookingPreference.handleSubmit}
                      >
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DemoContainer
                            components={["DatePicker", "DateTimePicker"]}
                          >
                            <DemoItem label="DateTimePicker">
                              <DateTimePicker
                                defaultValue={today}
                                // shouldDisableMonth={isInCurrentMonth}
                                onChange={handleDateChange}
                                views={[
                                  "year",
                                  "month",
                                  "day",
                                  // "hours",
                                  // "minutes",
                                ]}
                              />
                            </DemoItem>
                          </DemoContainer>
                        </LocalizationProvider>

                        <p>Time slots for {timeslotsDay}</p>

                        {timeslots && (
                          <div className="grid grid-cols-8 gap-4 py-4">
                            {timeslots.map((timeSlot, index) => (
                              <div key={index}>
                                <Button
                                  variant="outlined"
                                  onClick={() => handleClickTimeslot(timeSlot)}
                                  className="hover:bg-slate-600 duration-200 ease-in bg-white p-2 px-2 border rounded-md border-gray-600 hover:text-black"
                                >
                                  {timeSlot}
                                </Button>
                              </div>
                            ))}
                          </div>
                        )}

                        <div className="flex flex-col">
                          <p>
                            picked date:{" "}
                            {/* {selectedBookingDate && selectedBookingDate} */}
                            {selectedBookingDate
                              ? selectedBookingDate
                              : new Date().toISOString().split("T")[0]}
                          </p>
                          <p>
                            picked time: {selectedTimeSlot && selectedTimeSlot}
                          </p>
                        </div>

                        <div className="flex flex-col gap-3 mt-3">
                          <div>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              onClick={submitBookingDateTime}
                            >
                              Finish Time Selection
                            </Button>
                          </div>
                        </div>
                      </form>
                    </>
                  ) : activeStep === 4 ? (
                    <>
                      <form
                      // onSubmit={formikPartnerBookingPreference.handleSubmit}
                      >
                        {/* <p className="text-sm text-gray-400 py-4 select-none">
                          Lorem ipsum dolor, sit amet consectetur adipisicing
                          elit. Natus, sunt!
                        </p> */}
                        <div className="flex flex-row items-center space-x-20">
                          <Typography
                            sx={{
                              fontWeight: "Light",
                              fontSize: [17, 28],
                              margin: 1,
                            }}
                          >
                            Checkout
                          </Typography>
                          <div className="flex">
                            <div className=" border-r-2  border-gray-700">
                              <Typography
                                sx={{
                                  fontSize: 25,
                                  marginRight: 1,
                                  fontWeight: "bold",
                                }}
                              >
                                {/* 11:45 */}
                                {selectedTimeSlot && selectedTimeSlot}
                              </Typography>
                            </div>
                            <div className=" ">
                              <Typography
                                sx={{
                                  fontSize: 20,
                                  marginLeft: 1,
                                  fontWeight: "bold",
                                }}
                              >
                                {selectedBookingDate && selectedBookingDate}
                              </Typography>
                              <Typography sx={{ fontSize: 13, marginLeft: 1 }}>
                                Service Duration: {totalServiceDuration}
                              </Typography>

                              {/* <div>{totalServiceDuration} hrs</div> */}
                            </div>
                          </div>
                        </div>
                        <div className="flex flex-row justify-between">
                          <div>
                            <h1 className="text-sm text-gray-400 font-light">
                              1. Customer information
                            </h1>
                            <div className="m-6 flex flex-col gap-6 ">
                              <div className="flex flex-row gap-12">
                                <TextField
                                  id="input-with-icon-textfield"
                                  label="First Name"
                                  defaultValue={
                                    selectedCustomer &&
                                    selectedCustomer.first_name
                                  }
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <AccountCircleOutlinedIcon />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />

                                <TextField
                                  id="input-with-icon-textfield"
                                  label="Last Name"
                                  defaultValue={
                                    selectedCustomer &&
                                    selectedCustomer.last_name
                                  }
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <AccountCircleOutlinedIcon color="primary" />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />
                              </div>
                              <div className="flex flex-row gap-12">
                                <TextField
                                  id="input-with-icon-textfield"
                                  label="Phone Number"
                                  defaultValue={selectedCustomer.phone_number}
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <PhoneAndroidOutlinedIcon />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />
                                <TextField
                                  id="input-with-icon-textfield"
                                  label="Email"
                                  defaultValue={selectedCustomer.email}
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <EmailOutlinedIcon />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />
                              </div>
                            </div>
                            <h1 className="text-sm text-gray-400 font-light">
                              1. Provider information
                            </h1>
                            <div className="m-6 flex flex-col gap-6 ">
                              <div className="flex flex-row gap-12">
                                <TextField
                                  id="input-with-icon-textfield"
                                  label="First Name"
                                  defaultValue={selectedProvider.first_name}
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <AccountCircleOutlinedIcon />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />

                                <TextField
                                  id="input-with-icon-textfield"
                                  label="Last Name"
                                  defaultValue={selectedProvider.last_name}
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <AccountCircleOutlinedIcon color="primary" />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />
                              </div>
                              <div className="flex flex-row gap-12">
                                <TextField
                                  id="input-with-icon-textfield"
                                  label="Phone Number"
                                  defaultValue={selectedProvider.phone_number}
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <PhoneAndroidOutlinedIcon />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />
                                <TextField
                                  id="input-with-icon-textfield"
                                  label="Email"
                                  defaultValue={selectedProvider.email}
                                  InputProps={{
                                    startAdornment: (
                                      <InputAdornment position="start">
                                        <EmailOutlinedIcon />
                                      </InputAdornment>
                                    ),
                                  }}
                                  variant="standard"
                                />
                              </div>
                            </div>
                          </div>

                          {/* right size containing the services*/}

                          <div className=" ml-8 shadow-xl shadow-gray-400 w-[400px] mt-[-70px] max-h-[400px] border">
                            <div className="ml-4 m-2 py-4 flex justify-center items-center">
                              <h1 className="text-xl font-bold text-gray-600">
                                My Booking
                              </h1>
                            </div>

                            <div>
                              {cartItems &&
                                cartItems.map((object, index) => (
                                  <div
                                    key={index}
                                    className="flex flex-row gap-8"
                                  >
                                    <p className="text-black">{index + 1}:</p>
                                    {/* <img
                                      src={location}
                                      alt="pic"
                                      className="h-12 w-12 rounded-full"
                                    /> */}

                                    <p className="text-black"> {object.type}</p>

                                    <p className="text-black">
                                      {" "}
                                      {object.service_duration}min
                                    </p>

                                    <p className="text-black">
                                      {" "}
                                      KES: {object.price}
                                    </p>

                                    {/* <Button
                                      onClick={() =>
                                        remove_ServiceDetailsfromCart(object)
                                      }
                                    >
                                      remove
                                    </Button> */}
                                  </div>
                                ))}
                            </div>

                            <div className="mt-[200px]">
                              {" "}
                              Booking Page
                              <p className="px-2 font-bold">
                                Total Price:{" "}
                                <span className="ml-40">
                                  {totalServicePrice} /=
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className="flex flex-row gap-20 mt-3">
                          <div>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              onClick={submitFinalBooking}
                            >
                              Finish Booking
                            </Button>
                          </div>

                          <div>Service Type: {partner_serviceType}</div>
                          <div>Service Location: {partner_Location}</div>
                        </div>
                      </form>
                    </>
                  ) : null}
                </Typography>
                <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                  <Button
                    color="inherit"
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    sx={{ mr: 1 }}
                  >
                    Back
                  </Button>
                  <Box sx={{ flex: "1 1 auto" }} />
                  {/* <Button onClick={handleNext} sx={{ mr: 1 }}>
                    Next
                  </Button> */}
                  {/* {activeStep !== steps.length &&
                    (completed[activeStep] ? (
                      <Typography
                        variant="caption"
                        sx={{ display: "inline-block" }}
                      >
                        Step {activeStep + 1} already completed
                      </Typography>
                    ) : (
                      <Button onClick={handleComplete}>
                        {completedSteps() === totalSteps() - 1
                          ? "Finish"
                          : "Complete Step"}
                      </Button>
                    ))} */}
                </Box>
              </React.Fragment>
            )}
          </div>
        </Box>
      </Modal>
      {loading && <LinearProgress />}
      <AdminAddedSuccess
        open={openAddAdminSuccess}
        autoHideDuration={6000}
        name={"Provider"}
        mssg={"Added Succefully"}
        onClose={() => setOpenAddAdminSuccess(false)}
      />
      <AdminAddedSuccess
        open={openImageSuccess}
        autoHideDuration={6000}
        name={"Image"}
        mssg={"Added Succefully"}
        onClose={() => setOpenImageSuccess(false)}
      />
      <AdminAddedSuccess
        open={openProviderUpdateSuccess}
        autoHideDuration={6000}
        name={"Provider"}
        mssg={"Updated Succefully"}
        onClose={() => setOpenProviderUpdateSuccess(false)}
      />

      <AdminAddfailed
        open={openAddAdminFailed}
        autoHideDuration={6000}
        mssg={"Failed To Add"}
        name={"Provider"}
      />
      <AdminAddfailed
        open={openProviderUpdateFailed}
        autoHideDuration={6000}
        mssg={"Failed To Update"}
        name={"Provider"}
      />

      <Navbar />
      <div className="relative sm:ml-[300px] bg-white h-screen select-none">
        <TopNav />
        <div className="bg-white relative h-s  flex flex-col select-none">
          <div className="absolute top-4 left-4 justify-between sm:hidden md:hiddden   z-[1000]    lg:hidden">
            <UserNameTag />
          </div>
          <div className="border-b border-gray-400 sm:m-6 m-3 py-2 flex sm:flex-row flex-col justify-between">
            <div className="flex sm:flex-row flex-col justify-start ">
              <div className="flex flex-col justify-start text-start mt-8 ">
                <h1 className="text-lg sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
                  Booking Page
                </h1>

                {/* <img src={pp}/> */}
              </div>
              <div className=" sm:mt-8 mt-1 ">
                <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                  <SearchIconWrapper>
                    <SearchIcon sx={{ color: "#616569" }} />
                  </SearchIconWrapper>
                  <StyledInputBase
                    value={searchTerm}
                    onChange={handleSearchChange}
                    placeholder="Search…"
                    inputProps={{ "aria-label": "search" }}
                  />
                </Search>
              </div>

              <div className="mt-6">
                <FormControl sx={{ m: 1, minWidth: 186 }} size="small">
                  <InputLabel id="demo-select-small-label">
                    Select subcategory
                  </InputLabel>
                  <Select
                    labelId="demo-select-small-label"
                    id="subcategory"
                    // value={subcategory}
                    value={bookingStatus}
                    label="subcategory"
                    onChange={handleChangeType}
                  >
                    {/* Map over the array of menu items to generate MenuItem components */}
                    {menuItems.map((item, index) => (
                      <MenuItem key={index} value={item.value}>
                        {item.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
            </div>

            <div className="mt-8 flex">
              <Button variant="contained" onClick={handleOpen_EditProvider}>
                New Booking
              </Button>
            </div>
          </div>
          <div
            style={{
              // height: 400,
              // width: "100%",
              backgroundColor: "#fff",
              margin: "1rem",
            }}
          >
            <Paper sx={{ width: "100%", overflow: "auto" }}>
              <TableContainer sx={{ maxHeight: 800, minWidth: 1000 }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow className="select-none">
                      <TableCell
                        sx={{
                          fontWeight: 800,
                          fontSize: ["14px", "14px"],
                          width: "200px",
                        }}
                      >
                        Booking Code
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Customer Name
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Service Date
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Company Name
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Service Location
                      </TableCell>

                      {/* <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Manager Services
                      </TableCell> */}

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Status
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Price
                      </TableCell>

                      <TableCell
                        align="center"
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Action
                      </TableCell>

                      {items}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows &&
                      rows
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map((row, index) => (
                          <TableRow
                            key={index}
                            sx={{
                              "&:last-child td, &:last-child th": { border: 0 },
                            }}
                            className="hover:bg-gray-400 select-none"
                            //onDoubleClick={handleRowDoubleClick}
                            //onDoubleClick={() => handleRowDoubleClick(row)}
                            onMouseEnter={() => {
                              setSelectedRowData(row);
                              //setIndex(index);
                            }}
                          >
                            {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                            <TableCell
                              className="flex flex-row w-10 hover:underline cursor-pointer "
                              sx={{
                                // fontWeight: 800,
                                fontSize: ["12px", "15px"],
                                width: "200px",
                              }}
                              onClick={handleOpen_EditProvider}
                            >
                              <p>{row.booking_id} &nbsp;</p>
                              {/* <div className="flex flex-row gap-2 items-center">
                              <p>
                                {row.customer_name} &nbsp;
                                {row.last_name}
                              </p>
                            </div> */}
                            </TableCell>

                            <TableCell>{row.customer_name}</TableCell>

                            <TableCell>
                              {" "}
                              {row.booking_date}, {row.booking_time}
                            </TableCell>

                            <TableCell>{row.provider_businessname}</TableCell>

                            <TableCell>
                              {row.provider_location.address}
                            </TableCell>

                            <TableCell>{row.status}</TableCell>

                            <TableCell align="right">
                              <Typography>
                                {row &&
                                  row.cart &&
                                  row.cart.reduce((total, item) => {
                                    return (
                                      total +
                                      parseInt(item.price) *
                                        parseInt(item.number)
                                    );
                                  }, 0)}
                              </Typography>
                            </TableCell>

                            <TableCell align="right">
                              <IconButton aria-controls={actionId}>
                                <CiSettings
                                  color="#111111"
                                  size={23}
                                  onClick={handleActionMenuOpen}
                                />
                              </IconButton>
                            </TableCell>
                            {/* Status column */}

                            {/* Amount Paid Column */}
                            {/* Balance Column */}
                          </TableRow>
                        ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                className="select-none"
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
      </div>
    </>
  );
}
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "scroll",
  maxHeight: "80vh",
  width: ["90%", "68%"],
};
const style_box2 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "90%"],
};
const style_box3 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "35%"],
};
