import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import endpoints from "../endpoints";
import { CiSettings } from "react-icons/ci";
import hsp from "../assets/avatar5.jpg";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  IconButton,
  InputAdornment,
  InputLabel,
  LinearProgress,
  Menu,
  MenuItem,
  Modal,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
import { styled, alpha } from "@mui/material/styles";
import { BsGearFill } from "react-icons/bs";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import FilterListIcon from "@mui/icons-material/FilterList";
import {
  MdAirplanemodeActive,
  MdAirplanemodeInactive,
  MdEdit,
  MdOutlineAirplanemodeInactive,
} from "react-icons/md";
import { RiDeleteBinFill } from "react-icons/ri";
import {
  IoIosCloseCircle,
  IoIosCloseCircleOutline,
  IoMdCloseCircle,
} from "react-icons/io";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { TbPasswordFingerprint } from "react-icons/tb";
import UserNameTag from "../components/UserNameTag";
import { useFormik } from "formik";
import { Textarea } from "../styles";
import axios from "axios";
import { FaCamera } from "react-icons/fa";
import hsp1 from "../assets/cover-photo-avatar.png";
import { useState } from "react";
import AdminAddedSuccess from "../components/Alerts/AdminAddedSuccess";
import LocationSearchInput from "../components/GoogleLocation";
import Success from "../components/Alerts-2/GeneralSuccess";
import ErrorEmailTaken from "../components/Alerts-2/ErrorEmailAlreadyTaken";
import MissingFieldsError from "../components/Alerts-2/MissingFields";
import GeneralError from "../components/Alerts-2/GeneralError";
import GeneralSuccess from "../components/Alerts-2/GeneralSuccess";
import * as Yup from "yup";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",

    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function UsersPage() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openAction, setOpenAction] = React.useState(false);
  const [isOPen, setIsOpen] = React.useState(false);
  const [openAddCompany, setOpenAddCompany] = React.useState(false);
  const [searchTerm, setSearchTerm] = React.useState();
  const [showPassword, setShowPassword] = React.useState(false);
  const [openResetPassword, setOpenResetPassword] = React.useState(false);
  const [openEdit, setEdit] = React.useState(false);
  const [openAddImage, setOpenAddImage] = useState(false);
  const [openAddAdminSuccess, setOpenAddAdminSuccess] = React.useState(false);

  const [success, setOpenSuccess] = React.useState(false);

  const [emailAlreadyTakenError, setEmailAlreadyTaken] = React.useState(false);

  const [missingFieldsError, setMissingFieldsError] = useState(false);

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  const [generalError, setGeneralError] = useState();
  const [generalSuccess, setGeneralSuccess] = useState();

  const [deactivateCustomer, setOpenDeactivateCustomerRecordModal] =
    useState(false);

  const [activateCustomer, setOpenActivateCustomerRecordModal] =
    useState(false);

  ///emailAlreadyTaken

  const [file, setFile] = useState();

  const [pp, setPP] = React.useState("");
  const [bp, setBP] = React.useState();
  const [bgfile, setBgFile] = React.useState();
  const [selectedRowData, setSelectedRowData] = useState();
  const [stateToken, setToken] = useState();

  const [subcategory, setSubcategory] = React.useState("ALL");

  const menuItems = [
    { value: "ALL", label: "All" },
    { value: "ACTIVE", label: "ACTIVE" },
    { value: "INACTIVE", label: "INACTIVE" },
    { value: "FEATURED", label: "FEATURED" },
  ];

  const validationSchema_resetPassword = Yup.object({
    // first_name: Yup.string().required("first name is required"),
    // last_name: Yup.string().required("last name is required"),
    // email: Yup.string().required("email is required"),
    // admin_type: Yup.string().required("admin type is required"),
    password: Yup.string().required("password is required"),
  });

  const actionId = "primary-action-menu";
  const handleOpen_file = () => setOpenfile(true);
  const handleClose_file = () => setOpenfile(false);
  const handleOpenAddImage = () => {
    setOpenAddImage(true);
  };
  const handleCloseAddImage = () => {
    setOpenAddImage(false);
  };

  function reloadPage() {
    window.location.reload();
  }

  const handleChangeType = (event) => {
    console.log("event selected:->", event.target.value);
    setSubcategory(event.target.value);

    if (event.target.value) {
      filterCustomers(event.target.value);
    }
  };

  function filterCustomers(filter) {
    setLoading(true);
    console.log("sort services functions called", filter);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("json_token", json_token.token);

    axios
      //.get(endpoints.getBeautyServices, {
      .get(`${endpoints.filterCustomers}?status=${filter}`, {
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        if (response.data) {
          const json = response.data;
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log(
            "Response filtered data: after sorting:--> ",
            response.data
          );
          setRows(json);
        } else {
          // setRows([]);
        }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoading(false);
      });
    /// } else {
    //console.log("Subcategiry empty", subcategory);
    // }
  }

  const handleUpdateCustomerProfilePicURL = async (generatedURL) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("generatedURL", generatedURL);

    const profileURL = {
      profile_pic: generatedURL,
    };

    console.log("profileURLOBject", profileURL);

    try {
      setLoading(true); // Set loading to true when making the request

      const response = await fetch(
        `${endpoints.updateCustomerProfilePicURL}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(profileURL),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        // setOpenImageSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  const handleUploadProfile = async (file) => {
    try {
      setLoading(true);
      const formData = new FormData();
      formData.append("profile", file);

      // Replace 'your-upload-endpoint' with your actual upload endpoint
      const response = await axios.post(
        endpoints.uploadCustomerProfilePic,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${stateToken}`,
          },
        }
      );

      console.log("Upload Customer Image successful:", response.data);
      setPP(response.data.image_url);
      console.log("ID", selectedRowData._id);
      handleUpdateCustomerProfilePicURL(response.data.image_url);
    } catch (error) {
      console.error("Error uploading file:", error);
    } finally {
      console.log("Request completed."); // This will be executed regardless of success or failure
      setLoading(false);
    }
  };

  function handleChange(e) {
    // Access the file object
    const file = e.target.files[0];
    console.log("upload profile picture");

    handleUploadProfile(file);

    // Create a new FileReader
    const reader = new FileReader();

    // Set up the onload event to handle the file read operation
    reader.onload = () => {
      // Get the Base64 encoded data
      const base64Data = reader.result;

      // Assign the Base64 data to the src attribute of the image element
      // if (profilePicRef.current) {
      //   profilePicRef.current.src = base64Data;
      // }

      // setPP(base64Data);

      // Optionally, you can handle the Base64 data (e.g., store it in state)
      // console.log("Base64 encoded image:", base64Data);
    };

    // Read the contents of the file as a Data URL
    reader.readAsDataURL(file);
    setFile(URL.createObjectURL(e.target.files[0]));
  }
  function handleChange2(e) {
    // Access the file object
    const bgfile = e.target.files[0];

    handleUploadLogo(bgfile);

    // Create a new FileReader
    const reader = new FileReader();

    // Set up the onload event to handle the file read operation
    reader.onload = () => {
      // Get the Base64 encoded data
      const base64Data = reader.result;

      // Assign the Base64 data to the src attribute of the image element
      if (profilePicRef.current) {
        profilePicRef.current.src = base64Data;
      }

      // setPP(base64Data);
      // setBP(base64Data);

      // Optionally, you can handle the Base64 data (e.g., store it in state)
      // console.log("Base64 encoded image:", base64Data);
    };

    // Read the contents of the file as a Data URL
    reader.readAsDataURL(bgfile);
    //setBgFile(URL.createObjectURL(e.target.files[0]));
  }

  const formikResetPassword = useFormik({
    initialValues: {
      password: "",
    },
    validationSchema: validationSchema_resetPassword,
    onSubmit: (values) => {
      console.log("form data:-", values);

      //console.log("first name:->", adminDetails.first_name);
      resetPassword(values.password.trim());

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  async function resetPassword(newPassword) {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
    console.log("json_token", stored_json_data);
    // console.log(
    //   "full url",
    //   `${endpoints.getAdminById}/${stored_json_data.admin_id}`
    // );
    const ongoing_json = {
      // first_name: selectedRowData.first_name,
      // last_name: selectedRowData.last_name,
      // email: selectedRowData.email,
      password: newPassword,
      // status: selectedRowData.status,
    };

    console.log("ongoing json for reset:->", ongoing_json);

    try {
      // setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${stored_json_data.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };

      const response = await fetch(
        `${endpoints.resetCustomerPassword}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${stored_json_data.token}`,
          },
          body: JSON.stringify(ongoing_json),
        }
      );
      console.log("response after edit:->>", response);

      response.json().then((data) => {
        console.log("json", data);

        if (data.status == "0") {
          console.log("Error detected");
          setErrorMsg("Something went wrong");
          setGeneralError(true);
          return;
        } else if (data.status == "1") {
          setOpenSuccess(true);
          setSuccessMsg("Password Reset Successful!");
          setGeneralSuccess(true);
          setTimeout(() => {
            setOpenAddCompany(false);
            reloadPage();
          }, 2000);
        }
      });
    } catch (error) {
      console.error("Error sending data to the backend:", error);

      if (error.message == "Network Error") {
        console.log("Network Error");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      } else {
        console.log("Error sending data to the backend:");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      }
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  }

  function handleOpenDeactivateModal() {
    //setOpenDeleteRecordModal(true);
    console.log("deactivate customer");
    setOpenDeactivateCustomerRecordModal(true);
  }

  function handleClose_popDeactivateModal() {
    //setOpenDeleteRecordModal(true);
    setOpenDeactivateCustomerRecordModal(false);
  }

  function handleOpenActivateModal() {
    //setOpenDeleteRecordModal(true);
    console.log("activate customer");
    setOpenActivateCustomerRecordModal(true);
  }

  function handleClose_popActivateModal() {
    //setOpenDeleteRecordModal(true);
    setOpenActivateCustomerRecordModal(false);
  }

  //handleClose_popDeactivateModal
  const handleOpenEdit = () => {
    console.log("formikEdit customer", selectedRowData);
    formikEditCustomerRegistration.setValues({
      first_name: selectedRowData.first_name,
      last_name: selectedRowData.last_name,
      email: selectedRowData.email,
      phone_number: selectedRowData.phone_number,
      location: selectedRowData.location.address,
    });

    console.log("selected Row data", selectedRowData);

    setPP(selectedRowData.profile_pic);

    setEdit(true);
  };
  const handleCloseEdit = () => {
    setEdit(false);
  };
  const handleOpen_AddCompany = () => {
    setOpenAddCompany(true);
  };
  const handleClose_AddCompany = () => {
    setOpenAddCompany(false);
  };

  const handleOpenResetPassWord = () => {
    setOpenResetPassword(true);
  };
  const handleCloseResetPassWord = () => {
    setOpenResetPassword(false);
  };
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const items = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={actionId}
      keepMounted
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      open={isOPen}
      onClose={handleActionMenuClose}
    >
      <MenuItem className="flex gap-2" onClick={handleOpenEdit}>
        <MdEdit /> Edit
      </MenuItem>
      {/* <MenuItem className="flex gap-2" onClick={handleOpenActivateModal}>
        <MdAirplanemodeActive /> Activate
      </MenuItem> */}
      {/* <MenuItem className="flex gap-2" onClick={handleOpenDeactivateModal}>
        <MdOutlineAirplanemodeInactive /> Deactivate
      </MenuItem> */}

      {selectedRowData && selectedRowData.status != "ACTIVE" && (
        <MenuItem className="flex gap-2" onClick={handleOpenActivateModal}>
          <MdAirplanemodeActive /> Activate
        </MenuItem>
      )}

      {selectedRowData && selectedRowData.status != "INACTIVE" && (
        <MenuItem className="flex gap-2" onClick={handleOpenDeactivateModal}>
          <MdOutlineAirplanemodeInactive /> Deactivate
        </MenuItem>
      )}

      <MenuItem className="flex gap-2" onClick={handleOpenResetPassWord}>
        <TbPasswordFingerprint /> Reset Password
      </MenuItem>
      <MenuItem className="flex gap-2">
        <RiDeleteBinFill /> Delete
      </MenuItem>
    </Menu>
  );
  function handleActionMenuClose() {
    setIsOpen(false);
  }
  function handleActionMenuOpen() {
    setIsOpen(true);
  }

  function filterUsers(searchVal) {
    const data = JSON.parse(localStorage.getItem("userData")) || [];

    console.log("To be filtered users:->", data);

    if (searchVal === undefined || searchVal.trim().length === 0) {
      setRows(data);
    } else {
      const searchTerm = searchVal.toLowerCase().split(" ");

      const results = data.filter((user) => {
        const firstName = user.first_name ? user.first_name.toLowerCase() : "";
        const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        const email = user.email ? user.email.toLowerCase() : "";
        const phoneNumber = user.phone_number ? user.phone_number : "";
        const companyName = user.main_businessName
          ? user.main_businessName.toLowerCase()
          : "";

        return searchTerm.every(
          (term) =>
            firstName.includes(term) ||
            lastName.includes(term) ||
            email.includes(term) ||
            phoneNumber.includes(term) ||
            companyName.includes(term)
        );
      });

      setRows(results);
    }
  }

  function handleGetAllRecords(searchVal) {
    setLoading(true);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("json_token", json_token.token);

    setToken(json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);

    fetch(endpoints.getCustomers, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log("JSON:->", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        localStorage.setItem("userData", JSON.stringify(json));
        console.log("customer data set globally...");
        setRows(json);

        // // Your further processing logic
        // if (searchVal === undefined || searchVal.trim().length === 0) {
        //   setRows(json);
        // } else {
        //   const results = json.filter((user) => {
        //     const firstName = user.first_name
        //       ? user.first_name.toLowerCase()
        //       : "";
        //     const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        //     const BusinessName = user.main_businessName
        //       ? user.main_businessName.toLowerCase()
        //       : "";
        //     const email = user.email ? user.email.toLowerCase() : "";

        //     const searchTerm = searchVal.toLowerCase();

        //     return (
        //       firstName.includes(searchTerm) ||
        //       lastName.includes(searchTerm) ||
        //       BusinessName.includes(searchTerm) ||
        //       email.includes(searchTerm)
        //     );
        //   });
        //   setRows(results);
        // }
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }
  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    filterUsers(event.target.value);
    // handleGetAllRecords(event.target.value);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const postCustomerRegistrationData = async (data) => {
    const customerRegistrationObject = {
      email: data.email.trim(),
      password: data.password.trim(),
      first_name: data.first_name,
      last_name: data.last_name,
      phone_number: data.phone_number,

      status: "active",
    };

    console.log("main object", customerRegistrationObject);

    try {
      setLoading(true);
      const response = await axios.post(
        endpoints.registerCustomer,
        customerRegistrationObject
      );

      console.log("Response from the backend:", response.data);

      if (response.data.status == "0") {
        if (
          response.data.message ===
          "Missing required fields: email, password, first_name, or last_name"
        ) {
          console.error("Missing required fields");
          setErrorMsg("Missing required fields");
          //setMissingFieldsError(true);
          setGeneralError(true);
          return;
        } else if (
          response.data.message ===
          "Missing required fields: email, password, first_name, or last_name"
        ) {
          console.error("Missing fields");
          return;
        } else if ("Email-already-taken") {
          console.error("Email Already Taken");
          setErrorMsg("Email Already Taken");
          //setMissingFieldsError(true);
          setGeneralError(true);
          //setEmailAlreadyTaken(true);
          return;
        }
        return;
      } else if (response.data.status == "1") {
        setOpenSuccess(true);
        setSuccessMsg("Customer Added Successfully!");
        setGeneralSuccess(true);
        setTimeout(() => {
          setOpenAddCompany(false);
          reloadPage();
        }, 2000);
      }

      // Reload the page only if there's no error
    } catch (error) {
      console.error("Error sending data to the backend:", error);

      if (error.message == "Network Error") {
        console.log("Network Error");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      }
      //setOpenAddAdminFailed(true);
      // setOpenAddCompany(true); // Open the pop-up modal on error
    } finally {
      setLoading(false);
    }
    // setTimeout(() => {
    //   setOpenAddCompany(false);
    //   reloadPage();
    // }, 2000);
  };

  const updateCustomerRegistrationData = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    console.log("inbound customer data", data);
    console.log("updated location", selectedRowData.location);

    const customerRegistrationObject = {
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email.trim(),
      phone_number: data.phone_number,
      //location: data.location,
    };

    console.log("main object", customerRegistrationObject);

    try {
      setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.editCustomerRegistrationInfo}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(customerRegistrationObject),
        }
      );
      if (response.ok) {
        //console.log("Records updated successfully");
        // setOpenProviderUpdateSuccess(true);
        response.json().then((data) => {
          console.log("json", data);

          if (data.status == "Error") {
            console.log("Error detected");
            if (
              data.message ===
              "Missing required fields: email, password, first_name, or last_name"
            ) {
              console.error("Missing required fields");
              setErrorMsg("Missing required fields");
              //setMissingFieldsError(true);
              setGeneralError(true);
              return;
            } else if (
              data.message ===
              "Missing required fields: email, password, first_name, or last_name"
            ) {
              console.error("Missing fields");
              return;
            } else if (data.message === "Nothing to be updated") {
              //console.error("Email Already Taken");
              setErrorMsg("Nothing to be updated!");
              //setMissingFieldsError(true);
              setGeneralError(true);
              //setEmailAlreadyTaken(true);
              return;
            }
            return;
          } else if (data.status == "Success") {
            console.log("successfully updated!!!");
            setOpenSuccess(true);
            setSuccessMsg("Customer updated Successfully!");
            setGeneralSuccess(true);
            setTimeout(() => {
              setOpenAddCompany(false);
              reloadPage();
            }, 2000);
          }
        });
      } else {
        console.error("Failed to update record information");
        //console.error("Email Already Taken");
        setErrorMsg("Failed to update record information-Select Location");
        //setMissingFieldsError(true);
        setGeneralError(true);
        //setOpenProviderUpdateFailed(true);
      }
    } catch (error) {
      //setOpenProviderUpdateFailed(true);

      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false);

      // if (response.status === 200) {
      //   console.log("Records updated successfully");

      // } else {
      //   setOpenAddAdminFailed(true);
      //   setOpenAddCompany(true);
      //   console.error("Failed to update record information");
      // }

      // Set loading to false after the request is complete (success or error)
    }
    // setTimeout(() => {
    //   setOpenAddCompany(false);
    //   reloadPage();
    // }, 2000);
  };

  const deactivateCustomerFunction1 = async (updatedRecord) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("json_token", json_token.token);
    const delete_property = {
      status: "INACTIVE",
    };
    try {
      const response = await fetch(
        //`endpoints${globalID}/${selectedRowData._id}`,
        `${endpoints.deactivateCustomer}?id=${selectedRowData._id}`,

        //deactivateCustomer

        //`http://localhost:8080/cancel/${globalID}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${json_token.token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify(delete_property),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        //setOpenRecordChangeSuccessfully(true);
        //setRefresher(true);
        // handleNotificationMenuClose();
        //setOpenDeleteRecordModal(false);

        // setTimeout(() => {
        //   //setOpenInstallment(false);
        //   // setOpenMainEditForm(false);
        //   reloadPage();
        // }, 1000);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  async function deactivateCustomerFunction() {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
    console.log("json_token", stored_json_data);

    const ongoing_json = {
      // first_name: selectedRowData.first_name,

      password: "newPassword",
    };

    try {
      // setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${stored_json_data.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };

      const response = await fetch(
        `${endpoints.deactivateCustomer}?id=${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${stored_json_data.token}`,
          },
          body: JSON.stringify(ongoing_json),
        }
      );
      console.log("response after edit:->>", response);

      response.json().then((data) => {
        console.log("json", data);

        if (data.status == "0") {
          console.log("Error detected");
          setErrorMsg("Something went wrong");
          setGeneralError(true);
          return;
        } else if (data.status == "1") {
          setOpenSuccess(true);
          setSuccessMsg("Deactivation Success!");
          setGeneralSuccess(true);
          setTimeout(() => {
            setOpenAddCompany(false);
            reloadPage();
          }, 2000);
        }
      });
    } catch (error) {
      console.error("Error sending data to the backend:", error);

      if (error.message == "Network Error") {
        console.log("Network Error");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      } else {
        console.log("Error sending data to the backend:");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      }
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  }

  async function activateCustomerFunction() {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
    console.log("json_token", stored_json_data);

    const ongoing_json = {
      // first_name: selectedRowData.first_name,

      password: "newPassword",
    };

    try {
      // setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${stored_json_data.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };

      const response = await fetch(
        `${endpoints.activateCustomer}?id=${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${stored_json_data.token}`,
          },
          body: JSON.stringify(ongoing_json),
        }
      );
      console.log("response after edit:->>", response);

      response.json().then((data) => {
        console.log("json", data);

        if (data.status == "0") {
          console.log("Error detected");
          setErrorMsg("Something went wrong");
          setGeneralError(true);
          return;
        } else if (data.status == "1") {
          setOpenSuccess(true);
          setSuccessMsg("Activation Success!");
          setGeneralSuccess(true);
          setTimeout(() => {
            setOpenAddCompany(false);
            reloadPage();
          }, 2000);
        }
      });
    } catch (error) {
      console.error("Error sending data to the backend:", error);

      if (error.message == "Network Error") {
        console.log("Network Error");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      } else {
        console.log("Error sending data to the backend:");
        setErrorMsg("Network Error");
        setGeneralError(true);

        return;
      }
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  }

  const formikEditCustomerRegistration = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      email: "",
      phone_number: "",
      location: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);
      //console.log("profile image", profilePicRef.current);

      //postPartnerRegistrationData(values);
      updateCustomerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  const formikCustomerRegistration = useFormik({
    initialValues: {
      email: "",
      password: "",
      first_name: "",
      last_name: "",
      phone_number: "",
      //main_businessName: "",
      //  bio: "",
      // profilePicture: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      postCustomerRegistrationData(values);
      //console.log("profile image", profilePicRef.current);

      //  postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  React.useEffect(() => {
    handleGetAllRecords();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      {/* <AdminAddedSuccess
        open={openAddAdminSuccess}
        autoHideDuration={6000}
        name={"Service"}
        mssg={"Added Succefully"}
        onClose={() => setOpenAddAdminSuccess(false)}
      /> */}
      <Success
        open={success}
        autoHideDuration={6000}
        //name={"Success"}
        msg={"Success!"}
        onClose={() => setOpenSuccess(false)}
      />
      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />
      <ErrorEmailTaken
        open={emailAlreadyTakenError}
        autoHideDuration={6000}
        //name={"Success"}
        //msg={"Success!"}
        onClose={() => setEmailAlreadyTaken(false)}
      />
      <MissingFieldsError
        open={missingFieldsError}
        autoHideDuration={6000}
        //name={"Success"}
        //msg={"Success!"}
        onClose={() => setMissingFieldsError(false)}
      />
      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />

      <Modal
        open={deactivateCustomer}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className=" m-4">
              <span className="font-bold m-2">Deactivation Confirmation</span>
              <br></br>{" "}
              <span>Are you sure you want to Deactivate this Customer?</span>
            </h2>

            {/* <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            /> */}
          </div>

          <form>
            <div className="flex flex-col gap-3 ">
              <div className="flex flex-row gap-3 mt-3"></div>
              <div className="flex flex-row gap-4">
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={deactivateCustomerFunction}
                >
                  yes
                </Button>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={() => setOpenDeactivateCustomerRecordModal(false)}
                >
                  No
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <Modal
        open={activateCustomer}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className=" m-4">
              <span className="font-bold m-2">Activation Confirmation</span>
              <br></br>{" "}
              <span>Are you sure you want to Activate this Customer?</span>
            </h2>

            {/* <CloseIcon
              fontSize="40"
              onClick={handlecloseAgentForm}
              sx={{ cursor: "pointer" }}
            /> */}
          </div>

          <form>
            <div className="flex flex-col gap-3 ">
              <div className="flex flex-row gap-3 mt-3"></div>
              <div className="flex flex-row gap-4">
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={activateCustomerFunction}
                >
                  yes
                </Button>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  onClick={handleClose_popActivateModal}
                >
                  No
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <Modal
        onClose={handleCloseResetPassWord}
        open={openResetPassword}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <Typography className="flex flex-row gap-2 text-black">
            Reset{" "}
            {/* <Typography sx={{ fontWeight: "bold" }}>
              {" "}
              {selectedRowData.username}&#39;s
            </Typography> */}
            Password?
          </Typography>

          {/* */}
          <form onSubmit={formikResetPassword.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <TextField
                type={showPassword ? "text" : "password"}
                id="password"
                name="password"
                label="New Password:"
                placeholder="Enter password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                maxRows={4}
                value={formikResetPassword.values.password}
                onChange={formikResetPassword.handleChange}
                onBlur={formikResetPassword.handleBlur}
                error={
                  formikResetPassword.touched.password &&
                  Boolean(formikResetPassword.errors.password)
                }
                helperText={
                  formikResetPassword.touched.password &&
                  formikResetPassword.errors.password
                }
              />
            </div>

            <div className="flex flex-row gap-4 m-4">
              <Button variant="contained" type="submit">
                Reset
              </Button>
              <Button
                variant="contained"
                color="error"
                onClick={handleCloseResetPassWord}
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openAddCompany}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <>
            <div className="flex flex-row justify-between m-2 py-2">
              <Typography sx={{ color: "#000", fontSize: [18] }}>
                Add Customer
              </Typography>
              <IoMdCloseCircle
                size={18}
                onClick={handleClose_AddCompany}
                cursor={"pointer"}
                color="#000"
              />
            </div>
            <form onSubmit={formikCustomerRegistration.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    variant="outlined"
                    type="text"
                    id="first_name"
                    name="first_name"
                    label="Fist Name"
                    placeholder="First Name"
                    value={formikCustomerRegistration.values.first_name}
                    onChange={formikCustomerRegistration.handleChange}
                    onBlur={formikCustomerRegistration.handleBlur}
                  />

                  <TextField
                    type="text"
                    id="last_name"
                    name="last_name"
                    label="Last Name"
                    placeholder="Last Name"
                    value={formikCustomerRegistration.values.last_name}
                    onChange={formikCustomerRegistration.handleChange}
                    onBlur={formikCustomerRegistration.handleBlur}
                  />
                </div>
              </div>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    type="text"
                    id="email"
                    name="email"
                    label="Email"
                    placeholder=" Email"
                    value={formikCustomerRegistration.values.email}
                    onChange={formikCustomerRegistration.handleChange}
                    onBlur={formikCustomerRegistration.handleBlur}
                  />
                  <TextField
                    id="phone_number"
                    name="phone_number"
                    label="Phone Number"
                    placeholder="Phone Number"
                    value={formikCustomerRegistration.values.phone_number}
                    onChange={formikCustomerRegistration.handleChange}
                    onBlur={formikCustomerRegistration.handleBlur}
                  />
                </div>
              </div>

              <div className="flex flex-row gap-3 ">
                {/* <TextField
                  type="text"
                  id="main_businessName"
                  name="main_businessName"
                  label="Business Name"
                  placeholder="Business Name"
                  value={formikCustomerRegistration.values.main_businessName}
                  onChange={formikCustomerRegistration.handleChange}
                  onBlur={formikCustomerRegistration.handleBlur}
                /> */}
                <TextField
                  id="password"
                  name="password"
                  label="Password"
                  placeholder="Password"
                  value={formikCustomerRegistration.values.password}
                  onChange={formikCustomerRegistration.handleChange}
                  onBlur={formikCustomerRegistration.handleBlur}
                />
              </div>
              {/* <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Bio description </FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="bio"
                  id="bio"
                  name="bio"
                  label="Bio"
                  placeholder="Bio"
                  value={formikCustomerRegistration.values.bio}
                  onChange={formikCustomerRegistration.handleChange}
                  onBlur={formikCustomerRegistration.handleBlur}
                />
              </div> */}
              <div className="flex flex-col gap-3 mt-3">
                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>
          </>
        </Box>
        {/* <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4 text-[#111111]">
              Add User Information
            </h2>
            <IoIosCloseCircle
              color="#000"
              size={23}
              cursor={"pointer"}
              onClick={handleClose_AddCompany}
            />
          </div>

          <form>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="company_name"
                  name="company_name"
                  label="Company Name"
                  placeholder="Company Name"
                />

                <TextField
                  type="email"
                  id="email"
                  name="email"
                  label="Email"
                  placeholder="Email"
                />
              </div>
            </div>
            <div className="flex flex-col gap-3 ">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="picture"
                  name="picture"
                  label="Profile picture"
                  placeholder=" Profile picture"
                />
                <TextField id="VAT" name="VAT" label="VAT" placeholder="VAT" />
              </div>
            </div>

            <div className="flex flex-row gap-3 ">
              <TextField
                type="text"
                id="country"
                name="country"
                label="Country"
                placeholder="Country"
              />
              <TextField
                type="phone"
                id="phone"
                name="phone"
                label="Phone"
                placeholder="Phone"
              />
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box> */}
      </Modal>
      <Modal open={openEdit}>
        <Box sx={style_box}>
          <div className="flex flex-row justify-between m-2 py-2">
            <Typography sx={{ color: "#000", fontSize: [18] }}>
              Edit Customer
            </Typography>
            <IoMdCloseCircle
              size={18}
              onClick={handleCloseEdit}
              cursor={"pointer"}
              color="#000"
            />
          </div>
          <form onSubmit={formikEditCustomerRegistration.handleSubmit}>
            <div>
              <Modal onClose={() => setOpenAddImage(false)} open={openAddImage}>
                <Box sx={style_box3} className="z-50">
                  <div className="flex flex-row justify-between m-2 ">
                    <Typography sx={{ color: "#000", fontSize: [18] }}>
                      Upload Image:
                    </Typography>
                    <IoMdCloseCircle
                      size={18}
                      onClick={handleCloseAddImage}
                      cursor={"pointer"}
                      color="#000"
                    />
                  </div>
                  <div className="flex flex-col gap-12">
                    <div className="flex flex-row gap-3 border-b bod border-gray-400 py-3">
                      <label className="text-black">Profile</label>
                      <input
                        type="file"
                        onChange={handleChange}
                        className="mt-[20%] cursor-pointer"
                      />
                      <img
                        src={!pp ? hsp : pp}
                        className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
                      />
                    </div>
                  </div>
                </Box>
              </Modal>

              <div className="h-44 relative mb-24">
                <div className="bg-cover bg-center rounded-lg relative h-44 mb-24 flex items-center justify-center">
                  <div className="absolute  w-[155px] h-[155px] bg-gray-600 rounded-full flex items-center justify-center top-[50%]">
                    <img
                      defaultValue={hsp}
                      src={!file ? (!pp ? hsp : pp) : file}
                      alt="pic"
                      className="rounded-full w-[140px] h-[140px]"
                    />
                  </div>

                  <div className="flex absolute bottom-[-20%] sm:left-44 left-48 text-black">
                    <Typography sx={{ fontSize: [12, 19] }}>
                      {formikEditCustomerRegistration.values.first_name}
                      &nbsp;
                      {formikEditCustomerRegistration.values.last_name}
                      &nbsp;
                      <span className="text-xs text-gray-400">
                        {formikEditCustomerRegistration.values.email}
                      </span>
                    </Typography>
                  </div>
                </div>
                <div className="absolute ml-[250px] mt-[-90px] bg-slate-400 rounded-full w-9 h-9 flex justify-center items-center">
                  <div className=" hover:scale-110 ease-linear duration-200 cursor-pointer bg-slate-200 rounded-full w-8 h-8 flex justify-center items-center">
                    <FaCamera
                      color="#111111"
                      size={20}
                      onClick={handleOpenAddImage}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="first_name"
                  name="first_name"
                  label="Fist Name"
                  placeholder="First Name"
                  value={formikEditCustomerRegistration.values.first_name}
                  onChange={formikEditCustomerRegistration.handleChange}
                  onBlur={formikEditCustomerRegistration.handleBlur}
                />

                <TextField
                  type="text"
                  id="last_name"
                  name="last_name"
                  label="Last Name"
                  placeholder="Last Name"
                  value={formikEditCustomerRegistration.values.last_name}
                  onChange={formikEditCustomerRegistration.handleChange}
                  onBlur={formikEditCustomerRegistration.handleBlur}
                />
              </div>
            </div>
            <div className="flex flex-col gap-3 ">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="email"
                  name="email"
                  label="Email"
                  placeholder=" Email"
                  value={formikEditCustomerRegistration.values.email}
                  onChange={formikEditCustomerRegistration.handleChange}
                  onBlur={formikEditCustomerRegistration.handleBlur}
                />
                <TextField
                  id="phone_number"
                  name="phone_number"
                  label="Phone Number"
                  placeholder="Phone Number"
                  value={formikEditCustomerRegistration.values.phone_number}
                  onChange={formikEditCustomerRegistration.handleChange}
                  onBlur={formikEditCustomerRegistration.handleBlur}
                />
              </div>
            </div>

            <div className="flex flex-row gap-3 ">
              <TextField
                type="text"
                id="main_businessName"
                name="main_businessName"
                label="Location"
                placeholder="Location"
                value={formikEditCustomerRegistration.values.location}
                onChange={formikEditCustomerRegistration.handleChange}
                onBlur={formikEditCustomerRegistration.handleBlur}
              />
            </div>

            {/* <p className="text-black">
              Current Location:-
              {selectedRowData && selectedRowData.location.address}
            </p> */}

            <LocationSearchInput
              partnerID={selectedRowData && selectedRowData._id}
              isPartner={"no"}
              Success={generalSuccess}
              SuccessMsg={"location updated successfuly"}
            />

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      {loading && <LinearProgress />}
      <Navbar />
      <div className="relative sm:ml-[300px] bg-white h-screen">
        <TopNav />
        <div className="bg-white relative h-s  flex flex-col select-none">
          <div className="absolute top-4 left-4 justify-between sm:hidden md:hiddden   z-[1000]    lg:hidden">
            <UserNameTag />
          </div>
          <div className="border-b border-gray-400 sm:m-6 m-3 py-2 flex sm:flex-row flex-col justify-between">
            <div className="flex sm:flex-row flex-col justify-start ">
              <div className="flex flex-col justify-start text-start mt-8 ">
                <h1 className="text-lg sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
                  Customers
                </h1>
              </div>
              <div className=" sm:mt-8 mt-1 ">
                <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                  <SearchIconWrapper>
                    <SearchIcon sx={{ color: "#616569" }} />
                  </SearchIconWrapper>
                  <StyledInputBase
                    placeholder="Search…"
                    inputProps={{ "aria-label": "search" }}
                    value={searchTerm}
                    onChange={handleSearchChange}
                  />
                </Search>
              </div>

              <div className="mt-6">
                <FormControl sx={{ m: 1, minWidth: 186 }} size="small">
                  <InputLabel id="demo-select-small-label">
                    Filter{" "}
                    <FilterListIcon sx={{ color: "black" }} fontSize="medium" />
                  </InputLabel>
                  <Select
                    labelId="demo-select-small-label"
                    id="subcategory"
                    // value={subcategory}
                    value={subcategory}
                    label="subcategory"
                    onChange={handleChangeType}
                  >
                    {/* Map over the array of menu items to generate MenuItem components */}
                    {menuItems.map((item, index) => (
                      <MenuItem key={index} value={item.value}>
                        {item.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
            </div>

            <div className="mt-8 flex">
              <Button variant="contained" onClick={handleOpen_AddCompany}>
                Add Customer
              </Button>
            </div>
          </div>
          <div
            style={{
              // height: 400,
              // width: "100%",
              backgroundColor: "#fff",
              margin: "1rem",
            }}
          >
            <Paper sx={{ width: "100%", overflow: "auto" }}>
              <TableContainer sx={{ maxHeight: 800, minWidth: 1000 }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow className="select-none">
                      <TableCell
                        sx={{
                          fontWeight: 800,
                          fontSize: ["14px", "14px"],
                          width: "200px",
                        }}
                      >
                        Customer Name
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Email
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Phone Number
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Status
                      </TableCell>

                      <TableCell
                        align="center"
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Action
                      </TableCell>

                      {items}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => (
                        <TableRow
                          key={index}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                          className="hover:bg-gray-400 select-none"
                          // onDoubleClick={handleRowDoubleClick}
                          //  onDoubleClick={() => handleRowDoubleClick(row)}
                          onMouseEnter={() => {
                            setSelectedRowData(row);
                            //setIndex(index);
                          }}
                        >
                          <TableCell
                            className="flex flex-row w-10"
                            sx={{
                              // fontWeight: 800,
                              fontSize: [12, 13],
                              color: "black",
                            }}
                          >
                            {row.first_name} &nbsp;
                            {row.last_name}
                          </TableCell>

                          <TableCell
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.email}
                          </TableCell>

                          <TableCell
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.phone_number}
                          </TableCell>

                          <TableCell>
                            <Button
                              sx={{
                                fontSize: [12, 13],
                                borderWidth: "2px",
                                borderColor:
                                  row.status === "INACTIVE" ? "red" : "green",
                                borderStyle: "solid",
                                padding: "0.5rem", // Adjust padding as needed
                                display: "inline-block", // Ensures the border fits tightly around the text
                                borderRadius: "0.25rem",
                                color: "black",
                              }}
                            >
                              {row.status}
                            </Button>
                          </TableCell>
                          <TableCell align="right">
                            <IconButton aria-controls={actionId}>
                              <CiSettings
                                color="#111111"
                                size={23}
                                onClick={handleActionMenuOpen}
                              />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                className="select-none"
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
      </div>
    </>
  );
}
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "white",
  color: "black",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "35%"],
};
const style_box3 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "35%"],
};
