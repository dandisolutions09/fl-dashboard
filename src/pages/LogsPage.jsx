import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import endpoints from "../endpoints";
import { CiSettings } from "react-icons/ci";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  LinearProgress,
  Menu,
  MenuItem,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
import { styled, alpha } from "@mui/material/styles";
import { BsGearFill } from "react-icons/bs";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import {
  MdAirplanemodeActive,
  MdAirplanemodeInactive,
  MdEdit,
  MdOutlineAirplanemodeInactive,
} from "react-icons/md";
import { RiDeleteBinFill } from "react-icons/ri";
import { IoIosCloseCircle, IoIosCloseCircleOutline } from "react-icons/io";
import { useState } from "react";
import axios from "axios";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",

    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function LogsPage() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  const [incomingLogs, setIncomingLogs] = React.useState([]);
  const [stateToken, setToken] = useState();

  // function handleGetAllLogs() {
  //   setLoading(true);
  //   console.log("handle get all nurses called!");
  //   fetch("https://nurse-backend.onrender.com/get-logs")
  //     .then((response) => response.json())
  //     .then((json) => {
  //       console.log("rec logs", json);
  //       json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
  //       setRows(json);
  //       setLoading(false);
  //     });
  // }

  function handleGetAllRecords(usrId) {
    console.log("handle get all records called");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
  //  console.log("json_token", json_token.token);

    setToken(json_token.token);

    // setLoading(true);

    axios
      .get(endpoints.getLogs, {
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        console.log("Response data: ", response.data);

        // Your further processing logic

        const json = response.data;
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        setRows(json);
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        //setLoading(false);
      });
  }
  const columns = [
    { id: "created_at", label: "TIMESTAMP", minWidth: 100 },
    { id: "first_name", label: "USER", minWidth: 100 },
    { id: "user_role", label: "ROLE", minWidth: 100 },
    { id: "email", label: "EMAIL", minWidth: 100 },
    { id: "activity", label: "ACTIVITY", minWidth: 100 },
  ];

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  React.useEffect(() => {
    // handleGetAllLogs();
    handleGetAllRecords();
  }, []);

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <>
      {loading && <LinearProgress />}
      <Navbar />
      <div className="relative sm:ml-[300px] bg-white h-screen">
        <TopNav />
        <div className="bg-white relative h-s  flex flex-col">
          <div className="flex flex-col justify-start text-start mt-8 border-b m-2 py-1">
            <h1 className="text-lg sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
              Logs
            </h1>
          </div>
          <Paper
            sx={{
              width: "100%",
              overflow: "hidden",
              padding: "30px",
              margin: "20px",
            }}
          >
            <TableContainer sx={{ maxHeight: "100%" }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                        sx={{ fontWeight: "600", fontSize: "16px" }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          role="checkbox"
                          tabIndex={-1}
                          key={row._id}
                          sx={{
                            "&:hover": { backgroundColor: "rgb(212 212 212)" },
                            cursor: "pointer",
                          }}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.format && typeof value === "number"
                                  ? column.format(value)
                                  : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
        </div>
      </div>
    </>
  );
}
