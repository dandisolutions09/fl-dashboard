import React from "react";

import { MdBrightness1 } from "react-icons/md";
import { TbRobot } from "react-icons/tb";
import { MdGames } from "react-icons/md";
import { FaPlus, FaUsers } from "react-icons/fa";
import game11 from "../assets/admin-logo.png";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import { ImStatsDots } from "react-icons/im";
import { HiServer } from "react-icons/hi";
import { Gauge, gaugeClasses } from "@mui/x-charts/Gauge";
import endpoints from "../endpoints";
import axios from "axios";
import { useState } from "react";
import UserNameTag from "../components/UserNameTag";
import { Chart } from "react-google-charts";
import BarsDataset from "../components/BarGraph";
import FilterListIcon from "@mui/icons-material/FilterList";

export default function HomePage() {
  const [loading, setLoading] = React.useState(false);
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();

  const [totalCustomers, setTotalCustomers] = useState();
  const [totalProviders, setTotalProviders] = useState();
  const [totalJobs, setTotalJobs] = useState();

  const [total_ongoingJobs, setTotal_ongoingJobs] = useState();
  const [total_cancelledJobs, setTotal_cancelled] = useState();

  const [total_completedJobs, setTotal_completedJobs] = useState();
  const [total_Admins, setTotal_Admins] = useState();
  const [total_upcomingJobs, setTotal_upcomingJobs] = useState();

  const [total_activeProviders, setTotal_activeProviders] = useState();
  const [total_inactiveProviders, setTotal_inactiveProviders] = useState();

  const [total_activeCustomers, setTotal_activeCustomers] = useState();
  const [total_inactiveCustomers, setTotal_inactiveCustomers] = useState();

  const [subcategory, setSubcategory] = React.useState("all");

  const settings = {
    width: 200,
    height: 500,
    value: 60,
  };

  const menuItems = [
    { value: "ALL", label: "All" },
    { value: "today", label: "TODAY" },
    // { value: "INACTIVE", label: "INACTIVE" },
    // { value: "FEATURED", label: "FEATURED" },
  ];

  const options_providers = {
    title: "Partners Chart",
    pieSliceText: "percentage",
    legend: { position: "bottom" },
  };

  const options_jobs = {
    title: "Jobs Chart",
    pieSliceText: "percentage",
    legend: { position: "bottom" },
    slices: {
      0: { color: "blue" }, // ongoing jobs
      1: { color: "green" }, // completed jobs
      2: { color: "red" }, // cancelled jobs
    },
  };

  const options_customers = {
    title: "Customers Chart",
    pieSliceText: "percentage",
    legend: { position: "bottom" },
  };

  const handleChangeType = (event) => {
    console.log("event selected:->", event.target.value);
    setSubcategory(event.target.value);

    if (event.target.value) {
      filterCustomers(event.target.value);
    }
  };

  function filterCustomers(filter) {
    setLoading(true);
    console.log("sort jobs functions called", filter);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
   // console.log("json_token", json_token.token);

    axios
      //.get(endpoints.getBeautyServices, {
      .get(`${endpoints.getStats}?filter=${filter}`, {
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        console.log("response", response.data);
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoading(false);
      });
    /// } else {
    //console.log("Subcategiry empty", subcategory);
    // }
  }

  async function getAdminDetails() {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
    console.log("json_token", stored_json_data);
    console.log(
      "full url",
      `${endpoints.getAdminById}/${stored_json_data.admin_id}`
    );

    try {
      // Replace 'YOUR_ENDPOINT' with the actual endpoint URL
      const response = await axios.get(
        `${endpoints.getAdminById}/${stored_json_data.admin_id}`,
        {
          headers: {
            Authorization: `Bearer ${stored_json_data.token}`,
          },
        }
      );
      //setAdmin(response.data);
      console.log("response", response.data);
      setFirstName(response.data.first_name);
      setLastName(response.data.last_name);
    } catch (error) {
      //setError(error.message);
      console.log("error", error);
    }
  }

  //GET STATISTICS
  async function getStats() {
    console.log("get sats!!");
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
    console.log("json_token", stored_json_data);
    console.log("full url", `${endpoints.getStats}`);

    try {
      // Replace 'YOUR_ENDPOINT' with the actual endpoint URL
      const response = await axios.get(`${endpoints.getStats}`, {
        headers: {
          Authorization: `Bearer ${stored_json_data.token}`,
        },
      });
      //setAdmin(response.data);
      console.log("response Stats", response.data);
      setTotalCustomers(response.data.customers);
      setTotalProviders(response.data.providers);
      setTotalJobs(response.data.total_jobs);

      setTotal_ongoingJobs(parseInt(response.data.ongoing_jobs));
      setTotal_cancelled(parseInt(response.data.cancelled_jobs));
      setTotal_completedJobs(parseInt(response.data.completed_jobs));

      setTotal_activeProviders(parseInt(response.data.active_providers));
      setTotal_inactiveProviders(parseInt(response.data.inactive_providers));

      setTotal_activeCustomers(parseInt(response.data.active_customers));
      setTotal_inactiveCustomers(parseInt(response.data.inactive_customers));
      setTotal_upcomingJobs(parseInt(response.data.upcoming_jobs));

      // setTotal_ongoingJobs(parseInt(response.data.active_customers));
      // setTotal_cancelledJobs(parseInt(response.data.inactive_customers));

      // setTotal_completedJobs(parseInt(response.data.inactive_customers));

      setTotal_Admins(response.data.admins);

      // console.log("providers", response.data.ac);

      // setFirstName(response.data.first_name);
      // setLastName(response.data.last_name);
    } catch (error) {
      //setError(error.message);
      console.log("error", error);
    }
  }

  const data_providers = [
    ["Providers", "Providers (in hundreds)"],
    ["Active Partners", total_activeProviders],
    ["Inactive Partners", total_inactiveProviders],
  ];

  const data_customers = [
    ["Customers", "Customers (in hundreds)"],
    ["Active Customers", total_activeCustomers],
    ["Inactive Customers", total_inactiveCustomers],
  ];

  const data_jobs = [
    ["Jobs", "Jobs (in hundreds)"],
    ["ongoing", total_ongoingJobs],
    ["completed", total_completedJobs],
    ["cancelled", total_cancelledJobs],
    ["upcoming", total_upcomingJobs],
  ];

  console.log("Data for active providers", typeof total_activeProviders);

  console.log("Data for inactive providers", total_inactiveProviders);

  React.useEffect(() => {
    //handleGetAllRecords();
    getAdminDetails();

    getStats();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      <Navbar />
      <div className="relative h-screen sm:ml-[300px] select-none">
        <TopNav />
        <div
          className="flex flex-col relative bg-[#fff] md:w-500px"
          style={{ minHeight: "100%" }}
        >
          <div className="absolute top-4 left-8 justify-between sm:hidden md:hiddden z-[1000] lg:hidden">
            <UserNameTag />
          </div>
          <div className="flex flex-col m-6 py-2 justify-start text-start mt-8 border-b border-gray-400">
            <h1 className="text-base sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
              Dashboard
            </h1>
          </div>
          <div className="flex flex-col gap-3 ">
            {/* Start*/}
            <div className="flex sm:flex-row flex-col justify-between px-4 gap-3">
              <div
                className="flex-1 bg-cover bg-center w-full rounded-lg border border-gray-300"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    User Statistics
                  </Typography>
                </div>
                <div className="m-3 flex gap-3 ">
                  <div className="flex flex-col gap-2 w-full">
                    <div className="border border-gray-300 p-2 sm:h-[110px]  shadow-md shadow-gray-400 rounded-lg bg-[#fff] flex ">
                      <div className="flex flex-row gap-2 px-1">
                        <FaUsers style={{ color: "#000" }} size={25} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Customers
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {totalCustomers && totalCustomers}
                          </Typography>
                        </div>
                      </div>
                    </div>
                    <div className="border border-gray-300 p-4 shadow-md sm:h-[110px] rounded-xl bg-[#fff] shadow-gray-400 flex ">
                      <div className="flex flex-row gap-2">
                        <HiServer style={{ color: "#000" }} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Companies
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {totalProviders && totalProviders}
                          </Typography>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="flex flex-col gap-2 w-full">
                    <div className="border border-gray-300 p-2 sm:h-[110px]  shadow-md shadow-gray-400 rounded-lg bg-[#fff] flex ">
                      <div className="flex flex-row gap-2 px-1">
                        <FaUsers style={{ color: "#000" }} size={25} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Providers
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {totalProviders && totalProviders}
                          </Typography>
                        </div>
                      </div>
                    </div>
                    <div className="border border-gray-300 p-4 shadow-md sm:h-[110px] rounded-xl bg-[#fff] shadow-gray-400 flex ">
                      <div className="flex flex-row gap-2">
                        <HiServer style={{ color: "#000" }} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Admins
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {total_Admins && total_Admins}
                          </Typography>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="flex-1 bg-cover bg-center w-full  rounded-lg border border-gray-300"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    Jobs Statistics
                  </Typography>
                </div>
                <div className="m-3 flex gap-3 ">
                  <div className="flex flex-col gap-2 w-full">
                    <div className="border border-gray-300 p-2 sm:h-[110px]  shadow-md shadow-gray-400 rounded-lg bg-[#fff] flex ">
                      <div className="flex flex-row gap-2 px-1">
                        <FaUsers style={{ color: "#000" }} size={25} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Total Jobs
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {totalJobs && totalJobs}
                          </Typography>
                        </div>
                      </div>
                    </div>
                    <div className="border border-gray-300 p-4 shadow-md sm:h-[110px] rounded-xl bg-[#fff] shadow-gray-400 flex ">
                      <div className="flex flex-row gap-2">
                        <HiServer style={{ color: "#000" }} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Cancelled Jobs
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {total_cancelledJobs && total_cancelledJobs}
                          </Typography>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="flex flex-col  gap-2 w-full">
                    <div className="border border-gray-300 p-2 sm:h-[110px]  shadow-md shadow-gray-400 rounded-lg bg-[#fff] flex ">
                      <div className="flex flex-row gap-2 px-1">
                        <FaUsers style={{ color: "#000" }} size={25} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Ongoing Jobs
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {total_ongoingJobs && total_ongoingJobs}
                          </Typography>
                        </div>
                      </div>
                    </div>
                    <div className="border border-gray-300 p-4 shadow-md sm:h-[110px] rounded-xl bg-[#fff] shadow-gray-400 flex ">
                      <div className="flex flex-row gap-2">
                        <HiServer style={{ color: "#000" }} />
                        <div className="  md:max-w-lg">
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [14] }}
                          >
                            Completed Jobs
                          </Typography>
                          <Typography
                            variant="body2"
                            color="#000"
                            sx={{ fontSize: [30, 40] }}
                          >
                            {total_completedJobs && total_completedJobs}
                          </Typography>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex sm:flex-row flex-col  justify-between px-4 gap-3">
              <div
                className="flex-1 bg-cover bg-center w-full  rounded-lg shadow-md shadow-gray-400"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    Provider Statistics
                  </Typography>
                </div>
                <div className="mt-4 grid md:grid-cols-2 lg:grid-cols-2 gap-4 px-4 h-64 justify-center text-center">
                  {/* <Gauge
                    {...settings}
                    cornerRadius="50%"
                    sx={(theme) => ({
                      [`& .${gaugeClasses.valueText}`]: {
                        fontSize: [28, 30, 40],
                      },
                      [`& .${gaugeClasses.valueArc}`]: {
                        fill: "#52b202",
                      },
                      [`& .${gaugeClasses.referenceArc}`]: {
                        fill: theme.palette.text.disabled,
                      },
                    })}
                  /> */}

                  <Chart
                    chartType="PieChart"
                    data={data_providers}
                    options={options_providers}
                    width={"100%"}
                    height={"250px"}
                  />

                  <Chart
                    chartType="PieChart"
                    data={data_customers}
                    options={options_customers}
                    width={"100%"}
                    height={"250px"}
                  />

                  {/* <div>
                    <Typography
                      sx={{ fontSize: [24], marginTop: 1 }}
                      className=" text-[#080808]"
                    >
                      Provider Summary
                    </Typography>

                    <div>
                      <Typography
                        sx={{ fontSize: [14], marginTop: 1 }}
                        className="m-1 text-[#080808]"
                      >
                        Active Providers :{" "}
                        {total_activeProviders && total_activeProviders}
                      </Typography>

                      <Typography
                        sx={{ fontSize: [14], marginTop: 1 }}
                        className="m-1 text-[#080808]"
                      >
                        Inactive Providers :{" "}
                        {total_inactiveProviders && total_inactiveProviders}
                      </Typography>
                    </div>
                  </div> */}
                </div>
              </div>
              <div
                className="flex-1 bg-cover bg-center w-full sm:w-[430px] rounded-lg shadow-md shadow-gray-400"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    Bookings Statistics
                  </Typography>
                </div>
                <div className="mt-4 grid md:grid-cols-2 lg:grid-cols-1 gap-4 px-4 h-64 justify-center text-center">
                  {/* <BarsDataset /> */}

                  <div className="flex flex-row">
                    <Chart
                      chartType="PieChart"
                      data={data_jobs}
                      options={options_jobs}
                      width={"100%"}
                      height={"250px"}
                    />
                    <FormControl sx={{ m: 1, minWidth: 186 }} size="small">
                      <InputLabel id="demo-select-small-label">
                        Filter{" "}
                        <FilterListIcon
                          sx={{ color: "black" }}
                          fontSize="medium"
                        />
                      </InputLabel>
                      <Select
                        labelId="demo-select-small-label"
                        id="subcategory"
                        // value={subcategory}
                        value={subcategory}
                        label="subcategory"
                        onChange={handleChangeType}
                      >
                        {/* Map over the array of menu items to generate MenuItem components */}
                        {menuItems.map((item, index) => (
                          <MenuItem key={index} value={item.value}>
                            {item.label}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>

                  {/* <Chart
                    chartType="Bar"
                    width="100%"
                    height="400px"
                    data={data_jobs}
                    options={options_jobs}
                  />{" "} */}
                  {/* <Gauge
                    value={75}
                    startAngle={-110}
                    endAngle={110}
                    sx={{
                      [`& .${gaugeClasses.valueText}`]: {
                        fontSize: [10, 27, 30],
                        transform: "translate(0px, 0px)",
                      },
                    }}
                    text={({ value, valueMax }) => `${value} / ${valueMax}`}
                  /> */}
                </div>
              </div>
            </div>
            {/* end */}
          </div>
        </div>
      </div>
    </>
  );
}
