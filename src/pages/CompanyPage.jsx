import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import endpoints from "../endpoints";
import { CiSettings } from "react-icons/ci";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  LinearProgress,
  Menu,
  MenuItem,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
import { styled, alpha } from "@mui/material/styles";
import { BsGearFill } from "react-icons/bs";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import {
  MdAirplanemodeActive,
  MdAirplanemodeInactive,
  MdEdit,
  MdOutlineAirplanemodeInactive,
} from "react-icons/md";
import { RiDeleteBinFill } from "react-icons/ri";
import { IoIosCloseCircle, IoIosCloseCircleOutline } from "react-icons/io";
import UserNameTag from "../components/UserNameTag";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",

    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function CompanyPage() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openAction, setOpenAction] = React.useState(false);
  const [isOPen, setIsOpen] = React.useState(false);
  const [openAddCompany, setOpenAddCompany] =
    React.useState(false);
  const actionId = "primary-action-menu";

  const handleOpen_AddCompany = () =>{
    setOpenAddCompany(true);
  }
  const handleClose_AddCompany = () => {
    setOpenAddCompany(false);
  };

  const items = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={actionId}
      keepMounted
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      open={isOPen}
      onClose={handleActionMenuClose}
    >
      <MenuItem>
        <MdEdit /> Edit
      </MenuItem>
      <MenuItem>
        <MdAirplanemodeActive /> Activate
      </MenuItem>
      <MenuItem>
        <MdOutlineAirplanemodeInactive /> Deactivate
      </MenuItem>
      <MenuItem>
        <RiDeleteBinFill /> Delete
      </MenuItem>
    </Menu>
  );
  function handleActionMenuClose() {
    setIsOpen(false);
  }
  function handleActionMenuOpen() {
    setIsOpen(true);
  }

  function handleGetAllRecords(usrId) {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
   // console.log("json_token", json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);


    fetch(endpoints.getPartners, {
      method: "GET",
      headers: {
        'Authorization': `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log("JSON:->", json);
        setRows(json);
        // Your further processing logic
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  React.useEffect(() => {
    handleGetAllRecords();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      <Modal
        open={openAddCompany}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4 text-[#111111]">
              Add Company Information
            </h2>
            <IoIosCloseCircle
              color="#000"
              size={23}
              cursor={"pointer"}
              onClick={handleClose_AddCompany}
            />
          </div>

          <form>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="company_name"
                  name="company_name"
                  label="Company Name"
                  placeholder="Company Name"
                />

                <TextField
                  type="email"
                  id="email"
                  name="email"
                  label="Email"
                  placeholder="Email"
                />
              </div>
            </div>
            <div className="flex flex-col gap-3 ">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="picture"
                  name="picture"
                  label="Profile picture"
                  placeholder=" Profile picture"
                />
                <TextField id="VAT" name="VAT" label="VAT" placeholder="VAT" />
              </div>
            </div>

            <div className="flex flex-row gap-3 ">
              <TextField
                type="text"
                id="country"
                name="country"
                label="Country"
                placeholder="Country"
              />
              <TextField
                type="phone"
                id="phone"
                name="phone"
                label="Phone"
                placeholder="Phone"
              />
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      {loading && <LinearProgress />}
      <Navbar />
      <div className="relative sm:ml-[300px] bg-white h-screen select-none">
        <TopNav />
        <div className="bg-white relative h-s  flex flex-col select-none">
          <div className="absolute top-4 left-4 justify-between sm:hidden md:hiddden   z-[1000]    lg:hidden">
            <UserNameTag />
          </div>
          <div className="border-b border-gray-400 sm:m-6 m-3 py-2 flex sm:flex-row flex-col justify-between">
            <div className="flex sm:flex-row flex-col justify-start ">
              <div className="flex flex-col justify-start text-start mt-8 ">
                <h1 className="text-lg sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
                  Company
                </h1>
              </div>
              <div className=" sm:mt-8 mt-1 ">
                <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                  <SearchIconWrapper>
                    <SearchIcon sx={{ color: "#616569" }} />
                  </SearchIconWrapper>
                  <StyledInputBase
                    placeholder="Search…"
                    inputProps={{ "aria-label": "search" }}
                  />
                </Search>
              </div>
            </div>

            <div className="mt-8 flex">
              <Button variant="contained" onClick={handleOpen_AddCompany}>
                Add Company
              </Button>
            </div>
          </div>
          <div
            style={{
              // height: 400,
              // width: "100%",
              backgroundColor: "#fff",
              margin: "1rem",
            }}
          >
            <Paper sx={{ width: "100%", overflow: "auto" }}>
              <TableContainer sx={{ maxHeight: 800, minWidth: 1000 }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow className="select-none">
                      <TableCell
                        sx={{
                          fontWeight: 800,
                          fontSize: ["14px", "14px"],
                          width: "200px",
                        }}
                      >
                        Provider Name
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Company Name
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Email
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Signup Date
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Phone Number
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Wallet Balance
                      </TableCell>

                      {/* <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Manager Services
                      </TableCell> */}

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Status
                      </TableCell>

                      <TableCell
                        align="center"
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Action
                      </TableCell>

                      {/* <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Balance
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  House selling Price
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  House Number{" "}
                </TableCell> */}

                      {/* <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Project{" "}
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Location{" "}
                </TableCell> */}
                      {items}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => (
                        <TableRow
                          key={index}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                          className="hover:bg-gray-400 select-none"
                          //onDoubleClick={handleRowDoubleClick}
                          // onDoubleClick={() => handleRowDoubleClick(row)}
                          // onMouseEnter={() => {
                          //   setSelectedRowData(row);
                          //   setIndex(index);
                          // }}
                        >
                          {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                          <TableCell
                            className="flex flex-row w-10"
                            sx={{
                              // fontWeight: 800,
                              fontSize: ["12px", "15px"],
                              width: "200px",
                            }}
                          >
                            {row.first_name} &nbsp;
                            {row.last_name}
                          </TableCell>
                          <TableCell>{row.main_businessName}</TableCell>
                          <TableCell>{row.email}</TableCell>
                          {/* <TableCell>
                            {parseFloat(row.email).toLocaleString()}
                          </TableCell> */}
                          <TableCell>
                            {/* {parseFloat(row.house_bonus).toLocaleString()} */}
                          </TableCell>
                          <TableCell>
                            {/* {parseFloat(
                        row.house_bonus - row.bonus_paid
                      ).toLocaleString()} */}
                          </TableCell>
                          {/* Selling Price Column */}
                          <TableCell>
                            {/* Map through each selling price and format with commas */}
                            {/* {row.house.map((x, index) => (
                        <div key={index}>
                          {parseFloat(x.selling_price).toLocaleString()}
                        </div>
                      ))} */}
                          </TableCell>
                          {/* house unit column */}
                          <TableCell>
                            {/* {row.house.map((x, index) => (
                        <div key={index} className="flex flex-row">
                          {x.house_number},{x.house_type}{" "}
                        </div>
                      ))} */}
                          </TableCell>
                          {/* Location column */}
                          <TableCell align="right">
                            {/* {row.house_information.map((house, index) => (
                    <div key={index}>{house.house_status} </div>
                  ))}
                */}
                            <IconButton aria-controls={actionId}>
                              <CiSettings
                                color="#111111"
                                size={23}
                                onClick={handleActionMenuOpen}
                              />
                            </IconButton>

                            {/* 
                      {row.house.map((x, index) => (
                        <div key={index} className="flex">
                          <LocationOnOutlinedIcon fontSize="20px" />{" "}
                          {x.residence}
                        </div>
                      ))} */}
                          </TableCell>
                          {/* Status column */}
                          <TableCell align="right">
                            {/* {row.house.map((x, index) => (
                        <div key={index} className="flex">
                          <LocationOnOutlinedIcon fontSize="20px" />{" "}
                          {x.location}
                        </div>
                      ))} */}
                          </TableCell>

                          {/* Amount Paid Column */}
                          {/* Balance Column */}
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                className="select-none"
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
      </div>
    </>
  );
}
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",

};
