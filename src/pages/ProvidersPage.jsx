import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import endpoints from "../endpoints";
import { CiSettings } from "react-icons/ci";
import CircularProgress from "@mui/joy/CircularProgress";
import AccordionActions from "@mui/material/AccordionActions";
import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  IconButton,
  InputLabel,
  LinearProgress,
  Menu,
  MenuItem,
  Modal,
  Radio,
  RadioGroup,
  Select,
  Step,
  StepButton,
  StepLabel,
  Stepper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import { TextareaAutosize as BaseTextareaAutosize } from "@mui/base/TextareaAutosize";
import TablePagination from "@mui/material/TablePagination";
import { styled, alpha } from "@mui/material/styles";
import { BsGearFill } from "react-icons/bs";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import {
  MdAirplanemodeActive,
  MdAirplanemodeInactive,
  MdEdit,
  MdModeEdit,
  MdOutlineAirplanemodeInactive,
} from "react-icons/md";
import { RiDeleteBinFill } from "react-icons/ri";
import {
  IoIosCloseCircle,
  IoIosCloseCircleOutline,
  IoMdCloseCircle,
} from "react-icons/io";
import { useFormik } from "formik";
import { useState } from "react";
import hsp from "../assets/avatar5.jpg";
import hsp4 from "../assets/admin-logo.png";
import hsp1 from "../assets/cover-photo-avatar.png";
import axios from "axios";
import { FaCamera } from "react-icons/fa";
import utils from "../utils";
import { Textarea } from "../styles";
import AdminAddedSuccess from "../components/Alerts/AdminAddedSuccess";
import UserNameTag from "../components/UserNameTag";
import AdminAddfailed from "../components/Alerts/AdminAddfailed";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
//import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Fade from "@mui/material/Fade";
import LocationSearchInput from "../components/GoogleLocation";
import GeneralError from "../components/Alerts-2/GeneralError";
import GeneralSuccess from "../components/Alerts-2/GeneralSuccess";
// import LocationSearchInput from "../components/GoogleLocation";

// import imagemin from "imagemin";
// import imageminJpegtran from "imagemin-jpegtran";
// import imageminPngquant from "imagemin-pngquant";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",

    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function ProvidersPage() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openAction, setOpenAction] = React.useState(false);
  const [isOPen, setIsOpen] = React.useState(false);
  const [openAddCompany, setOpenAddCompany] = React.useState(false);
  const [openfile, setOpenfile] = useState(false);
  const [openBgfile, setOpenBgfile] = useState(false);
  const [searchValue, setSearchValue] = React.useState();
  const [searchTerm, setSearchTerm] = React.useState();

  const [openProviderUpdateSuccess, setOpenProviderUpdateSuccess] =
    useState(false);

  const [openmap, setOpenmap] = React.useState(false);
  const { time_array } = utils;
  const [file, setFile] = useState();
  const [bgfile, setBgFile] = useState();
  const [activeStep, setActiveStep] = React.useState(0);
  const [openEdit, setOpenEdit] = React.useState(false);
  const [skipped, setSkipped] = React.useState(new Set());

  const [completed, setCompleted] = React.useState({});
  const profilePicRef = React.useRef(null);
  const [pp, setPP] = useState("");
  const [openAddAdminSuccess, setOpenAddAdminSuccess] = React.useState(false);
  const [openImageSuccess, setOpenImageSuccess] = React.useState(false);
  const [openAddAdminFailed, setOpenAddAdminFailed] = React.useState(false);
  const [openAddAdminFailed_emailTaken, setOpenAddAdminFailed_emailTaken] =
    React.useState(false);

  const [openProviderUpdateFailed, setOpenProviderUpdateFailed] =
    React.useState(false);
  // const [subcategory, setSubcategory] = React.useState("mens_hair");

  const [BookingPreferencevalue, setBookingPreferencevalueValue] =
    React.useState();
  function reloadPage() {
    window.location.reload();
  }
  // const [paymentMethod, setPaymentMethod] = React.useState("Mobile Service");
  const [selectedRowData, setSelectedRowData] = useState();

  const [serviceType, setServiceType] = useState();
  const [paymentmethod, setPaymentMethod] = useState();

  const [bp, setBP] = useState();

  const [stateToken, setToken] = useState();

  const [partnerServices, setPartnerServices] = useState();
  const [expanded, setExpanded] = React.useState(false);
  const [openNewServiceModal, setOpenAddNewServiceModal] = useState(false);
  const [openAddNewDurationPriceModal, setOpenAddNewDurationPriceModal] =
    useState(false);

  const [serviceHolder, setServiceHolder] = useState();
  const [sortedPartnerServices, setSortedServices] = useState();
  const [openDurationTimeSuccess, setOpenDurationTimeSuccess] = useState(false);

  const [subcategory, setSubcategory] = React.useState("all");

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  const [generalError, setGeneralError] = useState();
  const [generalSuccess, setGeneralSuccess] = useState();
  //setOpenAddNewDurationPriceModal

  const menuItems = [
    { value: "ALL", label: "All" },
    { value: "ACTIVE", label: "ACTIVE" },
    { value: "INACTIVE", label: "INACTIVE" },
    { value: "FEATURED", label: "FEATURED" },
  ];

  let globalData = [];

  // const hairTypes = [
  //   {
  //     title: "Men's Hair",
  //     content:
  //       "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.",
  //   },
  //   {
  //     title: "Women's Hair",
  //     content:
  //       "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.",
  //   },
  //   // Add more categories as needed
  // ];

  const hairServices = {
    "Face Services": [
      {
        type: "Facial",
        status: "available",
        description: "",
        localization: "Kenya",
        subcategory: "Face Services",
        created_at: "2024-04-15 12:02:22",
      },
    ],
    "Nail Services": [
      {
        type: "Nail ",
        status: "available",
        description: "",
        localization: "Kenya",
        subcategory: "Nail Services",
        created_at: "2024-04-15 12:05:01",
      },
    ],
    "Women Hair": [
      {
        type: "locs",
        status: "available",
        description: "",
        localization: "all-over",
        subcategory: "Women Hair",
        created_at: "2024-04-16 15:17:21",
      },
      {
        type: "locs",
        status: "available",
        description: "",
        localization: "all-over",
        subcategory: "Women Hair",
        created_at: "2024-04-16 15:17:42",
      },
    ],
  };

  const handleChangeType = (event) => {
    console.log("event selected:->", event.target.value);
    setSubcategory(event.target.value);

    if (event.target.value) {
      filterPartners(event.target.value);
    }
  };

  function filterPartners(filter) {
    setLoading(true);

    console.log("sort services functions called", filter);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    axios
      //.get(endpoints.getBeautyServices, {
      .get(`${endpoints.filterPartners}?status=${filter}`, {
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        if (response.data) {
          const json = response.data;
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log(
            "Response filtered data: after sorting:--> ",
            response.data
          );
          setRows(json);
          console.log("filtered partners");
        } else {
          setRows([]);
        }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoading(false);
      });
    /// } else {
    //console.log("Subcategiry empty", subcategory);
    // }
  }

  const handleChangeSelectBeauty = (event) => {
    console.log("event selected:->", event.target.value);
    setSubcategory(event.target.value);

    if (event.target.value) {
      sortServices(event.target.value);
    }
  };

  const handleChangeServiceLocation = (event) => {
    setServiceType(event.target.value);
    console.log("Service Location:->", event.target.value);
  };

  const handleChangeRadio_paymentMethod = (event) => {
    setPaymentMethod(event.target.value);
    console.log("payment method:->", event.target.value);
  };

  const updatePartnerBookingPreference = async (data) => {
    setLoading(true);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    const bookingpreference = {
      payment_option: paymentmethod,
      service_type: serviceType,
    };

    console.log("main object", bookingpreference);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.addProviderBookingPreference}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(bookingpreference),
        }
      );
      console.log("response after edit:->>", response);

      response.json().then((data) => {
        console.log("json", data);

        if (data.status == "Error") {
          console.log("Error detected");
          setErrorMsg("Something went wrong");
          setGeneralError(true);
          return;
        } else if (data.status == "Success") {
          //setOpenSuccess(true);
          setSuccessMsg("Partner Booking Preference updated successfully!");
          setGeneralSuccess(true);
          // setTimeout(() => {
          //   setOpenAddCompany(false);
          //   reloadPage();
          // }, 2000);
        }
      });
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  };

  //helol
  const submitBookingPreference = async () => {
    updatePartnerBookingPreference();
  };

  //

  const blue = {
    100: "#DAECFF",
    200: "#b6daff",
    400: "#3399FF",
    500: "#007FFF",
    600: "#0072E5",
    900: "#003A75",
  };

  const grey = {
    50: "#F3F6F9",
    100: "#E5EAF2",
    200: "#DAE2ED",
    300: "#C7D0DD",
    400: "#B0B8C4",
    500: "#9DA8B7",
    600: "#6B7A90",
    700: "#434D5B",
    800: "#303740",
    900: "#1C2025",
  };

  const steps = [
    "Partner Registation",
    "Add Availability",
    "Booking Preference",
    "Add Services",
    "Venue Address",
  ];

  const actionId = "primary-action-menu";

  const handleOpen_file = () => setOpenfile(true);
  const handleClose_file = () => setOpenfile(false);
  const handleClose_Bgfile = () => setOpenBgfile(false);

  const handleOpen_AddCompany = () => {
    setOpenAddCompany(true);
  };

  function handleGetSortedServices(searchVal) {
    console.log("sorted services called...");
    setLoading(true);
    console.log("handle get all records called");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // console.log("json_token", json_token.token);

    if (subcategory !== null) {
      const filtered_type = "all";
      console.log("subcategory", filtered_type);

      axios
        .get(`${endpoints.getSortedServices}?subcategory=all`, {
          headers: {
            Authorization: `Bearer ${json_token.token}`,
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          //const json = response.data;
          // json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response sorted services: ", response.data);
          setPartnerServices(response.data);
          // if (response.data) {
          //   setPartnerServices(response.data);
          // } else {
          //   setPartnerServices([]);
          // }

          // setRows(json);

          // if (searchVal === undefined || searchVal.trim().length === 0) {
          //   console.log("my json", rows);
          //   setRows(json);
          // } else {
          //   const results = json.filter((user) => {
          //     const Type = user.type ? user.type.toLowerCase() : "";
          //     const searchTerm = searchVal.toLowerCase();
          //     return Type.includes(searchTerm);
          //   });
          //   console.log("my results", results);
          //   setRows(results);
          // }
        })
        .catch((error) => {
          console.error("There was a problem with your Axios request:", error);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      console.log("Subcategiry empty", subcategory);
    }
  }

  function handleGetSortedPartnerServices(searchVal) {
    setLoading(true);
    console.log("handle get all records called");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    if (subcategory !== null) {
      const filtered_type = subcategory;
      console.log("subcategory", filtered_type);

      axios
        .get(
          `${endpoints.getSortedPartnerServices}?partner_id=${selectedRowData._id}`,
          {
            headers: {
              Authorization: `Bearer ${json_token.token}`,
              "Content-Type": "application/json",
            },
          }
        )
        .then((response) => {
          //const json = response.data;
          // json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response sorted data: ", response.data);
          setSortedServices(response.data.partner_services);
          // if (response.data) {
          //   setPartnerServices(response.data);
          // } else {
          //   setPartnerServices([]);
          // }

          // setRows(json);

          // if (searchVal === undefined || searchVal.trim().length === 0) {
          //   console.log("my json", rows);
          //   setRows(json);
          // } else {
          //   const results = json.filter((user) => {
          //     const Type = user.type ? user.type.toLowerCase() : "";
          //     const searchTerm = searchVal.toLowerCase();
          //     return Type.includes(searchTerm);
          //   });
          //   console.log("my results", results);
          //   setRows(results);
          // }
        })
        .catch((error) => {
          console.error("There was a problem with your Axios request:", error);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      console.log("Subcategiry empty", subcategory);
    }
  }
  const handleOpen_EditProvider = () => {
    console.log("partner", selectedRowData);
    formikEditPartnerRegistration.setValues({
      email: selectedRowData.email,
      first_name: selectedRowData.first_name,
      last_name: selectedRowData.last_name,
      email: selectedRowData.email,
      phone_number: selectedRowData.phone_number,
      main_businessName: selectedRowData.main_businessName,
      bio: selectedRowData.bio,
    });

    handleGetSortedServices();

    if (
      selectedRowData.partner_availability.monday.opening_time == "" ||
      selectedRowData.partner_availability.monday.closing_time == "" ||
      selectedRowData.partner_availability.tuesday.opening_time == "" ||
      selectedRowData.partner_availability.tuesday.closing_time == "" ||
      selectedRowData.partner_availability.wednesday.opening_time == "" ||
      selectedRowData.partner_availability.wednesday.closing_time == "" ||
      selectedRowData.partner_availability.thursday.opening_time == "" ||
      selectedRowData.partner_availability.thursday.closing_time == "" ||
      selectedRowData.partner_availability.friday.opening_time == "" ||
      selectedRowData.partner_availability.friday.closing_time == "" ||
      selectedRowData.partner_availability.saturday.opening_time == "" ||
      selectedRowData.partner_availability.saturday.closing_time == "" ||
      selectedRowData.partner_availability.sunday.opening_time == "" ||
      selectedRowData.partner_availability.sunday.closing_time == ""
    ) {
      console.log("opening time is empty");
    }

    formikPartnerAvailability.setValues({
      monday_opening_time:
        selectedRowData.partner_availability.monday.opening_time,
      monday_closing_time:
        selectedRowData.partner_availability.monday.closing_time,

      tuesday_opening_time:
        selectedRowData.partner_availability.tuesday.opening_time,
      tuesday_closing_time:
        selectedRowData.partner_availability.tuesday.closing_time,

      wednesday_opening_time:
        selectedRowData.partner_availability.wednesday.opening_time,
      wednesday_closing_time:
        selectedRowData.partner_availability.wednesday.closing_time,

      thursday_opening_time:
        selectedRowData.partner_availability.thursday.opening_time,
      thursday_closing_time:
        selectedRowData.partner_availability.thursday.closing_time,

      friday_opening_time:
        selectedRowData.partner_availability.friday.opening_time,
      friday_closing_time:
        selectedRowData.partner_availability.friday.closing_time,

      saturday_opening_time:
        selectedRowData.partner_availability.saturday.opening_time,
      saturday_closing_time:
        selectedRowData.partner_availability.saturday.closing_time,

      sunday_opening_time:
        selectedRowData.partner_availability.sunday.opening_time,
      sunday_closing_time:
        selectedRowData.partner_availability.sunday.closing_time,
    });

    setPP(selectedRowData.profile_pic);
    setBP(selectedRowData.logo);
    setServiceType(selectedRowData.service_type);
    setPaymentMethod(selectedRowData.payment_option);
    console.log("partener services", partnerServices);
    handleGetSortedPartnerServices();

    setOpenEdit(true);

    //setPartnerServices(selectedRowData.partner_services);

    console.log("partner services:->", selectedRowData.partner_services);
  };

  ///handleOpen_DeleteProvider

  // async const handleOpen_DeleteProvider = () => {

  async function handleOpen_DeleteProvider() {
    //setOpenEdit(false);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    console.log(selectedRowData);

    try {
      //setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.deleteProvider}/${selectedRowData._id}`,

        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          //body: JSON.stringify(partnerRegistrationObject),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        setOpenAddAdminSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  }
  const handleClose_EditProvider = () => {
    setOpenEdit(false);
  };
  const handleClose_AddCompany = () => {
    setOpenAddCompany(false);
  };

  const handleOpenAddNewServiceModal = () => {
    setOpenAddNewServiceModal(true);
  };

  const handleCloseAdd_duration_price = () => {
    setOpenAddNewDurationPriceModal(false);
  };

  const handleClose_AddNewServiceModal = () => {
    setOpenAddNewServiceModal(false);
  };

  const handleOpenAdd_duration_price = (incoming_service) => {
    //setOpenAddNewServiceModal(false);
    console.log("incoming service", incoming_service);
    setServiceHolder(incoming_service);
    setOpenAddNewDurationPriceModal(true);
  };

  //handleOpenAdd_duration_price

  //handleOpenAddNewServiceModal

  function handleChange(e) {
    // Access the file object
    const file = e.target.files[0];
    console.log("upload profile picture");

    handleUploadProfile(file);

    // Create a new FileReader
    const reader = new FileReader();

    // Set up the onload event to handle the file read operation
    reader.onload = () => {
      // Get the Base64 encoded data
      const base64Data = reader.result;

      // Assign the Base64 data to the src attribute of the image element
      if (profilePicRef.current) {
        profilePicRef.current.src = base64Data;
      }

      // setPP(base64Data);

      // Optionally, you can handle the Base64 data (e.g., store it in state)
      // console.log("Base64 encoded image:", base64Data);
    };

    // Read the contents of the file as a Data URL
    reader.readAsDataURL(file);
    setFile(URL.createObjectURL(e.target.files[0]));
  }

  // const handleUploadProfile = async (file) => {
  //   try {
  //     const formData = new FormData();
  //     formData.append("profile", file);

  //     // Replace 'your-upload-endpoint' with your actual upload endpoint
  //     const response = await axios.post(endpoints.uploadProfilePic, formData, {
  //       headers: {
  //         "Content-Type": "multipart/form-data",
  //         Authorization: `Bearer ${stateToken}`,
  //       },
  //     });

  //     console.log("Upload successful:", response.data);
  //     setPP(response.data);
  //     console.log("ID", selectedRowData._id);
  //     handleUpdateProfilePicURL(response.data);
  //   } catch (error) {
  //     console.error("Error uploading file:", error);
  //   }
  // };

  const handleUploadProfile = async (file) => {
    try {
      setLoading(true);
      const formData = new FormData();
      formData.append("profile", file);

      // Replace 'your-upload-endpoint' with your actual upload endpoint
      const response = await axios.post(endpoints.uploadProfilePic, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${stateToken}`,
        },
      });

      console.log("Upload successful:", response.data);
      setPP(response.data.image_url);
      console.log("ID", selectedRowData._id);
      handleUpdateProfilePicURL(response.data.image_url);
    } catch (error) {
      console.error("Error uploading file:", error);
    } finally {
      console.log("Request completed."); // This will be executed regardless of success or failure
      setLoading(false);
    }
  };

  //handleUpdateLogoPic

  const handleUploadLogo = async (file) => {
    try {
      const formData = new FormData();
      formData.append("logo", file);

      // Replace 'your-upload-endpoint' with your actual upload endpoint
      const response = await axios.post(endpoints.uploadLogoPic, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${stateToken}`,
        },
      });

      console.log("Upload successful:", response.data);
      setBP(response.data.image_url);
      console.log("ID", selectedRowData._id);
      handleUpdateLogoPicURL(response.data);
    } catch (error) {
      console.error("Error uploading file:", error);
    }
  };

  const handleUpdateProfilePicURL = async (generatedURL) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("generatedURL", generatedURL);

    const profileURL = {
      profile_pic: generatedURL,
    };

    console.log("profileURLOBject", profileURL);

    try {
      setLoading(true); // Set loading to true when making the request

      const response = await fetch(
        `${endpoints.updateProfilePicURL}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(profileURL),
        }
      );
      if (response.ok) {
        // console.log("Records updated successfully");
        //setOpenImageSuccess(true);
        setSuccessMsg("image added successfully!");
        setGeneralSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
        setErrorMsg("Failed to Add Image");
        setGeneralError(true);
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  const handleUpdateLogoPicURL = async (generatedURL) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("generatedURL", generatedURL);

    const logoURL = {
      logo: generatedURL.image_url,
    };

    console.log("profileURLOBject", logoURL);

    try {
      setLoading(true); // Set loading to true when making the request

      const response = await fetch(
        `${endpoints.updateLogoPicURL}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(logoURL),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        setOpenImageSuccess(true);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }

      response.json().then((data) => {
        console.log("json", data);

        // if (data.status == "0") {
        //   console.log("Error detected");
        //   setErrorMsg("Something went wrong");
        //   setGeneralError(true);
        //   return;
        // } else if (data.status == "1") {
        //   setOpenSuccess(true);
        //   setSuccessMsg("Password Reset Successful!");
        //   setGeneralSuccess(true);
        //   setTimeout(() => {
        //     setOpenAddCompany(false);
        //     reloadPage();
        //   }, 2000);
        // }
      });
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  //handleUploadLogo

  function handleChange2(e) {
    // Access the file object
    const bgfile = e.target.files[0];

    handleUploadLogo(bgfile);

    // Create a new FileReader
    const reader = new FileReader();

    // Set up the onload event to handle the file read operation
    reader.onload = () => {
      // Get the Base64 encoded data
      const base64Data = reader.result;

      // Assign the Base64 data to the src attribute of the image element
      if (profilePicRef.current) {
        profilePicRef.current.src = base64Data;
      }

      // setPP(base64Data);
      // setBP(base64Data);

      // Optionally, you can handle the Base64 data (e.g., store it in state)
      console.log("Base64 encoded image:", base64Data);
    };

    // Read the contents of the file as a Data URL
    reader.readAsDataURL(bgfile);
    //setBgFile(URL.createObjectURL(e.target.files[0]));
  }

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;
    setActiveStep(newActiveStep);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    setCompleted(newCompleted);
    handleNext();
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted({});
  };

  const items = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={actionId}
      keepMounted
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      open={isOPen}
      onClose={handleActionMenuClose}
    >
      <MenuItem className="flex gap-2" onClick={handleOpen_EditProvider}>
        <MdEdit /> Edit
      </MenuItem>
      <MenuItem className="flex gap-2">
        <MdAirplanemodeActive /> Activate Partner
      </MenuItem>
      <MenuItem className="flex gap-2">
        <MdOutlineAirplanemodeInactive /> Deactivate
      </MenuItem>
      <MenuItem className="flex gap-2" onClick={handleOpen_DeleteProvider}>
        <RiDeleteBinFill /> Delete
      </MenuItem>
    </Menu>
  );
  function handleActionMenuClose() {
    setIsOpen(false);
  }
  function handleActionMenuOpen() {
    setIsOpen(true);
  }

  function filterUsers(searchVal) {
    const data = JSON.parse(localStorage.getItem("providerData")) || [];

    console.log("To be filtered users:->", data);

    if (searchVal === undefined || searchVal.trim().length === 0) {
      setRows(data);
    } else {
      const searchTerm = searchVal.toLowerCase().split(" ");

      const results = data.filter((user) => {
        const firstName = user.first_name ? user.first_name.toLowerCase() : "";
        const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        const email = user.email ? user.email.toLowerCase() : "";
        const phoneNumber = user.phone_number ? user.phone_number : "";
        const companyName = user.main_businessName
          ? user.main_businessName.toLowerCase()
          : "";

        return searchTerm.every(
          (term) =>
            firstName.includes(term) ||
            lastName.includes(term) ||
            email.includes(term) ||
            phoneNumber.includes(term) ||
            companyName.includes(term)
        );
      });

      setRows(results);
    }
  }

  function handleGetAllRecords(searchVal) {
    setLoading(true);
    console.log("*************handle get all records called***************");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);
    setToken(json_token.token); // Assuming setToken is a function to set token somewhere in your code

    axios
      .get(endpoints.getPartners, {
        headers: {
          Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        console.log("Response data all records: ", response.data);
        const json = response.data;
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        localStorage.setItem("providerData", JSON.stringify(json));
        console.log("provider data set globally...");
        globalData = json;
        setRows(json);

        // if (searchVal === undefined || searchVal.trim().length === 0) {
        //   setRows(json);
        // } else {
        //   const results = json.filter((user) => {
        //     const firstName = user.first_name
        //       ? user.first_name.toLowerCase()
        //       : "";
        //     const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        //     const email = user.email ? user.email.toLowerCase() : "";
        //     const phoneNumber = user.phone_number ? user.phone_number : "";
        //     const CompanyName = user.main_businessName
        //       ? user.main_businessName.toLowerCase()
        //       : "";

        //     const searchTerm = searchVal.toLowerCase();

        //     return (
        //       firstName.includes(searchTerm) ||
        //       lastName.includes(searchTerm) ||
        //       email.includes(searchTerm) ||
        //       phoneNumber.includes(searchTerm) ||
        //       CompanyName.includes(searchTerm)
        //     );
        //   });

        //   setRows(results);
        // }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }
  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);

    //console.log("all users", rows);

    filterUsers(event.target.value);
    //handleGetAllRecords(event.target.value);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // const handleExpansion = () => {
  //   setExpanded((prevExpanded) => !prevExpanded);
  // };

  const handleExpansion = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : null);
  };

  const postPartnerRegistrationData = async (data) => {
    const default_availability = {
      monday_opening_time: "10:00AM",
      monday_closing_time: "6:00PM",
      tuesday_opening_time: "10:00AM",
      tuesday_closing_time: "6:00PM",
      wednesday_opening_time: "10:00AM",
      wednesday_closing_time: "6:00PM",
      thursday_opening_time: "10:00AM",
      thursday_closing_time: "6:00PM",
      friday_opening_time: "10:00AM",
      friday_closing_time: "6:00PM",
      saturday_opening_time: "10:00AM",
      saturday_closing_time: "6:00PM",
      sunday_opening_time: "closed",
      sunday_closing_time: "closed",
    };

    const partnerRegistrationObject = {
      email: data.email.trim(),
      password: data.password.trim(),
      first_name: data.first_name,
      last_name: data.last_name,
      phone_number: data.phone_number,
      main_businessName: data.main_businessName,
      bio: data.bio,
    };

    console.log("main object", partnerRegistrationObject);

    try {
      setLoading(true);
      const response = await axios.post(
        endpoints.registerPartner,
        partnerRegistrationObject
      );

      console.log("Response from the backend:", response.data);

      if (response.data.status === "Error") {
        console.error("Error detected");

        if (
          response.data.message ===
          "Missing required fields: email, password, first_name, or last_name"
        ) {
          setErrorMsg("One or more fields is required");
          setGeneralError(true);

          return;
        }
      }

      if (response.data.message == "Email-already-taken") {
        console.log("FAILED");
        //setOpenAddAdminFailed_emailTaken(true);

        console.log("Error detected");
        setErrorMsg("Email-already-taken");
        setGeneralError(true);
        return;
      }
      setOpenAddAdminSuccess(true);

      // Reload the page only if there's no error
      // setTimeout(() => {
      //   reloadPage();
      // }, 2000);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
      console.log("Error detected");
      setErrorMsg("Error sending data to the backend");
      setGeneralError(true);
      return;

      setOpenAddCompany(true); // Open the pop-up modal on error
    } finally {
      setLoading(false);
    }
  };

  const postNewService = async (current_serviceholder, formData) => {
    const newServiceObject = {
      subcategory: current_serviceholder.subcategory,
      localization: current_serviceholder.localization,
      price: formData.price,
      status: "active",
      service_duration: formData.duration,
      type: current_serviceholder.type,
      created_at: "2024-03-23T09:18:12Z",
    };

    console.log("main object", newServiceObject);

    try {
      setLoading(true); // Set loading to true when making the request

      const response = await fetch(
        `${endpoints.addPartnerService}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${stateToken}`,
          },
          body: JSON.stringify(newServiceObject),
        }
      );
      if (response.ok) {
        console.log(response);
        // setOpenImageSuccess(true);
        setOpenAddNewDurationPriceModal(false);
        setOpenDurationTimeSuccess(true);

        //  Reload the page only if there's no error
        setTimeout(() => {
          reloadPage();
        }, 2000);
        //  setRefresher(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }

    // try {
    //   setLoading(true);
    //   const response = await axios.post(
    //     // endpoints.addPartnerService,
    //     `${endpoints.addPartnerService}/${selectedRowData._id}`,
    //     newServiceObject
    //   );

    //   console.log("Response from the backend:", response.data);
    //   setOpenAddAdminSuccess(true);

    //   // Reload the page only if there's no error
    //   // setTimeout(() => {
    //   //   reloadPage();
    //   // }, 2000);
    // } catch (error) {
    //   console.error("Error sending data to the backend:", error);
    //   setOpenAddAdminFailed(true);
    //   setOpenAddCompany(true); // Open the pop-up modal on error
    // } finally {
    //   setLoading(false);
    // }
  };

  //updatePartnerRegistrationData

  //   const updatePartnerRegistrationData = async (data) => {
  //     var storedToken = localStorage.getItem("stored_token");
  //     var json_token = JSON.parse(storedToken);
  //     console.log("Updating id", selectedRowData._id);

  //     const partnerRegistrationObject = {
  //       email: data.email.trim(),
  //       //password: data.password.trim(),
  //       first_name: data.first_name,
  //       last_name: data.last_name,
  //       phone_number: data.phone_number,
  //       main_businessName: data.main_businessName,
  //       bio: data.bio,
  //       profile_pic: pp,
  //       logo: bp,
  //     };

  //     console.log("main object", partnerRegistrationObject);

  //     try {
  //       setLoading(true); // Set loading to true when making the request
  //       //setOpenAddAdminSuccess(true);

  //       const response = await fetch(
  //         `${endpoints.editProviderRegisrationInfo}/${selectedRowData._id}`,

  //         {
  //           method: "PUT",
  //           headers: {
  //             "Content-Type": "application/json",
  //             Authorization: `Bearer ${json_token.token}`,
  //           },
  //           body: JSON.stringify(partnerRegistrationObject),
  //         }
  //       );
  //  console.log("data returned ====>" , response)
  //     } catch (error) {
  //       console.error("Error sending data to the backend:", error);
  //     } finally {
  //       setLoading(false); // Set loading to false after the request is complete (success or error)

  //     }
  //   };
  const updatePartnerRegistrationData = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    const partnerRegistrationObject = {
      email: data.email.trim(),
      //password: data.password.trim(),
      first_name: data.first_name,
      last_name: data.last_name,
      phone_number: data.phone_number,
      main_businessName: data.main_businessName,
      bio: data.bio,
      profile_pic: pp.image_url,
      logo: bp,
    };

    console.log("main object", partnerRegistrationObject);

    try {
      setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.editProviderRegisrationInfo}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(partnerRegistrationObject),
        }
      );
      // if (response.ok) {
      //   // console.log("Records updated successfully");
      //   // setOpenProviderUpdateSuccess(true);

      //   console.log("response", response.json());

      //   if (response.status == "error") {
      //     console.log("error detected");

      //     if (response.status == "") {
      //     }
      //     console.error("Error sending data to the backend:", error);
      //     console.log("Error detected");
      //     setErrorMsg("Error sending data to the backend");
      //     setGeneralError(true);
      //     return;
      //   } else if (response.status == "Success") {
      //     setSuccessMsg("Provider Updated Successfully!");
      //     setGeneralSuccess(true);
      //   }
      // } else {
      //   console.error("Failed to update record information");
      //   setOpenProviderUpdateFailed(true);
      // }

      response.json().then((data) => {
        console.log("json", data);

        if (data.status == "Error") {
          console.log("Error detected");
          //setErrorMsg("Something went wrong");
          // setGeneralError(true);

          if (data.message == "User Not Found") {
            setErrorMsg("No Change made to be updated");
            setGeneralError(true);
            return;
          } else {
            setErrorMsg("Something went wrong");
            setGeneralError(true);
            return;
          }
        } else if (data.status == "Success") {
          setOpenSuccess(true);
          setSuccessMsg("Provider Updated Successfully!");
          setGeneralSuccess(true);
          // setTimeout(() => {
          //   setOpenAddCompany(false);
          //   reloadPage();
          // }, 2000);
        }
      });
    } catch (error) {
      setOpenProviderUpdateFailed(true);

      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false);

      // if (response.status === 200) {
      //   console.log("Records updated successfully");

      // } else {
      //   setOpenAddAdminFailed(true);
      //   setOpenAddCompany(true);
      //   console.error("Failed to update record information");
      // }

      // Set loading to false after the request is complete (success or error)
    }
    // setTimeout(() => {
    //   setOpenAddCompany(false);
    //   reloadPage();
    // }, 2000);
  };

  //updatePartnerAvailabilityData
  const updatePartnerAvailabilityData = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);

    const availability = {
      monday: {
        opening_time: data.monday_opening_time,
        closing_time: data.monday_closing_time,
      },
      tuesday: {
        opening_time: data.tuesday_opening_time,
        closing_time: data.tuesday_closing_time,
      },
      wednesday: {
        opening_time: data.wednesday_opening_time,
        closing_time: data.wednesday_closing_time,
      },
      thursday: {
        opening_time: data.thursday_opening_time,
        closing_time: data.thursday_closing_time,
      },
      friday: {
        opening_time: data.friday_opening_time,
        closing_time: data.friday_closing_time,
      },
      saturday: {
        opening_time: data.saturday_opening_time,
        closing_time: data.saturday_closing_time,
      },
      sunday: {
        opening_time: data.sunday_opening_time,
        closing_time: data.sunday_closing_time,
      },
    };

    console.log("main object", availability);

    try {
      setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.addProviderAvailability}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(availability),
        }
      );
      console.log("response after edit:->>", response);

      response.json().then((data) => {
        console.log("json", data);

        if (data.status == "0") {
          console.log("Error detected");
          setErrorMsg("Something went wrong");
          setGeneralError(true);
          return;
        } else if (data.status == "Success") {
          //setOpenSuccess(true);
          setSuccessMsg("Partner Availability Updated Succcessfully!");
          setGeneralSuccess(true);
          // setTimeout(() => {
          //   setOpenAddCompany(false);
          //   reloadPage();
          // }, 2000);
        }
      });
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to falsehadnl after the request is complete (success or error)
    }
  };

  React.useEffect(() => {
    //handleGetSortedServices()
    console.error("USE EFFECT CALLED!!!");
    handleGetAllRecords();
    // ;

    //handleGetPartner("660e5e18bc8e1f4458c3b9ac");
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  const formikNewAdmin = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      email: "",
      admin_type: "",
      country: "",
      timezone: "",
      password: "",
      main_businessName: "",
      bio: "",
      logo: "",
      service_type: "",
      payment_option: "",
      social_media: "",
      location: {
        latitude: "",
        longitude: "",
      },

      partner_availability: {
        monday: {
          opening_time: "",
          closing_time: "",
        },
        tuesday: {
          opening_time: "",
          closing_time: "",
        },
        wednesday: {
          opening_time: "",
          closing_time: "",
        },
        thursday: {
          opening_time: "",
          closing_time: "",
        },
        friday: {
          opening_time: "",
          closing_time: "",
        },
        saturday: {
          opening_time: "",
          closing_time: "",
        },
        sunday: {
          opening_time: "",
          closing_time: "",
        },
      },
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      // postAdminRegistrationData(
      //   values.first_name.trim(),
      //   values.last_name.trim(),
      //   values.email.trim(),
      //   values.admin_type.trim(),
      //   values.password.trim()
      // );
    },
  });

  const formikPartnerRegistration = useFormik({
    initialValues: {
      email: "",
      password: "",
      first_name: "",
      last_name: "",
      phone_number: "",
      main_businessName: "",
      bio: "",
      profilePicture: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);
      console.log("profile image", profilePicRef.current);

      postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  //formikAddDurationPrice

  const formikAddDurationPrice = useFormik({
    initialValues: {
      duration: "",
      price: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("SERVICE HOLDER", serviceHolder);
      console.log("form data:-", values);

      postNewService(serviceHolder, values);

      //postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  //formikEditPartnerRegistration

  const formikEditPartnerRegistration = useFormik({
    initialValues: {
      email: "",
      password: "",
      first_name: "",
      last_name: "",
      phone_number: "",
      main_businessName: "",
      bio: "",
      profilePicture: "",
      logo: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);
      console.log("profile image", profilePicRef.current);

      //postPartnerRegistrationData(values);
      updatePartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  const formikPartnerAvailability = useFormik({
    initialValues: {
      monday_opening_time: "",
      monday_closing_time: "",

      tuesday_opening_time: "",
      tuesday_closing_time: "",

      wednesday_opening_time: "",
      wednesday_closing_time: "",

      thursday_opening_time: "",
      thursday_closing_time: "",

      friday_opening_time: "",
      friday_closing_time: "",

      saturday_opening_time: "",
      saturday_closing_time: "",

      sunday_opening_time: "",
      sunday_closing_time: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data partner availability:-", values);

      updatePartnerAvailabilityData(values);

      //postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  //formikPartnerBookingPreference

  const formikPartnerBookingPreference = useFormik({
    initialValues: {},
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data partner availability:-", values);

      updatePartnerAvailabilityData(values);

      //postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  return (
    <>
      {/* start of Add Modal */}

      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />

      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />

      <Modal
        open={openAddCompany}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <>
            <div className="flex flex-row justify-between m-2 py-2">
              <Typography sx={{ color: "#000", fontSize: [18] }}>
                Add Provider
              </Typography>
              <IoMdCloseCircle
                size={18}
                onClick={handleClose_AddCompany}
                cursor={"pointer"}
                color="#000"
              />
            </div>
            <form onSubmit={formikPartnerRegistration.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    variant="outlined"
                    type="text"
                    id="first_name"
                    name="first_name"
                    label="Fist Name"
                    placeholder="First Name"
                    value={formikPartnerRegistration.values.first_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />

                  <TextField
                    type="text"
                    id="last_name"
                    name="last_name"
                    label="Last Name"
                    placeholder="Last Name"
                    value={formikPartnerRegistration.values.last_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    type="text"
                    id="email"
                    name="email"
                    label="Email"
                    placeholder=" Email"
                    value={formikPartnerRegistration.values.email}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                  <TextField
                    id="phone_number"
                    name="phone_number"
                    label="Phone Number"
                    placeholder="Phone Number"
                    value={formikPartnerRegistration.values.phone_number}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>

              <div className="flex flex-row gap-3 ">
                <TextField
                  type="text"
                  id="main_businessName"
                  name="main_businessName"
                  label="Business Name"
                  placeholder="Business Name"
                  value={formikPartnerRegistration.values.main_businessName}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
                <TextField
                  id="password"
                  name="password"
                  label="Password"
                  placeholder="Password"
                  value={formikPartnerRegistration.values.password}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Bio description </FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="bio"
                  id="bio"
                  name="bio"
                  label="Bio"
                  placeholder="Bio"
                  value={formikPartnerRegistration.values.bio}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-3 mt-3">
                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>
          </>
        </Box>
      </Modal>
      {/* End fo Add Modal */}
      <Modal
        open={openEdit}
        onClose={handleClose_EditProvider}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box4} className="select-none ">
          <div className="flex flex-row justify-between m-2 py-2">
            <Typography
              sx={{ color: "#000", fontSize: [18], fontWeight: "bold" }}
            >
              Edit Provider
            </Typography>
            <IoMdCloseCircle
              size={18}
              onClick={handleClose_EditProvider}
              cursor={"pointer"}
              color="#000"
            />
          </div>
          <Stepper nonLinear activeStep={activeStep} className="mt-8">
            {steps.map((label, index) => (
              <Step key={label} completed={completed[index]}>
                <StepButton color="inherit" onClick={handleStep(index)}>
                  {label}
                </StepButton>
              </Step>
            ))}
          </Stepper>
          <div>
            {allStepsCompleted() ? (
              <React.Fragment>
                <Typography sx={{ mt: 2, mb: 1 }}>
                  All steps completed - you&apos;re finished
                </Typography>
                <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                  <Box sx={{ flex: "1 1 auto" }} />
                  <Button onClick={handleReset}>Reset</Button>
                </Box>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Typography sx={{ mt: 2, mb: 1, py: 1, color: "#000" }}>
                  {/* Step {activeStep + 1} */}
                  {activeStep === 0 ? (
                    // <p>First form</p>
                    <>
                      <form
                        onSubmit={formikEditPartnerRegistration.handleSubmit}
                      >
                        <div>
                          <Modal open={openfile}>
                            <Box sx={style_box3} className="z-50">
                              <div className="flex flex-row justify-between m-2 ">
                                <Typography
                                  sx={{ color: "#000", fontSize: [18] }}
                                >
                                  Upload Image:
                                </Typography>
                                <IoMdCloseCircle
                                  size={18}
                                  onClick={handleClose_file}
                                  cursor={"pointer"}
                                  color="#000"
                                />
                              </div>
                              <div className="flex flex-col gap-12">
                                <div className="flex flex-row gap-3 border-b bod border-gray-400 py-3">
                                  <label className="text-black">Profile</label>
                                  <input
                                    type="file"
                                    onChange={handleChange}
                                    className="mt-[20%] cursor-pointer"
                                  />
                                  <img
                                    src={!pp ? hsp : pp}
                                    className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
                                  />
                                </div>
                                <div className="flex flex-row gap-3">
                                  <label className="text-black">Logo</label>
                                  <input
                                    type="file"
                                    onChange={handleChange2}
                                    className="mt-[20%] cursor-pointer"
                                  />
                                  <img
                                    src={!bp ? hsp1 : bp}
                                    className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
                                  />
                                </div>
                              </div>
                            </Box>
                          </Modal>

                          <div className="h-44 relative mb-24">
                            <div
                              className="bg-cover bg-center rounded-lg relative h-44 mb-24"
                              style={{
                                // backgroundImage: `url(${
                                //   !bgfile ? hsp1 : bgfile
                                // })`,
                                backgroundImage: `url(${
                                  // !bgfile ? hsp1 : bgfile
                                  !bgfile ? (!bp ? hsp1 : bp) : bgfile
                                })`,
                              }}
                            >
                              <div className="absolute  w-[155px] h-[155px] bg-gray-600 rounded-full flex items-center justify-center top-[50%]">
                                <img
                                  defaultValue={hsp}
                                  src={!file ? (!pp ? hsp : pp) : file}
                                  alt="pic"
                                  className="rounded-full w-[140px] h-[140px]"
                                />
                              </div>

                              <div className="flex absolute bottom-[-20%] sm:left-44 left-48 text-black">
                                <Typography sx={{ fontSize: [12, 19] }}>
                                  {
                                    formikEditPartnerRegistration.values
                                      .first_name
                                  }
                                  &nbsp;
                                  {
                                    formikEditPartnerRegistration.values
                                      .last_name
                                  }
                                  &nbsp;
                                  <span className="text-xs text-gray-400">
                                    {formikEditPartnerRegistration.values.email}
                                  </span>
                                </Typography>
                              </div>
                            </div>
                            <div className="absolute  ml-[128px] mt-[-90px] bg-slate-400 rounded-full w-9 h-9 flex justify-center items-center">
                              <div className=" hover:scale-110 ease-linear duration-200 cursor-pointer bg-slate-200 rounded-full w-8 h-8 flex justify-center items-center">
                                <FaCamera
                                  color="#111111"
                                  size={23}
                                  onClick={handleOpen_file}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="flex flex-col gap-3 mt-3">
                          <div className="flex flex-row gap-3  mb-4">
                            <TextField
                              type="text"
                              id="first_name"
                              name="first_name"
                              label="Fist Name"
                              placeholder="First Name"
                              sx={{ width: [160, 400] }}
                              value={
                                formikEditPartnerRegistration.values.first_name
                              }
                              onChange={
                                formikEditPartnerRegistration.handleChange
                              }
                              onBlur={formikEditPartnerRegistration.handleBlur}
                            />

                            <TextField
                              type="text"
                              id="last_name"
                              name="last_name"
                              label="Last Name"
                              placeholder="Last Name"
                              sx={{ width: [160, 400] }}
                              value={
                                formikEditPartnerRegistration.values.last_name
                              }
                              onChange={
                                formikEditPartnerRegistration.handleChange
                              }
                              onBlur={formikEditPartnerRegistration.handleBlur}
                            />
                          </div>
                        </div>
                        <div className="flex flex-col gap-3 ">
                          <div className="flex flex-row gap-3  mb-4">
                            <TextField
                              type="text"
                              id="email"
                              name="email"
                              label="Email"
                              sx={{ width: [160, 400] }}
                              placeholder=" Email"
                              value={formikEditPartnerRegistration.values.email}
                              onChange={
                                formikEditPartnerRegistration.handleChange
                              }
                              onBlur={formikEditPartnerRegistration.handleBlur}
                            />
                            <TextField
                              id="phone_number"
                              name="phone_number"
                              label="Phone Number"
                              placeholder="Phone Number"
                              sx={{ width: [160, 400] }}
                              value={
                                formikEditPartnerRegistration.values
                                  .phone_number
                              }
                              onChange={
                                formikEditPartnerRegistration.handleChange
                              }
                              onBlur={formikEditPartnerRegistration.handleBlur}
                            />
                          </div>
                        </div>

                        <div className="flex flex-row gap-3 ">
                          <TextField
                            type="text"
                            id="main_businessName"
                            name="main_businessName"
                            label="Business Name"
                            placeholder="Business Name"
                            fullWidth
                            value={
                              formikEditPartnerRegistration.values
                                .main_businessName
                            }
                            onChange={
                              formikEditPartnerRegistration.handleChange
                            }
                            onBlur={formikEditPartnerRegistration.handleBlur}
                          />
                          {/* <TextField
                            id="password"
                            name="password"
                            label="Password"
                            placeholder="Password"
                            value={
                              formikEditPartnerRegistration.values.password
                            }
                            onChange={
                              formikEditPartnerRegistration.handleChange
                            }
                            onBlur={formikEditPartnerRegistration.handleBlur}
                          /> */}
                        </div>
                        <div className="flex flex-col gap-1  mt-3">
                          <FormLabel>Bio description </FormLabel>

                          <Textarea
                            maxRows={4}
                            aria-label="maximum height"
                            type="bio"
                            id="bio"
                            name="bio"
                            label="Bio"
                            placeholder="Bio description"
                            value={formikEditPartnerRegistration.values.bio}
                            onChange={
                              formikEditPartnerRegistration.handleChange
                            }
                            onBlur={formikEditPartnerRegistration.handleBlur}
                          />
                        </div>
                        <div className="flex flex-col gap-3 mt-3">
                          <div>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              type="submit"
                            >
                              Save
                            </Button>
                          </div>
                        </div>
                      </form>
                    </>
                  ) : activeStep === 1 ? (
                    <>
                      <form
                        onSubmit={formikPartnerAvailability.handleSubmit}
                        // className="w-full flex flex-col"
                      >
                        <div className="flex flex-col gap-3 mt-3 ">
                          <div className="flex flex-row items-center gap-2 justify-between">
                            <div>
                              <FormControl>
                                <FormLabel>Monday Opening </FormLabel>

                                <TextField
                                  id="monday_opening_time"
                                  select
                                  name="monday_opening_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .monday_opening_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>

                            <div>
                              <FormControl>
                                <FormLabel>Monday Closing Time </FormLabel>

                                <TextField
                                  id="monday_closing_time"
                                  select
                                  name="monday_closing_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .monday_closing_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                          </div>
                          <div className="flex flex-row gap-2  justify-between">
                            <div>
                              <FormControl>
                                <FormLabel>Tuesday Opening </FormLabel>

                                <TextField
                                  id="tuesday_opening_time"
                                  select
                                  name="tuesday_opening_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .tuesday_opening_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>

                            <div>
                              <FormControl>
                                <FormLabel>Tuesday Closing Time </FormLabel>

                                <TextField
                                  id="tuesday_closing_time"
                                  select
                                  name="tuesday_closing_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .tuesday_closing_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                          </div>
                          <div className="flex flex-row gap-2 justify-between">
                            <div>
                              {" "}
                              <FormControl>
                                <FormLabel>Wednesady Opening Time </FormLabel>

                                <TextField
                                  id="wednesday_opening_time"
                                  select
                                  name="wednesday_opening_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .wednesday_opening_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>

                            <div>
                              <FormControl>
                                <FormLabel>Wednesday Closing Time </FormLabel>

                                <TextField
                                  id="wednesday_closing_time"
                                  select
                                  name="wednesday_closing_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .wednesday_closing_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                          </div>
                          <div className="flex flex-row gap-2 justify-between">
                            <div>
                              <FormControl>
                                <FormLabel>Thursaday Opening Time </FormLabel>

                                <TextField
                                  id="thursday_opening_time"
                                  select
                                  name="thursday_opening_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .thursday_opening_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                            <div>
                              <FormControl>
                                <FormLabel>Thursday Closing Time </FormLabel>

                                <TextField
                                  id="thursday_closing_time"
                                  select
                                  name="thursday_closing_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .thursday_closing_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                          </div>
                          <div className="flex flex-row gap-2 justify-between">
                            <div>
                              <FormControl>
                                <FormLabel>Friday Opening Time </FormLabel>
                                <TextField
                                  id="friday_opening_time"
                                  select
                                  name="friday_opening_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .friday_opening_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>

                            <div>
                              <FormControl>
                                <FormLabel>Friday Closing Time </FormLabel>

                                <TextField
                                  id="friday_closing_time"
                                  select
                                  name="friday_closing_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .friday_closing_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                          </div>
                          <div className="flex flex-row gap-2 justify-between">
                            <div>
                              <FormControl>
                                <FormLabel>Saturday Opening Time </FormLabel>

                                <TextField
                                  id="saturday_opening_time"
                                  select
                                  name="saturday_opening_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .saturday_opening_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                            <div>
                              <FormControl>
                                <FormLabel>Saturday Closing Time </FormLabel>

                                <TextField
                                  id="saturday_closing_time"
                                  select
                                  name="saturday_closing_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .saturday_closing_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                          </div>
                          <div className="flex flex-row gap-2 justify-between">
                            <div>
                              <FormControl>
                                <FormLabel>Sunday Opening Time </FormLabel>

                                <TextField
                                  id="sunday_opening_time"
                                  select
                                  name="sunday_opening_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .sunday_opening_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>

                            <div>
                              <FormControl>
                                <FormLabel>Sunday Closing Time </FormLabel>

                                <TextField
                                  id="sunday_closing_time"
                                  select
                                  name="sunday_closing_time"
                                  sx={{ width: [160, 400] }}
                                  value={
                                    formikPartnerAvailability.values
                                      .sunday_closing_time
                                  }
                                  onChange={
                                    formikPartnerAvailability.handleChange
                                  }
                                  onBlur={formikPartnerAvailability.handleBlur}
                                  helperText="Select position"
                                >
                                  {time_array.map((option) => (
                                    <MenuItem
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </MenuItem>
                                  ))}
                                </TextField>
                              </FormControl>
                            </div>
                          </div>
                        </div>

                        <div className="flex flex-col gap-3 mt-3">
                          <div>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              type="submit"
                            >
                              Save
                            </Button>
                          </div>
                        </div>
                      </form>
                    </>
                  ) : activeStep === 2 ? (
                    <>
                      <form
                      // onSubmit={formikPartnerBookingPreference.handleSubmit}
                      >
                        <div className="flex flex-col gap-3 mt-3">
                          <div className="flex flex-row gap-3  mb-4">
                            <FormControl>
                              <FormLabel id="demo-controlled-radio-buttons-group">
                                Where do you Provide your services?
                              </FormLabel>
                              <RadioGroup
                                aria-labelledby="demo-controlled-radio-buttons-group"
                                name="controlled-radio-buttons-group"
                                value={serviceType}
                                onChange={handleChangeServiceLocation}
                              >
                                <FormControlLabel
                                  value="at_venue"
                                  control={<Radio />}
                                  label="At Venue"
                                />
                                <FormControlLabel
                                  value="mobile_service"
                                  control={<Radio />}
                                  label="Mobile Service"
                                />
                                <FormControlLabel
                                  value="both"
                                  control={<Radio />}
                                  label="Both"
                                />
                              </RadioGroup>
                            </FormControl>
                          </div>
                        </div>
                        <div className="flex flex-col gap-3 ">
                          <div className="flex flex-row gap-3  mb-4">
                            <FormControl>
                              <FormLabel id="demo-controlled-radio-buttons-group">
                                How would you like your clients to book you?
                              </FormLabel>
                              <RadioGroup
                                aria-labelledby="demo-controlled-radio-buttons-group"
                                name="controlled-radio-buttons-group"
                                value={paymentmethod}
                                onChange={handleChangeRadio_paymentMethod}
                              >
                                <FormControlLabel
                                  value="card_cash"
                                  control={<Radio />}
                                  label="Card & Cash"
                                />
                                <FormControlLabel
                                  value="card_only"
                                  control={<Radio />}
                                  label="Card Only"
                                />
                              </RadioGroup>
                            </FormControl>
                          </div>
                        </div>

                        <div className="flex flex-col gap-3 mt-3">
                          <div>
                            <Button
                              variant="contained"
                              sx={{ backgroundColor: "#242333" }}
                              size="small"
                              onClick={submitBookingPreference}
                            >
                              Save
                            </Button>
                          </div>
                        </div>
                      </form>
                    </>
                  ) : activeStep === 3 ? (
                    <>
                      <div className="mt-4">
                        <h2 className="text-xl sm:text-2xl text-start mr-4 font-bold text-gray-600">
                          Add Services
                        </h2>
                        <div className="flex sm:flex-row flex-col  ">
                          <div className="mt-6">
                            {/* 
                            {partnerServices.map((element, index) => (
                              <div key={index}>
                                {element.type} {element.price}
                              </div>
                            ))} */}
                            {/* <div>
                              {partnerServices.map((hairType, index) => (
                                <Accordion
                                  key={index}
                                  expanded={expanded === `panel${index}`}
                                  onChange={handleExpansion(`panel${index}`)}
                                  TransitionComponent={Fade}
                                  TransitionProps={{ timeout: 400 }}
                                  sx={{
                                    "& .MuiAccordion-region": {
                                      height:
                                        expanded === `panel${index}`
                                          ? "auto"
                                          : 0,
                                    },
                                    "& .MuiAccordionDetails-root": {
                                      display:
                                        expanded === `panel${index}`
                                          ? "block"
                                          : "none",
                                    },
                                  }}
                                >
                                  <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls={`panel${index}-content`}
                                    id={`panel${index}-header`}
                                  >
                                    <Typography>
                                      {hairType.subcategory}
                                    </Typography>
                                  </AccordionSummary>
                                  <AccordionDetails>
                                    <Typography>{hairType.type}</Typography>
                                  </AccordionDetails>
                                </Accordion>
                              ))}
                            </div> */}
                            <div className="sm:absolute right-[80px] top-[200px]">
                              <Button
                                variant="contained"
                                onClick={handleOpenAddNewServiceModal}
                              >
                                Add New Service
                              </Button>
                            </div>
                            {/* {partnerServices.map((element, index) => (
                              <div key={index}>
                                {element.type} {element.price}
                              </div>
                            ))}{" "} */}

                            <div>
                              {/* {partnerServices.map((hairType, index) => (
                                <Accordion
                                  key={index}
                                  expanded={expanded === `panel${index}`}
                                  onChange={handleExpansion(`panel${index}`)}
                                  TransitionComponent={Fade}
                                  TransitionProps={{ timeout: 400 }}
                                  sx={{
                                    "& .MuiAccordion-region": {
                                      height:
                                        expanded === `panel${index}`
                                          ? "auto"
                                          : 0,
                                    },
                                    "& .MuiAccordionDetails-root": {
                                      display:
                                        expanded === `panel${index}`
                                          ? "block"
                                          : "none",
                                    },
                                  }}
                                >
                                  <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls={`panel${index}-content`}
                                    id={`panel${index}-header`}
                                  >
                                    <Typography>
                                      {hairType.subcategory}
                                    </Typography>
                                  </AccordionSummary>
                                  <AccordionDetails>
                                    <Typography>{hairType.type}</Typography>
                                    <Typography>
                                      KES:{hairType.price}
                                    </Typography>
                                    <Button>Edit</Button>
                                  </AccordionDetails>
                                </Accordion>
                              ))} */}

                              {/* {partnerServices.length > 0 &&
                                partnerServices.map((hairType, index) => (
                                  <Accordion
                                    key={index}
                                    expanded={expanded === `panel${index}`}
                                    onChange={handleExpansion(`panel${index}`)}
                                    TransitionComponent={Fade}
                                    TransitionProps={{ timeout: 400 }}
                                    sx={{
                                      "& .MuiAccordion-region": {
                                        height:
                                          expanded === `panel${index}`
                                            ? "auto"
                                            : 0,
                                      },
                                      "& .MuiAccordionDetails-root": {
                                        display:
                                          expanded === `panel${index}`
                                            ? "block"
                                            : "none",
                                      },
                                    }}
                                  >
                                    <AccordionSummary
                                      expandIcon={<ExpandMoreIcon />}
                                      aria-controls={`panel${index}-content`}
                                      id={`panel${index}-header`}
                                    >
                                      <Typography>
                                        {hairType.subcategory}
                                      </Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                      <Typography>{hairType.type}</Typography>
                                      <Typography>
                                        KES:{hairType.price}
                                      </Typography>
                                      <Button>Edit</Button>
                                    </AccordionDetails>
                                  </Accordion>
                                ))} */}

                              <div className="mt-10 flex flex-col gap-4 px-2 justify-center  mb-4 w-full">
                                {sortedPartnerServices &&
                                  Object.entries(sortedPartnerServices).map(
                                    ([category, services], index) => (
                                      <Accordion
                                        key={index}
                                        expanded={expanded === `panel${index}`}
                                        onChange={handleExpansion(
                                          `panel${index}`
                                        )}
                                        sx={{
                                          "& .MuiAccordion-region": {
                                            height:
                                              expanded === `panel${index}`
                                                ? "auto"
                                                : 0,
                                          },
                                          "& .MuiAccordionDetails-root": {
                                            display:
                                              expanded === `panel${index}`
                                                ? "block"
                                                : "none",
                                          },
                                        }}
                                        className="w-[300px] sm:w-[300px] md:w-[500px] lg:w-[800px]"
                                      >
                                        <AccordionSummary
                                          expandIcon={<ExpandMoreIcon />}
                                          aria-controls={`panel${index}-content`}
                                          id={`panel${index}-header`}
                                        >
                                          <div className="flex flex-row justify-center items-center">
                                            <img
                                              src={hsp}
                                              alt=""
                                              className="w-20"
                                            />
                                            <Typography
                                              sx={{
                                                fontWeight: "bold",
                                                fontSize: [14, 18],
                                              }}
                                            >
                                              {category}
                                            </Typography>
                                          </div>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                          <div className="flex flex-col gap-2">
                                            {services.map(
                                              (service, serviceIndex) => (
                                                <div
                                                  key={serviceIndex}
                                                  className="flex flex-row gap-2 justify-between items-center border-b py-2 border-gray-400"
                                                >
                                                  <Typography>
                                                    -{service.type}
                                                  </Typography>

                                                  <Typography>
                                                    KES: {service.price}
                                                  </Typography>

                                                  <Typography>
                                                    {service.localization}
                                                  </Typography>
                                                  <Typography>
                                                    {service.description}
                                                  </Typography>
                                                  <Button
                                                    variant="contained"
                                                    size="small"
                                                    sx={{ bgcolor: "#000" }}
                                                    onClick={() =>
                                                      handleOpenAdd_duration_price(
                                                        service
                                                      )
                                                    }
                                                  >
                                                    Edit
                                                  </Button>
                                                  {/* <Button>Edit</Button> */}

                                                  {/* <Typography>
                                                {service.localization}
                                              </Typography> */}
                                                  {/* <Typography>
                                                {service.created_at}
                                              </Typography> */}
                                                </div>
                                              )
                                            )}
                                          </div>
                                        </AccordionDetails>
                                      </Accordion>
                                    )
                                  )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : activeStep === 4 ? (
                    <>
                      {/* <div className="mt-4">
                        <h2 className="text-2xl text-start mr-4 font-bold text-gray-600">
                          Choose Venue
                        </h2>
                        {openmap ? (
                          <p>Loading</p>
                        ) : (
                          <iframe
                            marginheight="0"
                            marginwidth="0"
                            src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street,%20Dublin,%20Ireland+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
                            width="100%"
                            height="500"
                            frameborder="0"
                          />
                        )}
                      </div> */}

                      <p>
                        Current Location:-{selectedRowData.location.address}
                      </p>

                      <LocationSearchInput
                        partnerID={selectedRowData._id}
                        isPartner={"yes"}
                      />
                    </>
                  ) : null}
                </Typography>
                <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                  <Button
                    color="inherit"
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    sx={{ mr: 1 }}
                  >
                    Back
                  </Button>
                  <Box sx={{ flex: "1 1 auto" }} />
                  <Button onClick={handleNext} sx={{ mr: 1 }}>
                    Next
                  </Button>
                  {activeStep !== steps.length &&
                    (completed[activeStep] ? (
                      <Typography
                        variant="caption"
                        sx={{ display: "inline-block" }}
                      >
                        Step {activeStep + 1} already completed
                      </Typography>
                    ) : (
                      <Button onClick={handleComplete}>
                        {completedSteps() === totalSteps() - 1
                          ? "Finish"
                          : "Complete Step"}
                      </Button>
                    ))}
                </Box>
              </React.Fragment>
            )}
          </div>
        </Box>
      </Modal>

      {/* start of Add Modal */}
      <Modal
        open={openNewServiceModal}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <>
            <div className="flex flex-row justify-between m-2 py-2">
              <Typography sx={{ color: "#000", fontSize: [18] }}>
                Choose From Available Services
              </Typography>
              <IoMdCloseCircle
                size={18}
                onClick={handleClose_AddNewServiceModal}
                cursor={"pointer"}
                color="#000"
              />
            </div>

            <div>
              {partnerServices &&
                Object.entries(partnerServices).map(
                  ([category, services], index) => (
                    <Accordion
                      key={index}
                      expanded={expanded === `panel${index}`}
                      onChange={handleExpansion(`panel${index}`)}
                      sx={{
                        "& .MuiAccordion-region": {
                          height: expanded === `panel${index}` ? "auto" : 0,
                        },
                        "& .MuiAccordionDetails-root": {
                          display:
                            expanded === `panel${index}` ? "block" : "none",
                        },
                      }}
                    >
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls={`panel${index}-content`}
                        id={`panel${index}-header`}
                      >
                        <Typography>{category}</Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        <div>
                          {services.map((service, serviceIndex) => (
                            <div
                              key={serviceIndex}
                              className="flex flex-row justify-between items-center"
                            >
                              <Typography>{service.type}</Typography>
                              <Typography>{service.description}</Typography>
                              <Button
                                onClick={() =>
                                  handleOpenAdd_duration_price(service)
                                }
                                variant="outlined"
                                size="small"
                              >
                                Pick
                              </Button>
                              {/* <Button>Edit</Button> */}

                              {/* <Typography>
                                                {service.localization}
                                              </Typography> */}
                              {/* <Typography>
                                                {service.created_at}
                                              </Typography> */}
                            </div>
                          ))}
                        </div>
                      </AccordionDetails>
                    </Accordion>
                  )
                )}
            </div>
            {/* <form onSubmit={formikPartnerRegistration.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    variant="outlined"
                    type="text"
                    id="first_name"
                    name="first_name"
                    label="Fist Name"
                    placeholder="First Name"
                    value={formikPartnerRegistration.values.first_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />

                  <TextField
                    type="text"
                    id="last_name"
                    name="last_name"
                    label="Last Name"
                    placeholder="Last Name"
                    value={formikPartnerRegistration.values.last_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    type="text"
                    id="email"
                    name="email"
                    label="Email"
                    placeholder=" Email"
                    value={formikPartnerRegistration.values.email}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                  <TextField
                    id="phone_number"
                    name="phone_number"
                    label="Phone Number"
                    placeholder="Phone Number"
                    value={formikPartnerRegistration.values.phone_number}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>

              <div className="flex flex-row gap-3 ">
                <TextField
                  type="text"
                  id="main_businessName"
                  name="main_businessName"
                  label="Business Name"
                  placeholder="Business Name"
                  value={formikPartnerRegistration.values.main_businessName}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
                <TextField
                  id="password"
                  name="password"
                  label="Password"
                  placeholder="Password"
                  value={formikPartnerRegistration.values.password}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Bio description </FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="bio"
                  id="bio"
                  name="bio"
                  label="Bio"
                  placeholder="Bio"
                  value={formikPartnerRegistration.values.bio}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-3 mt-3">
                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form> */}
          </>
        </Box>
      </Modal>
      {/* End fo Add Modal */}

      {/* start of Add Modal */}
      <Modal
        open={openAddNewDurationPriceModal}
        onClose={handleCloseAdd_duration_price}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <>
            <div className="flex flex-row justify-between m-2 py-2">
              <Typography sx={{ color: "#000", fontSize: [18] }}>
                Add Duration and Price
              </Typography>
              <IoMdCloseCircle
                size={18}
                onClick={handleCloseAdd_duration_price}
                cursor={"pointer"}
                color="#000"
              />
            </div>
            <form onSubmit={formikAddDurationPrice.handleSubmit}>
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  variant="outlined"
                  type="text"
                  id="price"
                  name="price"
                  label="Price"
                  placeholder="Price"
                  value={formikAddDurationPrice.values.price}
                  onChange={formikAddDurationPrice.handleChange}
                  onBlur={formikAddDurationPrice.handleBlur}
                />

                <TextField
                  type="text"
                  id="duration"
                  name="duration"
                  label="Duration"
                  placeholder="Duration"
                  value={formikAddDurationPrice.values.duration}
                  onChange={formikAddDurationPrice.handleChange}
                  onBlur={formikAddDurationPrice.handleBlur}
                />
              </div>

              <div className="flex flex-col gap-3 mt-3">
                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form>

            {/* <form onSubmit={formikPartnerRegistration.handleSubmit}>
              <div className="flex flex-col gap-3 mt-3">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    variant="outlined"
                    type="text"
                    id="first_name"
                    name="first_name"
                    label="Fist Name"
                    placeholder="First Name"
                    value={formikPartnerRegistration.values.first_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />

                  <TextField
                    type="text"
                    id="last_name"
                    name="last_name"
                    label="Last Name"
                    placeholder="Last Name"
                    value={formikPartnerRegistration.values.last_name}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>
              <div className="flex flex-col gap-3 ">
                <div className="flex flex-row gap-3  mb-4">
                  <TextField
                    type="text"
                    id="email"
                    name="email"
                    label="Email"
                    placeholder=" Email"
                    value={formikPartnerRegistration.values.email}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                  <TextField
                    id="phone_number"
                    name="phone_number"
                    label="Phone Number"
                    placeholder="Phone Number"
                    value={formikPartnerRegistration.values.phone_number}
                    onChange={formikPartnerRegistration.handleChange}
                    onBlur={formikPartnerRegistration.handleBlur}
                  />
                </div>
              </div>

              <div className="flex flex-row gap-3 ">
                <TextField
                  type="text"
                  id="main_businessName"
                  name="main_businessName"
                  label="Business Name"
                  placeholder="Business Name"
                  value={formikPartnerRegistration.values.main_businessName}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
                <TextField
                  id="password"
                  name="password"
                  label="Password"
                  placeholder="Password"
                  value={formikPartnerRegistration.values.password}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Bio description </FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="bio"
                  id="bio"
                  name="bio"
                  label="Bio"
                  placeholder="Bio"
                  value={formikPartnerRegistration.values.bio}
                  onChange={formikPartnerRegistration.handleChange}
                  onBlur={formikPartnerRegistration.handleBlur}
                />
              </div>
              <div className="flex flex-col gap-3 mt-3">
                <div>
                  <Button
                    variant="contained"
                    sx={{ backgroundColor: "#242333" }}
                    size="small"
                    type="submit"
                  >
                    Save
                  </Button>
                </div>
              </div>
            </form> */}
          </>
        </Box>
      </Modal>
      {/* End fo Add Modal */}
      {loading && <LinearProgress />}
      <AdminAddedSuccess
        open={openAddAdminSuccess}
        autoHideDuration={6000}
        name={"Provider"}
        mssg={"Added Succefully"}
        onClose={() => setOpenAddAdminSuccess(false)}
      />
      <AdminAddedSuccess
        open={openDurationTimeSuccess}
        autoHideDuration={6000}
        name={"Service"}
        mssg={"Updated Succefully"}
        onClose={() => setOpenDurationTimeSuccess(false)}
      />
      <AdminAddedSuccess
        open={openImageSuccess}
        autoHideDuration={6000}
        name={"Image"}
        mssg={"Added Succefully"}
        onClose={() => setOpenImageSuccess(false)}
      />
      <AdminAddedSuccess
        open={openProviderUpdateSuccess}
        autoHideDuration={6000}
        name={"Provider"}
        mssg={"Updated Succefully"}
        onClose={() => setOpenProviderUpdateSuccess(false)}
      />

      <AdminAddfailed
        open={openAddAdminFailed}
        autoHideDuration={6000}
        mssg={"Failed To Add"}
        name={"Provider"}
      />

      <AdminAddfailed
        open={openAddAdminFailed_emailTaken}
        autoHideDuration={2000}
        mssg={"Email Already Taken"}
        name={""}
      />
      <AdminAddfailed
        open={openProviderUpdateFailed}
        autoHideDuration={6000}
        mssg={"Failed To Update"}
        name={"Provider"}
      />

      <Navbar />
      <div className="relative sm:ml-[300px] bg-white h-screen select-none">
        <TopNav />
        <div className="bg-white relative h-s  flex flex-col select-none">
          <div className="absolute top-4 left-4 justify-between sm:hidden md:hiddden   z-[1000]    lg:hidden">
            <UserNameTag />
          </div>
          <div className="border-b border-gray-400 sm:m-6 m-3 py-2 flex sm:flex-row flex-col justify-between">
            <div className="flex sm:flex-row flex-col justify-start ">
              <div className="flex flex-col justify-start text-start mt-8 ">
                <h1 className="text-lg sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
                  Providers
                </h1>

                {/* <img src={pp}/> */}
              </div>
              <div className=" sm:mt-8 mt-1 ">
                <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                  <SearchIconWrapper>
                    <SearchIcon sx={{ color: "#616569" }} />
                  </SearchIconWrapper>
                  <StyledInputBase
                    value={searchTerm}
                    onChange={handleSearchChange}
                    placeholder="Search…"
                    inputProps={{ "aria-label": "search" }}
                  />
                </Search>
              </div>

              <div className="mt-6">
                <FormControl sx={{ m: 1, minWidth: 186 }} size="small">
                  <InputLabel id="demo-select-small-label">
                    Select subcategory
                  </InputLabel>
                  <Select
                    labelId="demo-select-small-label"
                    id="subcategory"
                    // value={subcategory}
                    value={subcategory}
                    label="subcategory"
                    onChange={handleChangeType}
                  >
                    {/* Map over the array of menu items to generate MenuItem components */}
                    {menuItems.map((item, index) => (
                      <MenuItem key={index} value={item.value}>
                        {item.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
            </div>

            <div className="mt-8 flex">
              <Button variant="contained" onClick={handleOpen_AddCompany}>
                Add Provider
              </Button>
            </div>
          </div>
          <div
            style={{
              // height: 400,
              // width: "100%",
              backgroundColor: "#fff",
              margin: "1rem",
            }}
          >
            <Paper sx={{ width: "100%", overflow: "auto" }}>
              <TableContainer sx={{ maxHeight: 800, minWidth: 1000 }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow className="select-none">
                      <TableCell
                        sx={{
                          fontWeight: 800,
                          fontSize: ["14px", "14px"],
                          width: "200px",
                        }}
                      >
                        Provider Name
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Company Name
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Email
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Signup Date
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Phone Number
                      </TableCell>
                      {/* <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Wallet Balance
                      </TableCell> */}

                      {/* <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Manager Services
                      </TableCell> */}

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Status
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Featured
                      </TableCell>

                      <TableCell
                        align="center"
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Action
                      </TableCell>

                      {items}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => (
                        <TableRow
                          key={index}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                          className="hover:bg-gray-400 select-none"
                          //onDoubleClick={handleRowDoubleClick}
                          //onDoubleClick={() => handleRowDoubleClick(row)}
                          onMouseEnter={() => {
                            setSelectedRowData(row);
                            //setIndex(index);
                          }}
                        >
                          {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                          <TableCell
                            className="flex flex-row w-10"
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.first_name} &nbsp;
                            {row.last_name}
                          </TableCell>
                          <TableCell
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.main_businessName}
                          </TableCell>
                          <TableCell
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.email}
                          </TableCell>
                          <TableCell
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.created_at}
                          </TableCell>
                          <TableCell
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.phone_number}
                          </TableCell>
                          <TableCell>
                            <Button
                              sx={{
                                fontSize: [12, 13],
                                borderWidth: "2px",
                                borderColor:
                                  row.status === "INACTIVE" ? "red" : "green",
                                borderStyle: "solid",
                                padding: "0.5rem", // Adjust padding as needed
                                display: "inline-block", // Ensures the border fits tightly around the text
                                borderRadius: "0.25rem",
                                color: "black",
                              }}
                            >
                              {row.status}
                            </Button>
                          </TableCell>

                          <TableCell>
                            <Button
                              sx={{
                                fontSize: [12, 13],
                                borderWidth: "2px",
                                borderColor:
                                  row.isFeatured === "NO" ? "red" : "green",
                                borderStyle: "solid",
                                padding: "0.5rem", // Adjust padding as needed
                                display: "inline-block", // Ensures the border fits tightly around the text
                                borderRadius: "0.25rem",
                                color: "black",
                              }}
                            >
                              {row.isFeatured}
                            </Button>
                          </TableCell>
                          {/* <TableCell
                            sx={{
                              fontSize: [12, 13],
                            }}
                          >
                            {row.phone_number}
                          </TableCell> */}
                          {/* <TableCell>
                            {parseFloat(row.email).toLocaleString()}
                          </TableCell> */}

                          {/* Selling Price Column */}

                          {/* house unit column */}

                          {/* Location column */}
                          <TableCell align="right">
                            {/* {row.house_information.map((house, index) => (
                    <div key={index}>{house.house_status} </div>
                  ))}
                */}
                            <IconButton aria-controls={actionId}>
                              <CiSettings
                                color="#111111"
                                size={23}
                                onClick={handleActionMenuOpen}
                              />
                            </IconButton>

                            {/* 
                      {row.house.map((x, index) => (
                        <div key={index} className="flex">
                          <LocationOnOutlinedIcon fontSize="20px" />{" "}
                          {x.residence}
                        </div>
                      ))} */}
                          </TableCell>
                          {/* Status column */}
                          <TableCell align="right">
                            {/* {row.house.map((x, index) => (
                        <div key={index} className="flex">
                          <LocationOnOutlinedIcon fontSize="20px" />{" "}
                          {x.location}
                        </div>
                      ))} */}
                          </TableCell>

                          {/* Amount Paid Column */}
                          {/* Balance Column */}
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                className="select-none"
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
      </div>
    </>
  );
}
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  color: "black",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["100%", "40%"],
};
const style_box2 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "90%"],
};
const style_box3 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
  width: ["90%", "35%"],
};
const style_box4 = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",

  maxHeight: "80vh",
  width: ["90%", "68%"],
};
