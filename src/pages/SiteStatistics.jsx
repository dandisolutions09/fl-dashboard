import React from "react";

import { MdBrightness1 } from "react-icons/md";
import { TbRobot } from "react-icons/tb";
import { MdGames } from "react-icons/md";
import { FaPlus, FaUsers } from "react-icons/fa";
import game11 from "../assets/admin-logo.png";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import { Typography } from "@mui/material";
import { ImStatsDots } from "react-icons/im";
import { HiServer } from "react-icons/hi";
import { Gauge, gaugeClasses } from "@mui/x-charts/Gauge";
import endpoints from "../endpoints";
import { LineChart } from "@mui/x-charts";
import UserNameTag from "../components/UserNameTag";
import BasicBarChart from "../components/Trend-Charts/Bar";

export default function SiteStatistics() {
  const [loading, setLoading] = React.useState(false);

  const settings = {
    width: 200,
    height: 200,
    value: 60,
  };

  function handleGetAllRecords(usrId) {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
  //  console.log("json_token", json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);

    fetch(endpoints.getPartners, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log("JSON:->", json);
        setRows(json);
        // Your further processing logic
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  React.useEffect(() => {
    handleGetAllRecords();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      <Navbar />
      <div className="relative sm:ml-[300px] ">
        <TopNav />
        <div className="flex flex-col relative bg-[#fff]  md:w-500px h-full select-none">
          <div className="absolute top-4 left-8 justify-between sm:hidden md:hiddden   z-[1000]    lg:hidden">
            <UserNameTag />
          </div>
          <div className="flex flex-col m-6 py-2 justify-start text-start mt-8 border-b border-gray-400">
            <h1 className="text-base sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
              Site Statistics
            </h1>
          </div>
          <div className="flex flex-col gap-3 ">
            {/* Start*/}
            <div className="flex sm:flex-row flex-col justify-between px-4 gap-3">
              <div
                className="flex-1 bg-cover bg-center w-full rounded-lg border border-gray-300"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [10, 12, 14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    Site Statistics
                  </Typography>
                </div>
                <div className="mt-4 grid md:grid-cols-2 lg:grid-cols-2 gap-4 px-4 h-64 justify-center text-center">
                  {/* <Gauge
                    value={75}
                    startAngle={-110}
                    endAngle={110}
                    sx={{
                      [`& .${gaugeClasses.valueText}`]: {
                        fontSize: [14, 27, 30],
                        transform: "translate(0px, 0px)",
                      },
                    }}
                    text={({ value, valueMax }) => `${value} / ${valueMax}`}
                  /> */}

                  <BasicBarChart />
                </div>
              </div>
              <div
                className="flex-1 bg-cover bg-center w-full  rounded-lg border border-gray-300"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [10, 12, 14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    Providers Statistics
                  </Typography>
                </div>
                <div className="justify-center text-center">
                  <LineChart
                    xAxis={[{ data: [1, 2, 3, 5, 8, 10] }]}
                    series={[
                      {
                        data: [2, 5.5, 2, 8.5, 1.5, 5],
                      },
                    ]}
                    width={390}
                    height={300}
                  />
                </div>
              </div>
            </div>
            <div className="flex sm:flex-row flex-col  justify-between px-4 gap-3">
              <div
                className="flex-1 bg-cover bg-center w-full  rounded-lg shadow-md shadow-gray-400"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [10, 12, 14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    Site Statistics
                  </Typography>
                </div>
                <div className="mt-4 grid md:grid-cols-2 lg:grid-cols-2 gap-4 px-4 h-64 justify-center text-center">
                  <Gauge
                    {...settings}
                    cornerRadius="50%"
                    sx={(theme) => ({
                      [`& .${gaugeClasses.valueText}`]: {
                        fontSize: [28, 30, 40],
                      },
                      [`& .${gaugeClasses.valueArc}`]: {
                        fill: "#52b202",
                      },
                      [`& .${gaugeClasses.referenceArc}`]: {
                        fill: theme.palette.text.disabled,
                      },
                    })}
                  />
                </div>
              </div>
              <div
                className="flex-1 bg-cover bg-center w-full sm:w-[430px] rounded-lg shadow-md shadow-gray-400"
                style={{ background: "#fff" }}
              >
                <div className="relative bg-blue-600 p-2 flex flex-row gap-2 rounded-t-lg border border-blue-600">
                  <ImStatsDots size={20} className="m-1" color="#fff" />
                  <Typography
                    sx={{ fontSize: [10, 12, 14], marginTop: 1 }}
                    className="m-1 text-[#fff]"
                  >
                    Site Statistics
                  </Typography>
                </div>
                <div className="mt-4 grid md:grid-cols-2 lg:grid-cols-2 gap-4 px-4 h-64 justify-center text-center">
                  {" "}
                  <Gauge
                    value={75}
                    startAngle={-110}
                    endAngle={110}
                    sx={{
                      [`& .${gaugeClasses.valueText}`]: {
                        fontSize: [10, 27, 30],
                        transform: "translate(0px, 0px)",
                      },
                    }}
                    text={({ value, valueMax }) => `${value} / ${valueMax}`}
                  />
                </div>
              </div>
            </div>
            {/* end */}
          </div>
        </div>
      </div>
    </>
  );
}
