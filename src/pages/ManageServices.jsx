import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import endpoints from "../endpoints";
import { CiSettings } from "react-icons/ci";
import { CgArrowsExchangeV } from "react-icons/cg";

import {
  Box,
  Button,
  FormLabel,
  IconButton,
  LinearProgress,
  Menu,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
import { styled, alpha } from "@mui/material/styles";
import { BsGearFill } from "react-icons/bs";
import Select from "@mui/material/Select";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import { Textarea } from "../styles";
import {
  MdAirplanemodeActive,
  MdAirplanemodeInactive,
  MdEdit,
  MdOutlineAirplanemodeInactive,
} from "react-icons/md";
import { RiDeleteBinFill } from "react-icons/ri";
import { IoIosCloseCircle, IoIosCloseCircleOutline } from "react-icons/io";
import UserNameTag from "../components/UserNameTag";
import axios from "axios";
import { useFormik } from "formik";
import AdminAddedSuccess from "../components/Alerts/AdminAddedSuccess";
import { useState } from "react";
import avatar from "../assets/avatar5.jpg";
import GeneralSuccess from "../components/Alerts-2/GeneralSuccess";
import GeneralError from "../components/Alerts-2/GeneralError";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",

    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function ManageServices() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openAction, setOpenAction] = React.useState(false);
  const [isOPen, setIsOpen] = React.useState(false);
  const [subcategory, setSubcategory] = React.useState("all");
  const [openAddCompany, setOpenAddCompany] = React.useState(false);
  const [openServiceCategoryModal, setOpenAddServiceCategory] =
    React.useState(false);

  const [openEditService, setOpenEditService] = React.useState(false);
  const [searchTerm, setSearchTerm] = React.useState();
  const [openAddAdminSuccess, setOpenAddAdminSuccess] = React.useState(false);

  const [selectedRowData, setSelectedRowData] = useState();

  const [category_pic, setCategoryPic] = useState();

  const actionId = "primary-action-menu";

  const [stateToken, setToken] = useState();

  const [serviceCategories, setServiceCategories] = useState();

  const [filterItems, setFilterItems] = useState();

  const [generalError, setGeneralError] = useState();
  const [generalSuccess, setGeneralSuccess] = useState();

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  //setCategories

  const menuItems = [
    { value: "all", label: "All" },
    { value: "Mens Hair", label: "Mens Hair" },
    { value: "Womens Hair", label: "Womens Hair" },
    { value: "Face", label: "Face" },
    { value: "Nail", label: "Nail" },
    { value: "Henna", label: "Henna" },
    { value: "Body", label: "Body" },
  ];

  const category_items = [
    { value: "Mens Hair", label: "Mens Hair" },
    { value: "Womens Hair", label: "Womens Hair" },
    { value: "Face Services", label: "Face Services" },
    { value: "Nail Services", label: "Nail Services" },
    { value: "Henna Services", label: "Henna Services" },
  ];

  const handleOpen_AddCompany = () => {
    setOpenAddCompany(true);
  };

  const handleOpen_AddCategory = () => {
    setOpenAddServiceCategory(true);
  };
  const handleClose_AddCompany = () => {
    setOpenAddCompany(false);
  };

  //handleCloseAddServiceCategoryModal

  const handleCloseAddServiceCategoryModal = () => {
    setOpenAddServiceCategory(false);
  };
  const handleOpenEditservice = () => {
    setOpenEditService(true);

    console.log("Selected Row Data", selectedRowData);

    formikEditServiceType.setValues({
      // email: selectedRowData.email,
      // first_name: selectedRowData.first_name,
      // last_name: selectedRowData.last_name,
      // email: selectedRowData.email,
      // phone_number: selectedRowData.phone_number,
      // main_businessName: selectedRowData.main_businessName,
      // bio: selectedRowData.bio,

      type: selectedRowData.type,
      status: "available",
      description: selectedRowData.description,
      localization: selectedRowData.localization,
      subcategory: selectedRowData.subcategory,
    });
  };
  const handleCloseEditservice = () => {
    setOpenEditService(false);
  };
  function reloadPage() {
    window.location.reload();
  }

  function handleManageUpload(e) {
    // Access the file object
    const file = e.target.files[0];
    console.log("upload profile picture");

    // handleUploadProfile(file);
    handleUploadCategoryImage(file);

    // Create a new FileReader
    //const reader = new FileReader();

    // // Set up the onload event to handle the file read operation
    // reader.onload = () => {
    //   // Get the Base64 encoded data
    //   const base64Data = reader.result;

    //   // Assign the Base64 data to the src attribute of the image element
    //   if (profilePicRef.current) {
    //     profilePicRef.current.src = base64Data;
    //   }

    //   // setPP(base64Data);

    //   // Optionally, you can handle the Base64 data (e.g., store it in state)
    //   // console.log("Base64 encoded image:", base64Data);
    // };

    // // Read the contents of the file as a Data URL
    // reader.readAsDataURL(file);
    // setFile(URL.createObjectURL(e.target.files[0]));
  }

  const handleUploadCategoryImage = async (file) => {
    try {
      setLoading(true);
      const formData = new FormData();
      formData.append("category-photo", file);

      // Replace 'your-upload-endpoint' with your actual upload endpoint
      var storedToken = localStorage.getItem("stored_token");
      var json_token = JSON.parse(storedToken);
     // console.log("json_token", json_token.token);
      setToken(json_token.token);
      const response = await axios.post(
        endpoints.uploadServiceCategoryPic,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${json_token.token}`,
          },
        }
      );

      console.log("Upload successful:", response.data.image_url);

      setSuccessMsg("Category photo uploaded successfully!");
      setGeneralSuccess(true);

      //setPP(response.data.image_url);
      // console.log("ID", selectedRowData._id);
      //handleUpdateProfilePicURL(response.data.image_url);
      setCategoryPic(response.data.image_url);
    } catch (error) {
      console.error("Error uploading file:", error);

      setErrorMsg("Photo Upload Not Successful!");
      setGeneralError(true);
    } finally {
      console.log("Request completed."); // This will be executed regardless of success or failure
      setLoading(false);
    }
  };

  const items = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={actionId}
      keepMounted
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      open={isOPen}
      onClose={handleActionMenuClose}
    >
      <MenuItem className="flex gap-2" onClick={handleOpenEditservice}>
        <MdEdit /> Edit
      </MenuItem>
      <MenuItem className="flex gap-2">
        <MdAirplanemodeActive /> Activate
      </MenuItem>
      <MenuItem className="flex gap-2">
        <MdOutlineAirplanemodeInactive /> Deactivate
      </MenuItem>
      <MenuItem className="flex gap-2">
        <RiDeleteBinFill /> Delete
      </MenuItem>
    </Menu>
  );
  function handleActionMenuClose() {
    setIsOpen(false);
  }
  function handleActionMenuOpen() {
    setIsOpen(true);
  }

  function handleGetAllServices(searchVal) {
    setLoading(true);
    console.log("handle get all records called");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    if (subcategory !== null) {
      const filtered_type = subcategory;
      console.log("subcategory", filtered_type);

      axios
        .get(`${endpoints.getServices}?subcategory=${filtered_type}`, {
          headers: {
            Authorization: `Bearer ${json_token.token}`,
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          const json = response.data.child_services;
          console.log("Response data: ", response.data);
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response services:-> ", response.data);
          // setRows(json);

          localStorage.setItem("servicesData", JSON.stringify(json));
          console.log("servicesData data set globally...");
          setRows(json);

          // if (searchVal === undefined || searchVal.trim().length === 0) {
          //   console.log("my json", rows);
          //   setRows(json);
          // } else {
          //   const results = json.filter((user) => {
          //     const Type = user.type ? user.type.toLowerCase() : "";
          //     const searchTerm = searchVal.toLowerCase();
          //     return Type.includes(searchTerm);
          //   });
          //   console.log("my results", results);
          //   setRows(results);
          // }
        })
        .catch((error) => {
          console.error("There was a problem with your Axios request:", error);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      console.log("Subcategiry empty", subcategory);
    }
  }

  function filterUsers(searchVal) {
    const data = JSON.parse(localStorage.getItem("servicesData")) || [];

    console.log("To be filtered users:->", data);

    if (searchVal === undefined || searchVal.trim().length === 0) {
      setRows(data);
    } else {
      const searchTerm = searchVal.toLowerCase().split(" ");

      const results = data.filter((user) => {
        const type = user.type ? user.type.toLowerCase() : "";
        const firstName = user.first_name ? user.first_name.toLowerCase() : "";
        const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        const email = user.email ? user.email.toLowerCase() : "";
        const phoneNumber = user.phone_number ? user.phone_number : "";
        const companyName = user.main_businessName
          ? user.main_businessName.toLowerCase()
          : "";

        return searchTerm.every(
          (term) =>
            type.includes(term) ||
            firstName.includes(term) ||
            lastName.includes(term) ||
            email.includes(term) ||
            phoneNumber.includes(term) ||
            companyName.includes(term)
        );
      });

      setRows(results);
    }
  }

  //handle get service categories

  function handleGetAllServiceCategories(searchVal) {
    setLoading(true);
    console.log("handle get all records called");
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
   // console.log("json_token", json_token.token);

    if (subcategory !== null) {
      const filtered_type = subcategory;
      console.log("subcategory", filtered_type);

      axios
        .get(`${endpoints.getServiceCategories}`, {
          headers: {
            // Authorization: `Bearer ${json_token.token}`,
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          const json = response.data;
          //json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response data: ", response.data.data);
          // setServiceCategories(response.data.categories);

          const serviceCategories = response.data.data.categories.map(
            (category) => ({
              value: category.category_name,
              label: category.category_name,
            })
          );

          console.error("INCOMING SERVICE CATEGORY!!!!", serviceCategories);

          setServiceCategories(serviceCategories);

          setFilterItems(serviceCategories);

          // setRows(json);

          // if (searchVal === undefined || searchVal.trim().length === 0) {
          //   console.log("my json", rows);
          //   setRows(json);
          // } else {
          //   const results = json.filter((user) => {
          //     const Type = user.type ? user.type.toLowerCase() : "";
          //     const searchTerm = searchVal.toLowerCase();
          //     return Type.includes(searchTerm);
          //   });
          //   console.log("my results", results);
          //   setRows(results);
          // }
        })
        .catch((error) => {
          console.error("There was a problem with your Axios request:", error);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      console.log("Subcategiry empty", subcategory);
    }
  }

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    filterUsers(event.target.value);
    //handleGetAllServices(event.target.value);
  };

  function sortServices(beauty_subcat) {
    setLoading(true);
    console.log("sort services functions called", beauty_subcat);
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
   // console.log("json_token", json_token.token);
    axios
      //.get(endpoints.getBeautyServices, {
      .get(`${endpoints.getServices}?subcategory=${beauty_subcat}`, {
        headers: {
          Authorization: `Bearer ${json_token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        console.log("error Response data -re: ", response.data.child_services);

        // Your further processing logic

        // const json = response.data;

        if (response.data) {
          const json = response.data.child_services;
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response data: ", response.data);
          setRows(json);
        } else {
          setRows([]);
        }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoading(false);
      });
    /// } else {
    //console.log("Subcategiry empty", subcategory);
    // }
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleChange = (event) => {
    console.log("event selected:->", event.target.value);
    setSubcategory(event.target.value);

    if (event.target.value) {
      sortServices(event.target.value);
    }
  };

  const postService = async (data, subcat) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    const serviceRegistrationObject = {
      //subcategory: data.subcategory,
      type: data.type,
      status: "available",
      description: data.description,
      localization: data.localization,
      subcategory: data.subcategory,
    };

    console.log("main object to be sent:->", serviceRegistrationObject);

    try {
      setLoading(true); // Set loading to true when making the request
      const response = await axios.post(
        `${endpoints.postServices}`,
        serviceRegistrationObject,
        {
          headers: {
            Authorization: `Bearer ${json_token.token}`,
            "Content-Type": "application/json",
          },
        }
      );
      console.log("Response from the backend:", response.data);
      setOpenAddAdminSuccess(true);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false);

      // Set loading to false after the request is complete (success or error)
    }
    setTimeout(() => {
      setOpenAddCompany(false);
      reloadPage();
    }, 2000);
  };

  const postServiceCategory = async (data) => {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    // const serviceRegistrationObject = {
    //   //subcategory: data.subcategory,
    //   category_name: data.type,
    //   status: "available",
    //   description: data.description,
    //   localization: data.localization,
    //   subcategory: data.subcategory,
    // };

    console.log("main object to be sent:->", data);

    try {
      setLoading(true); // Set loading to true when making the request
      const response = await axios.post(
        `${endpoints.postServicesCategory}`,
        data,
        {
          headers: {
            Authorization: `Bearer ${json_token.token}`,
            "Content-Type": "application/json",
          },
        }
      );
      console.log("Response from the backend:", response.data);
      setOpenAddAdminSuccess(true);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false);

      // Set loading to false after the request is complete (success or error)
    }
    // setTimeout(() => {
    //   setOpenAddCompany(false);
    //   reloadPage();
    // }, 2000);
  };

  const formikServiceRegistration = useFormik({
    initialValues: {
      subcategory: "",
      type: "",
      status: "",
      description: "",
      localization: "",
    },

    onSubmit: (values) => {
      console.log("form data:-", values);
      postService(values, values.subcategory);

      //postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  const formikServiceCategoryRegistration = useFormik({
    initialValues: {
      category: "",
      // type: "",
      // status: "",
      // description: "",
      // localization: "",
    },

    onSubmit: (values) => {
      console.log("form data:-", values);

      console.log("category pic url", category_pic);

      const cat_object = {
        category_name: values.category,
        category_pic: category_pic,
      };

      console.log("category object about to be uploaded", cat_object);

      postServiceCategory(cat_object);

      //postService(values, values.subcategory);

      //postPartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  //FORMIK EDIT
  const formikEditServiceType = useFormik({
    // type: selectedRowData.type,
    // status: "available",
    // description: selectedRowData.description,
    // localization: selectedRowData.localization,
    // subcategory: selectedRowData.subcategory,

    initialValues: {
      type: "",
      status: "",
      description: "",
      localization: "",
      //subcategory: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      //postPartnerRegistrationData(values);
      //updatePartnerRegistrationData(values);

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  React.useEffect(() => {
    handleGetAllServices();
    handleGetAllServiceCategories();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />

      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />
      <AdminAddedSuccess
        open={openAddAdminSuccess}
        autoHideDuration={6000}
        name={"Service"}
        mssg={"Added Succefully"}
        onClose={() => setOpenAddAdminSuccess(false)}
      />
      <Modal
        open={openAddCompany}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box} className="select-none">
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4 text-[#111111]">Add Service</h2>
            <IoIosCloseCircle
              color="#000"
              size={23}
              cursor={"pointer"}
              onClick={handleClose_AddCompany}
            />
          </div>

          <form onSubmit={formikServiceRegistration.handleSubmit}>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3 mt-3">
                <TextField
                  type=""
                  id="type"
                  name="type"
                  label="Service Type"
                  placeholder="Service Type"
                  value={formikServiceRegistration.values.type}
                  onChange={formikServiceRegistration.handleChange}
                  onBlur={formikServiceRegistration.handleBlur}
                />

                <TextField
                  type=""
                  id="localization"
                  name="localization"
                  label="Localization"
                  placeholder="Localization"
                  value={formikServiceRegistration.values.localization}
                  onChange={formikServiceRegistration.handleChange}
                  onBlur={formikServiceRegistration.handleBlur}
                />
              </div>
              <FormControl>
                <FormLabel>Select Subcategory </FormLabel>

                <TextField
                  id="subcategory"
                  select
                  name="subcategory"
                  fullWidth
                  value={formikServiceRegistration.values.subcategory}
                  onChange={formikServiceRegistration.handleChange}
                  onBlur={formikServiceRegistration.handleBlur}
                  helperText="Select position"
                >
                  {serviceCategories &&
                    serviceCategories.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                </TextField>
              </FormControl>
            </div>
            <div className="flex flex-col mt-3 gap-3 ">
              <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Description:-</FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="description"
                  id="description"
                  name="description"
                  label="Description"
                  placeholder="Description"
                  value={formikServiceRegistration.values.description}
                  onChange={formikServiceRegistration.handleChange}
                  onBlur={formikServiceRegistration.handleBlur}
                />
              </div>
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <Modal
        open={openServiceCategoryModal}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box} className="select-none">
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4 text-[#111111]">
              Add Service Category
            </h2>
            <IoIosCloseCircle
              color="#000"
              size={23}
              cursor={"pointer"}
              onClick={handleCloseAddServiceCategoryModal}
            />
          </div>

          <form onSubmit={formikServiceCategoryRegistration.handleSubmit}>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3 mt-3">
                <TextField
                  type=""
                  id="category"
                  name="category"
                  label="Category Name"
                  placeholder="Category Name"
                  value={formikServiceCategoryRegistration.values.category}
                  onChange={formikServiceCategoryRegistration.handleChange}
                  onBlur={formikServiceCategoryRegistration.handleBlur}
                />

                {/* <div className="flex flex-row gap-3 border-b bod border-gray-400 py-3">
                  <label className="text-black">Profile</label>
                </div> */}
              </div>

              <div className="flex-col gap-2">
                <div className="flex flex-row gap-3 border-b bod border-gray-400 py-3">
                  <label className="text-black">
                    Upload Service Category Pic:
                  </label>
                </div>

                <img
                  src={!category_pic ? avatar : category_pic}
                  className="sm:w-[150px] w-[70px] sm:h-[150px] h-[70px] rounded-full object-cover"
                />

                <input
                  type="file"
                  onChange={handleManageUpload}
                  className="mt-[20%] cursor-pointer"
                />
              </div>
              {/* <FormControl>
                <FormLabel>Select Subcategory </FormLabel>

                <TextField
                  id="subcategory"
                  select
                  name="subcategory"
                  fullWidth
                  value={formikServiceRegistration.values.subcategory}
                  onChange={formikServiceRegistration.handleChange}
                  onBlur={formikServiceRegistration.handleBlur}
                  helperText="Select position"
                >
                  {category_items.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl> */}
            </div>
            {/* <div className="flex flex-col mt-3 gap-3 ">
              <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Description:-</FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="description"
                  id="description"
                  name="description"
                  label="Description"
                  placeholder="Description"
                  value={formikServiceRegistration.values.description}
                  onChange={formikServiceRegistration.handleChange}
                  onBlur={formikServiceRegistration.handleBlur}
                />
              </div>
            </div> */}

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openEditService}
        onClose={handleCloseEditservice}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4 text-[#111111]">Add Service</h2>
            <IoIosCloseCircle
              color="#000"
              size={23}
              cursor={"pointer"}
              onClick={handleClose_AddCompany}
            />
          </div>

          <form onSubmit={formikEditServiceType.handleSubmit}>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3 mt-3">
                {/* <FormControl>
                  <FormLabel>Select Subcategory </FormLabel>

                  <TextField
                    id="subcategory"
                    select
                    name="subcategory"
                    fullWidth
                    value={formikEditServiceType.values.subcategory}
                    onChange={formikEditServiceType.handleChange}
                    onBlur={formikEditServiceType.handleBlur}
                    helperText="Select position"
                  >
                    {menuItems.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl> */}

                <TextField
                  type=""
                  id="type"
                  name="type"
                  label="Service Type"
                  placeholder="Service Type"
                  value={formikEditServiceType.values.type}
                  onChange={formikEditServiceType.handleChange}
                  onBlur={formikEditServiceType.handleBlur}
                />

                <TextField
                  type=""
                  id="localization"
                  name="localization"
                  label="Localization"
                  placeholder="Localization"
                  value={formikEditServiceType.values.localization}
                  onChange={formikEditServiceType.handleChange}
                  onBlur={formikEditServiceType.handleBlur}
                />
              </div>
            </div>
            <div className="flex flex-col mt-3 gap-3 ">
              <div className="flex flex-col gap-1  mt-3">
                <FormLabel>Description:-</FormLabel>
                <Textarea
                  className="bg-white border border-gray-400 rounded-md py-2"
                  maxRows={4}
                  aria-label="maximum height"
                  type="description"
                  id="description"
                  name="description"
                  label="Description"
                  placeholder="Description"
                  value={formikEditServiceType.values.description}
                  onChange={formikEditServiceType.handleChange}
                  onBlur={formikEditServiceType.handleBlur}
                />
              </div>
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      {loading && <LinearProgress />}
      <Navbar />
      <div className="relative sm:ml-[300px] bg-white h-screen">
        <TopNav />
        <div className="bg-white relative h-s  flex flex-col select-none">
          <div className="absolute top-4 left-4 justify-between sm:hidden md:hiddden   z-[1000]    lg:hidden">
            <UserNameTag />
          </div>
          <div className="border-b border-gray-400 sm:m-6 m-3 py-2 flex sm:flex-row flex-col justify-between">
            <div className="flex sm:flex-row flex-col justify-start ">
              <div className="flex flex-col justify-start text-start mt-8 ">
                <h1 className="text-lg sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[3px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
                  Beauty Service
                </h1>
              </div>
              <div className=" sm:mt-8 mt-1 ">
                <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                  <SearchIconWrapper>
                    <SearchIcon sx={{ color: "#616569" }} />
                  </SearchIconWrapper>
                  <StyledInputBase
                    placeholder="Search…"
                    inputProps={{ "aria-label": "search" }}
                    value={searchTerm}
                    onChange={handleSearchChange}
                  />
                </Search>
              </div>
              <div className="mt-6">
                <FormControl sx={{ m: 1, minWidth: 186 }} size="small">
                  <InputLabel id="demo-select-small-label">
                    Select subcategory
                  </InputLabel>
                  <Select
                    labelId="demo-select-small-label"
                    id="subcategory"
                    // value={subcategory}
                    value={subcategory}
                    label="subcategory"
                    onChange={handleChange}
                  >
                    {/* Map over the array of menu items to generate MenuItem components */}
                    {filterItems &&
                      filterItems.map((item, index) => (
                        <MenuItem key={index} value={item.value}>
                          {item.label}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </div>
            </div>

            <div className="mt-8 flex gap-4">
              <Button variant="contained" onClick={handleOpen_AddCompany}>
                Add Service
              </Button>

              <Button variant="contained" onClick={handleOpen_AddCategory}>
                Add Service Category
              </Button>
            </div>
          </div>
          <div
            style={{
              // height: 400,
              // width: "100%",
              backgroundColor: "#fff",
              margin: "1rem",
            }}
          >
            {items}
            <Paper sx={{ width: "100%", overflow: "auto" }}>
              <TableContainer sx={{ maxHeight: 800, minWidth: 1000 }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow className="select-none">
                      <TableCell
                        sx={{
                          fontWeight: 800,
                          fontSize: ["14px", "14px"],
                          width: "200px",
                        }}
                      >
                        Type
                      </TableCell>
                      {/* <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Sub Category
                      </TableCell> */}
                      {/* <TableCell
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          fontWeight: 800,
                          fontSize: ["14px", "14px"],
                        }}
                      >
                        Display Order
                        <div style={{ marginRight: "5px" }}>
                          <CgArrowsExchangeV size={25} cursor={"pointer"} />
                        </div>
                      </TableCell> */}
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Localization
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Status
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Subcategory
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Action
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => (
                        <TableRow
                          key={index}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                          className="hover:bg-gray-400 select-none"
                          // onDoubleClick={handleRowDoubleClick}
                          // onDoubleClick={() => handleRowDoubleClick(row)}
                          onMouseEnter={() => {
                            setSelectedRowData(row);
                            // setIndex(index);
                          }}
                        >
                          {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                          <TableCell
                            className="flex flex-row w-10"
                            sx={{
                              // fontWeight: 800,
                              fontSize: ["12px", "15px"],
                              width: "200px",
                            }}
                          >
                            {row.type} &nbsp;
                          </TableCell>
                          <TableCell>{row.localization}</TableCell>
                          <TableCell>{row.status}</TableCell>
                          <TableCell>{row.subcategory}</TableCell>

                          <TableCell align="right">
                            <IconButton aria-controls={actionId}>
                              <CiSettings
                                color="#111111"
                                size={23}
                                onClick={handleActionMenuOpen}
                              />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                className="select-none"
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
      </div>
    </>
  );
}
const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  maxHeight: "80vh",
};
