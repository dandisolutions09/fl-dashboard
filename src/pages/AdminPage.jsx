import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Navbar from "../components/Navbar";
import TopNav from "../components/TopNav";
import endpoints from "../endpoints";
import { CiSettings } from "react-icons/ci";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  IconButton,
  InputAdornment,
  LinearProgress,
  Menu,
  MenuItem,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
import { styled, alpha } from "@mui/material/styles";
import { BsGearFill } from "react-icons/bs";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import {
  MdAirplanemodeActive,
  MdAirplanemodeInactive,
  MdEdit,
  MdOutlineAirplanemodeInactive,
} from "react-icons/md";
import { TbPasswordFingerprint } from "react-icons/tb";
import { RiDeleteBinFill } from "react-icons/ri";
import { IoIosCloseCircle, IoIosCloseCircleOutline } from "react-icons/io";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import AdminAddedSuccess from "../components/Alerts/AdminAddedSuccess";
import AdminAddfailed from "../components/Alerts/AdminAddfailed";
import { useState } from "react";
import {
  styles,
  style_box,
  Search,
  SearchIconWrapper,
  StyledInputBase,
} from "../styles";
import UserNameTag from "../components/UserNameTag";

export default function AdminPage() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openAction, setOpenAction] = React.useState(false);
  const [isOPen, setIsOpen] = React.useState(false);
  const [openAddCompany, setOpenAddCompany] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);
  const [openResetPassword, setOpenResetPassword] = React.useState(false);
  const [openAddAdminSuccess, setOpenAddAdminSuccess] = React.useState(false);
  const [adminDetails, setAdminDetails] = useState({});
  const [openAdminEdit, setOpenAdminEdit] = React.useState(false);
  const [selectedRowData, setSelectedRowData] = useState();
  const [searchTerm, setSearchTerm] = React.useState();

  const actionId = "primary-action-menu";

  const admin_type = [
    {
      value: "Main admin",
      label: "Main Admin",
    },
    {
      value: "Dispatcher admin",
      label: "Dispatcher Admin",
    },
    {
      value: "Billing admin",
      label: "Billing Admin",
    },
  ];
  const handleOpenAdmin = () => {
    formikEditAdmin.setValues({
      // email: selectedRowData.email,
      first_name: selectedRowData.first_name,
      last_name: selectedRowData.last_name,
      email: selectedRowData.email,
      admin_type: selectedRowData.type,

      //password: selectedRowData.password,
    });

    setOpenAdminEdit(true);
  };
  const handleCloseEditAdmin = () => {
    setOpenAdminEdit(false);
  };
  const handleOpen_AddCompany = () => {
    setOpenAddCompany(true);
  };
  const handleClose_AddCompany = () => {
    setOpenAddCompany(false);
  };
  const handleOpenResetPassWord = () => {
    console.log("selected row name", selectedRowData.first_name);
    setOpenResetPassword(true);
  };
  const handleCloseResetPassWord = () => {
    setOpenResetPassword(false);
  };
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const items = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={actionId}
      keepMounted
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      open={isOPen}
      onClose={handleActionMenuClose}
    >
      <MenuItem className="flex gap-2" onClick={handleOpenAdmin}>
        <MdEdit /> Edit
      </MenuItem>
      <MenuItem className="flex gap-2">
        <MdAirplanemodeActive /> Activate
      </MenuItem>
      <MenuItem className="flex gap-2">
        <MdOutlineAirplanemodeInactive /> Deactivate
      </MenuItem>
      <MenuItem className="flex gap-2" onClick={handleOpenResetPassWord}>
        <TbPasswordFingerprint /> Reset Password
      </MenuItem>
      <MenuItem className="flex gap-2">
        <RiDeleteBinFill /> Delete
      </MenuItem>
    </Menu>
  );
  function handleActionMenuClose() {
    setIsOpen(false);
  }
  function handleActionMenuOpen() {
    setIsOpen(true);
  }

  function filterUsers(searchVal) {
    const data = JSON.parse(localStorage.getItem("adminData")) || [];

    console.log("To be filtered users:->", data);

    if (searchVal === undefined || searchVal.trim().length === 0) {
      setRows(data);
    } else {
      const searchTerm = searchVal.toLowerCase().split(" ");

      const results = data.filter((user) => {
        const firstName = user.first_name ? user.first_name.toLowerCase() : "";
        const lastName = user.last_name ? user.last_name.toLowerCase() : "";
        const email = user.email ? user.email.toLowerCase() : "";
        const phoneNumber = user.phone_number ? user.phone_number : "";
        const companyName = user.main_businessName
          ? user.main_businessName.toLowerCase()
          : "";

        return searchTerm.every(
          (term) =>
            firstName.includes(term) ||
            lastName.includes(term) ||
            email.includes(term) ||
            phoneNumber.includes(term) ||
            companyName.includes(term)
        );
      });

      setRows(results);
    }
  }

  function handleGetAllRecords(usrId) {
    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    //console.error("retrieved username  and ID:-->", storedData);
    setLoading(true);

    fetch(endpoints.getAdmins, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // optional, depending on your API requirements
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log("JSON:->", json);
        //json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        //setRows(json);

        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        localStorage.setItem("adminData", JSON.stringify(json));
        console.log("admin data set globally...");
        setRows(json);
        // Your further processing logic
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  async function getAdminDetails() {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
   // console.log("json_token", stored_json_data);
    console.log(
      "full url",
      `${endpoints.getAdminById}/${stored_json_data.admin_id}`
    );

    try {
      // Replace 'YOUR_ENDPOINT' with the actual endpoint URL
      const response = await axios.get(
        `${endpoints.getAdminById}/${stored_json_data.admin_id}`,
        {
          headers: {
            Authorization: `Bearer ${stored_json_data.token}`,
          },
        }
      );
      //setAdmin(response.data);
      console.log("response", response.data.first_name);
      //setFirstName(response.data.first_name);
      // setLastName(response.data.last_name);
      setAdminDetails(response.data);
    } catch (error) {
      //setError(error.message);
      console.log("error", error);
    }
  }

  const postAdminRegistrationData = async (
    first_name,
    last_name,
    email,
    admin_type,
    pwd
  ) => {
    //console.log("data to be sent", email, pwd);
    // values.first_name.trim(),
    // values.last_name.trim(),
    // values.admin_type.trim(),
    // values.email.trim(),
    // values.password.trim()

    const AdminData = {
      first_name: first_name,
      last_name: last_name,
      type: admin_type,
      email: email,
      password: pwd,
      status: "ACTIVE",
    };

    //console.log("data to be sent", AdminData);

    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);

    try {
      setLoading(true); // Set loading to true when making the request
      setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };
      const response = await axios.post(endpoints.addAdmin, AdminData);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
      handleClose_AddCompany();
    }
  };


  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    filterUsers(event.target.value);
    // handleGetAllRecords(event.target.value);
  };

  const postUpdatedRegistrationData = async (
    first_name,
    last_name,
    email,
    admin_type
    //pwd
  ) => {
    //console.log("data to be sent", email, pwd);
    // values.first_name.trim(),
    // values.last_name.trim(),
    // values.admin_type.trim(),
    // values.email.trim(),
    // values.password.trim()

    const updatedAdminData = {
      first_name: first_name,
      last_name: last_name,
      type: admin_type,
      email: email,
      status: "ACTIVE",
      //password: pwd,
    };

    //console.log("data to be sent", AdminData);
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);

    var storedToken = localStorage.getItem("stored_token");
    var json_token = JSON.parse(storedToken);
    console.log("Updating id", selectedRowData._id);
    //console.log("json_token", json_token.token);

    try {
      setLoading(true); // Set loading to true when making the request
      setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };

      const response = await fetch(
        `${endpoints.editAdminInfo}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${stored_json_data.token}`,
          },
          body: JSON.stringify(updatedAdminData),
        }
      );
      console.log("response after edit:->>", response);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
      handleClose_AddCompany();
    }
  };

  async function resetPassword(adminDetail, newPassword) {
    var storedToken = localStorage.getItem("stored_token");
    var stored_json_data = JSON.parse(storedToken);
    console.log("json_token", stored_json_data);
    // console.log(
    //   "full url",
    //   `${endpoints.getAdminById}/${stored_json_data.admin_id}`
    // );
    const ongoing_json = {
      first_name: selectedRowData.first_name,
      last_name: selectedRowData.last_name,
      email: selectedRowData.email,
      password: newPassword,
      status: selectedRowData.status,
    };

    console.log("ongoing json for reset:->", ongoing_json);

    try {
      // setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${stored_json_data.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };

      const response = await fetch(
        `${endpoints.resetAdminPassword}/${selectedRowData._id}`,

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${stored_json_data.token}`,
          },
          body: JSON.stringify(ongoing_json),
        }
      );
      console.log("response after edit:->>", response);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      //setLoading(false); // Set loading to false after the request is complete (success or error)
      //handleClose_AddCompany();
    }
  }

  const validationSchema = Yup.object({
    first_name: Yup.string().required("first name is required"),
    last_name: Yup.string().required("last name is required"),
    email: Yup.string().required("email is required"),
    admin_type: Yup.string().required("admin type is required"),
    password: Yup.string().required("password is required"),
  });

  const validationSchema_resetPassword = Yup.object({
    // first_name: Yup.string().required("first name is required"),
    // last_name: Yup.string().required("last name is required"),
    // email: Yup.string().required("email is required"),
    // admin_type: Yup.string().required("admin type is required"),
    password: Yup.string().required("password is required"),
  });

  const formikNewAdmin = useFormik({
    initialValues: {
      email: "",
      first_name: "",
      last_name: "",
      email: "",
      admin_type: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      postAdminRegistrationData(
        values.first_name.trim(),
        values.last_name.trim(),
        values.email.trim(),
        values.admin_type.trim(),
        values.password.trim()
      );
    },
  });

  const formikEditAdmin = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      email: "",
      admin_type: "",
    },
    //validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("Edited form data:-", values);
      postUpdatedRegistrationData(
        values.first_name.trim(),
        values.last_name.trim(),
        values.email.trim(),
        values.admin_type.trim()
        //values.password.trim()
      );

      // postAdminRegistrationData(
      //   values.first_name.trim(),
      //   values.last_name.trim(),
      //   values.email.trim(),
      //   values.admin_type.trim(),
      //   values.password.trim()
      // );
    },
  });

  const formikResetPassword = useFormik({
    initialValues: {
      password: "",
    },
    validationSchema: validationSchema_resetPassword,
    onSubmit: (values) => {
      console.log("form data:-", values);

      console.log("first name:->", adminDetails.first_name);
      resetPassword(adminDetails, values.password.trim());

      //postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  React.useEffect(() => {
    handleGetAllRecords();
    getAdminDetails();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  return (
    <>
      <AdminAddedSuccess
        open={openAddAdminSuccess}
        autoHideDuration={4000}
        name={"Admin"}
        mssg={"Added Successfully"}
        onClose={() => setOpenAddAdminSuccess(false)}
      />
      <AdminAddfailed autoHideDuration={4000} />
      {loading && <LinearProgress />}
      <Modal
        onClose={handleCloseResetPassWord}
        open={openResetPassword}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <Typography className="flex flex-row gap-2 text-black">
            Reset{" "}
            {/* <Typography sx={{ fontWeight: "bold" }}>
              {" "}
              {selectedRowData.username}&#39;s
            </Typography> */}
            Password?
          </Typography>

          {/* */}
          <form onSubmit={formikResetPassword.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <TextField
                type={showPassword ? "text" : "password"}
                id="password"
                name="password"
                label="New Password:"
                placeholder="Enter password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                maxRows={4}
                value={formikResetPassword.values.password}
                onChange={formikResetPassword.handleChange}
                onBlur={formikResetPassword.handleBlur}
                error={
                  formikResetPassword.touched.password &&
                  Boolean(formikResetPassword.errors.password)
                }
                helperText={
                  formikResetPassword.touched.password &&
                  formikResetPassword.errors.password
                }
              />
            </div>

            <div className="flex flex-row gap-4 m-4">
              <Button variant="contained" type="submit">
                Reset
              </Button>
              <Button
                variant="contained"
                color="error"
                onClick={handleCloseResetPassWord}
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openAddCompany}
        onClose={handleClose_AddCompany}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4 text-[#111111]">
              Add Admin Information
            </h2>
            <IoIosCloseCircle
              color="#000"
              size={23}
              cursor={"pointer"}
              onClick={handleClose_AddCompany}
            />
          </div>

          <form onSubmit={formikNewAdmin.handleSubmit}>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="first_name"
                  name="first_name"
                  label="First Name"
                  placeholder="First Name"
                  value={formikNewAdmin.values.first_name}
                  onChange={formikNewAdmin.handleChange}
                  onBlur={formikNewAdmin.handleBlur}
                  error={
                    formikNewAdmin.touched.first_name &&
                    Boolean(formikNewAdmin.errors.first_name)
                  }
                  helperText={
                    formikNewAdmin.touched.first_name &&
                    formikNewAdmin.errors.first_name
                  }
                />

                <TextField
                  type="text"
                  id="last_name"
                  name="last_name"
                  label="Last Name"
                  placeholder="Last Name"
                  value={formikNewAdmin.values.last_name}
                  onChange={formikNewAdmin.handleChange}
                  onBlur={formikNewAdmin.handleBlur}
                  error={
                    formikNewAdmin.touched.last_name &&
                    Boolean(formikNewAdmin.errors.last_name)
                  }
                  helperText={
                    formikNewAdmin.touched.last_name &&
                    formikNewAdmin.errors.last_name
                  }
                />
              </div>
            </div>
            <div className="flex flex-col gap-3 ">
              <div className="flex flex-row gap-3  mb-3">
                <TextField
                  type="email"
                  id="email"
                  name="email"
                  label="Email"
                  placeholder="Email"
                  value={formikNewAdmin.values.email}
                  onChange={formikNewAdmin.handleChange}
                  onBlur={formikNewAdmin.handleBlur}
                  error={
                    formikNewAdmin.touched.email &&
                    Boolean(formikNewAdmin.errors.email)
                  }
                  helperText={
                    formikNewAdmin.touched.email && formikNewAdmin.errors.email
                  }
                />
                <TextField
                  type="password"
                  id="password"
                  name="password"
                  label="Password"
                  placeholder="Password"
                  value={formikNewAdmin.values.password}
                  onChange={formikNewAdmin.handleChange}
                  onBlur={formikNewAdmin.handleBlur}
                  error={
                    formikNewAdmin.touched.password &&
                    Boolean(formikNewAdmin.errors.password)
                  }
                  helperText={
                    formikNewAdmin.touched.password &&
                    formikNewAdmin.errors.password
                  }
                />
              </div>
            </div>

            <div className="flex flex-col w-full">
              <FormControl>
                <FormLabel>Group </FormLabel>

                <TextField
                  id="admin_type"
                  select
                  name="admin_type"
                  fullWidth
                  value={formikNewAdmin.values.admin_type}
                  onChange={formikNewAdmin.handleChange}
                  onBlur={formikNewAdmin.handleBlur}
                  helperText="Select position"
                >
                  {admin_type.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openAdminEdit}
        onClose={handleCloseEditAdmin}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <h2 className="font-bold m-4 text-[#111111]">
              Edit Admin Information
            </h2>
            <IoIosCloseCircle
              color="#000"
              size={23}
              cursor={"pointer"}
              onClick={handleCloseEditAdmin}
            />
          </div>

          <form onSubmit={formikEditAdmin.handleSubmit}>
            <div className="flex flex-col gap-3 mt-3">
              <div className="flex flex-row gap-3  mb-4">
                <TextField
                  type="text"
                  id="first_name"
                  name="first_name"
                  label="First Name"
                  placeholder="First Name"
                  value={formikEditAdmin.values.first_name}
                  onChange={formikEditAdmin.handleChange}
                  onBlur={formikEditAdmin.handleBlur}
                  error={
                    formikEditAdmin.touched.first_name &&
                    Boolean(formikEditAdmin.errors.first_name)
                  }
                  helperText={
                    formikEditAdmin.touched.first_name &&
                    formikEditAdmin.errors.first_name
                  }
                />

                <TextField
                  type="text"
                  id="last_name"
                  name="last_name"
                  label="Last Name"
                  placeholder="Last Name"
                  value={formikEditAdmin.values.last_name}
                  onChange={formikEditAdmin.handleChange}
                  onBlur={formikEditAdmin.handleBlur}
                  error={
                    formikEditAdmin.touched.last_name &&
                    Boolean(formikEditAdmin.errors.last_name)
                  }
                  helperText={
                    formikEditAdmin.touched.last_name &&
                    formikEditAdmin.errors.last_name
                  }
                />
              </div>
            </div>
            <div className="flex flex-col gap-3 ">
              {/* <div className="flex flex-row gap-3  mb-3">
                <TextField
                  type="email"
                  id="email"
                  name="email"
                  label="Email"
                  placeholder="Email"
                  value={formikEditAdmin.values.email}
                  onChange={formikEditAdmin.handleChange}
                  onBlur={formikEditAdmin.handleBlur}
                  error={
                    formikEditAdmin.touched.email &&
                    Boolean(formikEditAdmin.errors.email)
                  }
                  helperText={
                    formikEditAdmin.touched.email &&
                    formikEditAdmin.errors.email
                  }
                />
                <TextField
                  type="password"
                  id="password"
                  name="password"
                  label="Password"
                  placeholder="Password"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  value={formikEditAdmin.values.password}
                  onChange={formikEditAdmin.handleChange}
                  onBlur={formikEditAdmin.handleBlur}
                  error={
                    formikEditAdmin.touched.password &&
                    Boolean(formikEditAdmin.errors.password)
                  }
                  helperText={
                    formikEditAdmin.touched.password &&
                    formikEditAdmin.errors.password
                  }
                />
              </div> */}
            </div>

            <div className="flex flex-col w-full">
              <FormControl>
                <FormLabel>Group </FormLabel>

                <TextField
                  id="admin_type"
                  select
                  name="admin_type"
                  fullWidth
                  value={formikEditAdmin.values.admin_type}
                  onChange={formikEditAdmin.handleChange}
                  onBlur={formikEditAdmin.handleBlur}
                  helperText="Select position"
                >
                  {admin_type.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
            </div>

            <div className="flex flex-col gap-3 mt-3">
              <div>
                <Button
                  variant="contained"
                  sx={{ backgroundColor: "#242333" }}
                  size="small"
                  type="submit"
                >
                  Save
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <Navbar />
      <div className="relative sm:ml-[300px] bg-white h-screen ">
        <TopNav />
        <div className="bg-white relative h-s  flex flex-col select-none">
          <div className="absolute top-4 left-4 justify-between sm:hidden md:hiddden   z-[1000]    lg:hidden">
            <UserNameTag />
          </div>
          <div className="border-b border-gray-400 sm:m-6 m-3 py-2 flex sm:flex-row flex-col justify-between">
            <div className="flex sm:flex-row flex-col justify-start ">
              <div className="flex flex-col justify-start text-start mt-8 ">
                <h1 className="text-lg sm:text-sm md:text-lg lg:text-3xl font-bold text-[#000] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] ">
                  Admin
                </h1>
              </div>
              <div className=" sm:mt-8 mt-1 ">
                <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                  <SearchIconWrapper>
                    <SearchIcon sx={{ color: "#616569" }} />
                  </SearchIconWrapper>
                  <StyledInputBase
                    placeholder="Search…"
                    inputProps={{ "aria-label": "search" }}
                    value={searchTerm}
                    onChange={handleSearchChange}
                  />
                </Search>
              </div>
            </div>

            <div className="mt-8 flex">
              <Button variant="contained" onClick={handleOpen_AddCompany}>
                Add Admin
              </Button>
            </div>
          </div>
          <div
            style={{
              // height: 400,
              // width: "100%",
              backgroundColor: "#fff",
              margin: "1rem",
            }}
          >
            <Paper sx={{ width: "100%", overflow: "auto" }}>
              <TableContainer sx={{ maxHeight: 800, minWidth: 1000 }}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow className="select-none">
                      <TableCell
                        sx={{
                          fontWeight: 800,
                          fontSize: ["14px", "14px"],
                          width: "200px",
                        }}
                      >
                        Admin Name
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Email
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Type
                      </TableCell>
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Status
                      </TableCell>

                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Action
                      </TableCell>
                      {/* <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Wallet Balance
                      </TableCell> */}

                      {/* <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Manager Services
                      </TableCell> */}
                      {/* 
                      <TableCell
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Status
                      </TableCell> */}

                      {/* <TableCell
                        align="center"
                        sx={{ fontWeight: 800, fontSize: ["14px", "14px"] }}
                      >
                        Action
                      </TableCell> */}

                      {/* <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Balance
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  House selling Price
                </TableCell>
                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  House Number{" "}
                </TableCell> */}

                      {/* <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Project{" "}
                </TableCell>

                <TableCell sx={{ fontWeight: 800, fontSize: ["14px", "18px"] }}>
                  Location{" "}
                </TableCell> */}
                      {items}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => (
                        <TableRow
                          key={index}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                          className="hover:bg-gray-400 select-none"
                          //onDoubleClick={handleRowDoubleClick}
                          // onDoubleClick={() => handleRowDoubleClick(row)}
                          onMouseEnter={() => {
                            setSelectedRowData(row);
                            //setIndex(index);
                          }}
                        >
                          {/* <TableCell align="">{row.created_at.toLocaleDateString('en-US')}</TableCell> */}
                          <TableCell
                            className="flex flex-row w-10"
                            sx={{
                              // fontWeight: 800,
                              fontSize: ["12px", "15px"],
                              width: "200px",
                            }}
                          >
                            {row.first_name} &nbsp;
                            {row.last_name}
                          </TableCell>
                          <TableCell>{row.email}</TableCell>
                          <TableCell>{row.type}</TableCell>

                          <TableCell>{row.status}</TableCell>

                          <TableCell>
                            <IconButton aria-controls={actionId}>
                              <CiSettings
                                color="#111111"
                                size={23}
                                onClick={handleActionMenuOpen}
                              />
                            </IconButton>
                          </TableCell>

                          {/* <TableCell>
                            {parseFloat(row.email).toLocaleString()}
                          </TableCell> */}

                          {/* Selling Price Column */}

                          {/* house unit column */}

                          {/* Location column */}

                          {/* Status column */}

                          {/* Amount Paid Column */}
                          {/* Balance Column */}
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                className="select-none"
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
      </div>
    </>
  );
}
