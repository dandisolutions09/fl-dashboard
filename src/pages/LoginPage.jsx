import {
  Box,
  Button,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import LOGO from "../assets/admin-logo.png";
import {
  MdAccountCircle,
  MdEmail,
  MdVisibility,
  MdVisibilityOff,
} from "react-icons/md";
import LoginTopbar from "../components/LoginTopbar";
import { useNavigate } from "react-router-dom";
import endpoints from "../endpoints";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { styled } from "@mui/material/styles";
import LoginSuccessfull from "../components/Alerts/LoginSuccessfull";
import LoginUnsuccefull from "../components/Alerts/LoginUnsuccefull";
// import endpoints from "./endpoints";

export default function LoginPage() {
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const [showPassword, setShowPassword] = React.useState(false);
  const [openLoginSuccess, setOpenLoginSuccess] = React.useState(false);
  const [openLoginFailed, setOpenLoginFailed] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const navigate = useNavigate();

  const validationSchema = Yup.object({
    email: Yup.string().required("email is required"),
    password: Yup.string().required("password is required"),
  });
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleNavigate_Home = () => {
    navigate("/dash");
  };

  const handleNavigateMain = (incoming_token, incoming_admin_id) => {
    setOpenLoginSuccess(true);
    console.log("Connecting...");
    ///console.log("email", passed_email, id, passed_firstname);

    const userData = {
      token: incoming_token,
      admin_id: incoming_admin_id,
    };
    console.log("user data",  userData)
    //setUserData(userData);

    const userDataJSON = JSON.stringify(userData);

    localStorage.setItem("stored_token", userDataJSON);


    

    setTimeout(() => {
      navigate("/dash");

      // Navigate to the "/home" route after the delay
    }, 2000);
    formikSubmit.resetForm();
  };

  const postLoginCreds = async (email, pwd) => {
    console.log("data to be sent", email, pwd);

    const LoginData = {
      email: email.trim(),

      password: pwd.trim(),
    };

    try {
      setLoading(true); // Set loading to true when making the request



      // const response = await axios.post(endpoints.login, LoginData);

       const response = await axios.post(endpoints.login, LoginData);

      //const response = await axios.post("http://localhost:8080/login", LoginData);


      console.log("Response from the backend:", response.data);

      //var arrayResult = response.data.split(",");

      if (response.data.message == "Authentication successful") {
        console.error("SUCCESSFULL", response.data.token);

        handleNavigateMain(
          // LoginData.username,
          response.data.token,
          response.data.admin_id
          //response.data.user.first_name + response.data.user.last_name
        );
      } else if (response.data.message == "Authentication Not Successful") {
        console.error("NOT SUCCESSFULL");
        setOpenLoginFailed(true);
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };
  const formikSubmit = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      postLoginCreds(values.email.trim(), values.password.trim());
    },
  });

  return (
    <>
      {loading && <LinearProgress />}
      <LoginSuccessfull
        open={openLoginSuccess}
        autoHideDuration={6000}
        //onClose={() => setOpenLoginSuccess(false)}
      />
      <LoginUnsuccefull
        open={openLoginFailed}
        autoHideDuration={3000}
        onClose={() => setOpenLoginFailed(false)}
      />
      <LoginTopbar />

      <div className="relative h-screen bg-white select-none">
        <div className="flex bg-white ">
          <Box
            sx={{ width: [390, 600] }}
            style={myComponentStyle}
            className="shadow-xl shadow-gray-400 rounded-lg border"
          >
            <img src={LOGO} alt="componay_logo" style={company_logo} />
            <div className="header" style={Header}>
              <Typography
                sx={{ fontSize: [30, 48], fontWeight: "bold", color: "#000" }}
              >
                FineLooks
              </Typography>

              <div className="underline" style={underline}></div>
              <Typography
                className="text-sm"
                sx={{ fontSize: [12, 20], color: "#000" }}
              >
                Portal
              </Typography>
            </div>
            <form
              onSubmit={formikSubmit.handleSubmit}
              className="flex flex-col w-full"
            >
              <div className="flex flex-col justify-center text-center gap-3 mt-8">
                <div className="input">
                  <TextField
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <MdEmail size={22} />
                        </InputAdornment>
                      ),
                    }}
                    type="email"
                    // style={inputfield}
                    sx={{ width: [320, 400] }}
                    placeholder="email"
                    name="email"
                    value={formikSubmit.values.email}
                    onChange={formikSubmit.handleChange}
                    onBlur={formikSubmit.handleBlur}
                    error={
                      formikSubmit.touched.email &&
                      Boolean(formikSubmit.errors.email)
                    }
                    helperText={
                      formikSubmit.touched.email && formikSubmit.errors.email
                    }
                  />
                </div>
                <div className="input">
                  <TextField
                    type={showPassword ? "text" : "password"}
                    name="password"
                    // style={inputfield}
                    sx={{ width: [320, 400] }}
                    placeholder="password"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                          >
                            {showPassword ? (
                              <MdVisibilityOff />
                            ) : (
                              <MdVisibility />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                    value={formikSubmit.values.password}
                    onChange={formikSubmit.handleChange}
                    onBlur={formikSubmit.handleBlur}
                    autoComplete="none"
                    error={
                      formikSubmit.touched.password &&
                      Boolean(formikSubmit.errors.password)
                    }
                    helperText={
                      formikSubmit.touched.password &&
                      formikSubmit.errors.password
                    }
                  />
                </div>
              </div>

              <div style={submit_container}>
                <Button
                  // onClick={handleNavigate_Home}
                  type="submit"
                  style={submit}
                  className=" text-white"
                >
                  <Typography sx={{ fontSize: [12, 15] }}>SIGN IN</Typography>
                </Button>
              </div>

              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <div style={{ marginLeft: [15, 10], marginTop: 4 }}>
                  <Typography
                    className="hover:underline cursor-pointer"
                    // onClick={handleNavigate_signup}
                    sx={{ fontSize: [12, 18] }}
                  >
                    Sign Up if you don't Have an Account
                  </Typography>
                </div>
              </div>
            </form>
          </Box>
        </div>
      </div>
    </>
  );
}

const myComponentStyle = {
  background: "",
  // height: "760px",
  alignItems: "center",
  display: "flex",
  padding: "10px",
  margin: "auto",
  marginTop: "120px",
  // width: "600px",
  flexDirection: "column",
  padingBottom: "20px",
};
const Header = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  gap: "9px",
  width: "100%",
  marginTop: "30px",
};

const text = {
  color: "#000",

  fontSize: "48px",
  fontWeight: "700",
};
const underline = {
  width: "71px",
  height: "6px",
  background: "#000",
  borderRadius: "9px",
};
const inputs = {
  marginTop: "55px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "column",
  gap: "25px",
};
const input = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  // width: "480px",
  // height: "80px",
};
const icons = {
  marginRight: "30px",
};
const inputfield = {
  height: "50px",
  width: "400px",
  background: "transparent",
  border: "none",
  outline: "none",
  color: "#797979",
  fontSize: "19px",
};
const forgot_password = {
  // paddingLeft: "70px",
  // marginTop: "27px",
  // color: "#797979",
  // justifyContent:"center",
  // alignItems: "center",
};
const spn = {
  color: "#4c00b4",
};
const submit_container = {
  // display: "flex",
  // gap: "30px",
  margin: "30px 110px",
  display: "flex",
  justifyContent: "center",
};
const submit = {
  display: "flex",
  background: "#000",
  justifyContent: "center",
  alignItems: "center",
  width: "220px",
  height: "59px",
  color: "#fff",
  borderRadius: "50px",
  fontSize: "19px",
  fontWeight: "700",
  cursor: "pointer",
};
const company_logo = {
  width: "100px",
  height: "90px",
  marginTop: "20px",
  //   borderRadius: 50,
  // padingBottom: "30px",
  // boxShadow: "0px 0px 10px 0px rgba(0, 0, 0, 0.1)",
};
