import { HashRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import ProvidersPage from "./pages/ProvidersPage";
import SiteStatistics from "./pages/SiteStatistics";
import CompanyPage from "./pages/CompanyPage";
import AdminGroupsPage from "./pages/AdminGroupsPage";
import AdminPage from "./pages/AdminPage";
import ManualBookingPage from "./pages/ManualBookingPage";
import UsersPage from "./pages/UsersPage";
import LogsPage from "./pages/LogsPage";
import { ThemeProvider, createTheme } from "@mui/material";
import ManageServices from "./pages/ManageServices";
import PromoCode from "./pages/PromoCode";



function App() {
    const theme = createTheme({
      typography: {
        fontFamily: ["Play"],
        backdropFilter: "none",
      },
    });
  return (
    <ThemeProvider theme={theme}>
      <HashRouter>
        <Routes>
          <Route index element={<LoginPage />} />
          <Route path="/" element={<LoginPage />} />
          <Route path="/home" element={<LoginPage />} />
          <Route path="/dash" element={<HomePage />} />
          <Route path="/providers" element={<ProvidersPage />} />
          <Route path="/statistics" element={<SiteStatistics />} />
          <Route path="/company" element={<CompanyPage />} />
          <Route path="/admingroups" element={<AdminGroupsPage />} />
          <Route path="/admin" element={<AdminPage />} />
          <Route path="/manualbooking" element={<ManualBookingPage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="/logs" element={<LogsPage />} />
          <Route path="/manageservices" element={<ManageServices />} />
          <Route path="/motions" element={<PromoCode/>} />
        </Routes>
      </HashRouter>
    </ThemeProvider>
  );
}

export default App;
