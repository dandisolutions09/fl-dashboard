#!/bin/bash

# Function to print messages
print_message() {
  echo "**** $1"
}

# Stop and remove existing frontend-container if it exists
print_message "Stopping and removing existing frontend-container if it exists..."
docker rm -f frontend-container >/dev/null 2>&1

if [ $? -eq 0 ]; then
  print_message "Container stopped and removed successfully."
else
  print_message "No existing container found, or failed to remove."
fi

# Build the frontend-image
print_message "Building the Docker image..."
docker build -t frontend-image .

if [ $? -eq 0 ]; then
  print_message "Successfully built the image."
else
  print_message "Failed to build the image."
  exit 1
fi

# Run the container in detached mode and map port 8001 to port 80 in the container
print_message "Running the container..."
docker run -d -p 8001:80 --name frontend-container frontend-image

if [ $? -eq 0 ]; then
  print_message "Container is running."
else
  print_message "Failed to start the container."
  exit 1
fi

print_message "Setup complete. Frontend is running and accessible at port 8001."
