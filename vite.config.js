import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],

  //we need to define which port the project will run on when we execute  $npm run preview

  preview:{
    host: true,
    port:9000
  }
})
